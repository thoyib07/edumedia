<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_belajar extends CI_Model {   

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_id_murid_id_materi($id_murid, $id_materi)
    {
        $this->db->select('
                t_belajar.id_belajar,
                t_join_kursus.id_join_kursus,
                m_murid.id_murid,
                m_materi.id_materi,
                m_kursus.id_kursus,
                m_materi.nama_materi,
                m_materi.embed_video,
                m_materi.path_pdf,
                m_materi.create_date,
                m_kursus.nama_kursus,
                m_kursus.card_kursus,
                t_belajar.nilai_kuis
            ');
        $this->db->from('t_belajar');
        $this->db->join('t_join_kursus', 't_join_kursus.id_join_kursus = t_belajar.id_join_kursus');
        $this->db->join('m_murid', 'm_murid.id_murid = t_join_kursus.id_murid');
        $this->db->join('m_materi', 'm_materi.id_materi = t_belajar.id_materi');
        $this->db->join('m_kursus', 'm_kursus.id_kursus = t_join_kursus.id_kursus');
        $this->db->where('m_materi.id_materi', $id_materi);
        $this->db->where('m_murid.id_murid', $id_murid);
        $query = $this->db->get()->row();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }

    public function get_by_id($id_belajar)
    {
        $this->db->select('
                t_belajar.id_belajar,
                t_join_kursus.id_join_kursus,
                m_murid.id_murid,
                m_materi.id_materi,
                m_kursus.id_kursus,
                m_materi.nama_materi,
                m_materi.embed_video,
                m_materi.path_pdf,
                m_materi.create_date,
                m_kursus.nama_kursus,
                m_kursus.card_kursus,
                t_belajar.nilai_kuis
            ');
        $this->db->from('t_belajar');
        $this->db->join('t_join_kursus', 't_join_kursus.id_join_kursus = t_belajar.id_join_kursus');
        $this->db->join('m_murid', 'm_murid.id_murid = t_join_kursus.id_murid');
        $this->db->join('m_materi', 'm_materi.id_materi = t_belajar.id_materi');
        $this->db->join('m_kursus', 'm_kursus.id_kursus = t_join_kursus.id_kursus');
        $this->db->where('t_belajar.id_belajar', $id_belajar);
        $whereQuery = array('t_belajar.status' => '1','t_join_kursus.status' => '1','m_murid.status' => '1','m_materi.status' => '1','m_kursus.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->row();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }
}