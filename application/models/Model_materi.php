<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_materi extends CI_Model {   

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // public function get_all_by_id_mata_pelajaran($id_mata_pelajaran, $semseter)
    // {
    //     $this->db->select('
    //             m_materi.id_materi,
    //             m_mata_pelajaran.id_mata_pelajaran,
    //             m_tingkat.id_tingkat,
    //             m_jurusan.id_jurusan,
    //             m_mata_pelajaran.nama_mata_pelajaran,
    //             m_tingkat.nama_tingkat,
    //             m_jurusan.nama_jurusan,
    //             m_mata_pelajaran.kelas,
    //             m_materi.nama_materi,
    //             m_materi.path_pdf,
    //             m_materi.path_video
    //         ');
    //     $this->db->from('m_materi');
    //     $this->db->join('m_mata_pelajaran', 'm_mata_pelajaran.id_mata_pelajaran = m_materi.id_mata_pelajaran', 'left');
    //     $this->db->join('m_tingkat', 'm_tingkat.id_tingkat = m_mata_pelajaran.id_tingkat', 'left');
    //     $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = m_mata_pelajaran.id_jurusan', 'left');
    //     $this->db->where('m_mata_pelajaran.id_mata_pelajaran', $id_mata_pelajaran);
    //     $this->db->where('m_materi.semseter', $semseter);
    //     $this->db->where('m_materi.`status`', '1');
    //     $query = $this->db->get()->result();
    //     // var_dump($this->db->last_query());
    //     // exit;
    //     return $query;
    // }

    // public function get_by_id($id_materi)
    // {
    //     $this->db->select('
    //             m_materi.id_materi,
    //             m_mata_pelajaran.id_mata_pelajaran,
    //             m_tingkat.id_tingkat,
    //             m_jurusan.id_jurusan,
    //             m_mata_pelajaran.nama_mata_pelajaran,
    //             m_tingkat.nama_tingkat,
    //             m_jurusan.nama_jurusan,
    //             m_materi.nama_materi,
    //             m_materi.path_pdf,
    //             m_materi.path_video
    //         ');
    //     $this->db->from('m_materi');
    //     $this->db->join('m_mata_pelajaran', 'm_mata_pelajaran.id_mata_pelajaran = m_materi.id_mata_pelajaran', 'left');
    //     $this->db->join('m_tingkat', 'm_tingkat.id_tingkat = m_mata_pelajaran.id_tingkat', 'left');
    //     $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = m_mata_pelajaran.id_jurusan', 'left');
    //     $this->db->where('m_materi.id_materi', $id_materi);
    //     $query = $this->db->get()->row();
    //     // var_dump($this->db->last_query());
    //     // exit;
    //     return $query;
    // }

    public function get_top(){
        $this->db->select('
                m_materi.id_materi,
                m_sesi.id_sesi,
                m_kursus.id_kursus,
                m_mentor.id_mentor,
                m_user.id_user,
                m_sub_kategori.id_sub_kategori,
                m_kategori.id_kategori,
                m_materi.nama_materi,
                m_materi.embed_video,
                m_materi.path_pdf,
                m_sesi.nama_sesi,
                m_kursus.nama_kursus,
                m_kursus.card_kursus,
                m_user.nama_user,
                m_sub_kategori.nama_sub_kategori,
                m_kategori.nama_kategori,
                m_kategori.icon,
                m_kategori.icon_web,
                m_kategori.background,
                m_materi.`status`
            ');
        $this->db->from('m_materi');
        $this->db->join('m_sesi', 'm_sesi.id_sesi = m_materi.id_sesi', 'left');
        $this->db->join('m_kursus', 'm_kursus.id_kursus = m_sesi.id_kursus');
        $this->db->join('m_mentor', 'm_mentor.id_mentor = m_kursus.id_mentor');
        $this->db->join('m_user', 'm_user.id_user = m_mentor.id_user');
        $this->db->join('m_sub_kategori', 'm_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori');
        $this->db->join('m_kategori', 'm_kategori.id_kategori = m_sub_kategori.id_kategori');
        $whereQuery = array('m_materi.status' => '1','m_sesi.status' => '1','m_kursus.status' => '1','m_mentor.status' => '1','m_user.status' => '1','m_sub_kategori.status' => '1','m_kategori.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;
        return $query;        
    }

    public function get_by_id_kursus($id_kursus){
        $this->db->select('
                m_materi.id_materi,
                m_sesi.id_sesi,
                m_kursus.id_kursus,
                m_mentor.id_mentor,
                m_user.id_user,
                m_sub_kategori.id_sub_kategori,
                m_kategori.id_kategori,
                m_materi.nama_materi,
                m_materi.embed_video,
                m_materi.path_pdf,
                m_sesi.nama_sesi,
                m_kursus.nama_kursus,
                m_kursus.card_kursus,
                m_user.nama_user,
                m_sub_kategori.nama_sub_kategori,
                m_kategori.nama_kategori,
                m_kategori.icon,
                m_kategori.icon_web,
                m_kategori.background,
                m_materi.`status`
            ');
        $this->db->from('m_materi');
        // $this->db->join('m_sesi', 'm_sesi.id_sesi = m_materi.id_sesi', 'left');
        $this->db->join('m_sesi', 'm_sesi.id_kursus = m_materi.id_sesi', 'left');
        $this->db->join('m_kursus', 'm_kursus.id_kursus = m_sesi.id_kursus');
        $this->db->join('m_mentor', 'm_mentor.id_mentor = m_kursus.id_mentor');
        $this->db->join('m_user', 'm_user.id_user = m_mentor.id_user');
        $this->db->join('m_sub_kategori', 'm_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori');
        $this->db->join('m_kategori', 'm_kategori.id_kategori = m_sub_kategori.id_kategori');
        $this->db->where('m_kursus.`id_kursus`', $id_kursus);
        $whereQuery = array('m_materi.status' => '1','m_sesi.status' => '1','m_kursus.status' => '1','m_mentor.status' => '1','m_user.status' => '1','m_sub_kategori.status' => '1','m_kategori.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;
        return $query;        
    }

}