<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_murid extends CI_Model {   

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_by_id($id_murid)
    {
        $this->db->from('m_murid');
        $this->db->join('m_user', 'm_user.id_user = m_murid.id_user');
        $whereQuery = array('m_user.status' => '1','m_murid.status' => '1');
        $this->db->where($whereQuery);
        $this->db->where('m_murid.id_murid', $id_murid);
        $query = $this->db->get()->row();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }

 

}