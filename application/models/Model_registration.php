<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_registration extends CI_Model {   
    var $column_order = array();   
    var $column_search = array();     
    var $order = array(); 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    } 

    public function update($table, $where, $data)
    {
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }
}