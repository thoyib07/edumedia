<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_kuesioner extends CI_Model {   

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_by_id_mentor_id_soal_kuesioner($id_mentor, $id_soal_kuesioner)
    {
        $this->db->from('t_kuisioner');
        $this->db->where('id_mentor',$id_mentor);
        $this->db->where('id_soal_kuisioner',$id_soal_kuesioner);
        $this->db->where('status','1');
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;  
        return $query;
    }

}