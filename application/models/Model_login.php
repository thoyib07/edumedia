<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_Model {   
    var $column_order = array();   
    var $column_search = array();     
    var $order = array(); 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function cek_login($username, $password)
    {

        $this->db->select('   
                m_user.id_user,
                m_admin.id_admin,
                m_mentor.id_mentor,
                m_murid.id_murid,
                m_user.nama_user,
                m_user.no_hp,
                m_user.email,
                m_user.alamat,
                m_user.tanggal_lahir,
                m_user.agama,
                m_user.jenis_kelamin,
                m_user.path_foto,
                m_user.`status`
            ');

        $this->db->from('m_user');
        $this->db->join('m_mentor', 'm_user.id_user = m_mentor.id_user', 'left');
        $this->db->join('m_murid', 'm_user.id_user = m_murid.id_user', 'left');
        $this->db->join('m_admin', 'm_user.id_user = m_admin.id_user', 'left');
        $this->db->where('m_user.email',$username);
        $this->db->where('m_user.`password`',$password);
        $whereQuery = array('m_user.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->row();
        // var_dump($this->db->last_query());
        // exit;
        return $query;   
    }
 
    public function update($where, $data)    
    {    
        $this->db->update('m_user', $data, $where);    
        return $this->db->affected_rows();    
    }
}