<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_kursus extends CI_Model {   

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_by_id_murid($id_murid)
    {
        $this->db->select('
                `t_join_kursus`.`id_join_kursus`, 
                `m_murid`.`id_murid`, 
                `m_user`.`id_user`, 
                `m_kursus`.`id_kursus`, 
                `m_mentor`.`id_mentor`, 
                `m_sub_kategori`.`id_sub_kategori`, 
                `m_kategori`.`id_kategori`,
                `m_user`.`nama_user`, 
                `m_user`.`no_hp`, 
                `m_user`.`email`, 
                `m_user`.`alamat`, 
                `m_user`.`tanggal_lahir`, 
                `m_user`.`agama`, 
                `m_user`.`jenis_kelamin`, 
                `m_user`.`path_foto`, 
                `m_kursus`.`nama_kursus`, 
                `m_kursus`.`card_kursus`,
                `m_kategori`.`nama_kategori`,
                `m_kategori`.`icon`,
                `m_kategori`.`icon_web`,
                `m_kategori`.`background`,
                `m_sub_kategori`.`nama_sub_kategori`,
                `m_sub_kategori`.`icon`,
                `m_sub_kategori`.`icon_web`,
                `m_sub_kategori`.`background`
            ');
        $this->db->from('t_join_kursus');
        $this->db->join('m_murid', 'm_murid.id_murid = t_join_kursus.id_murid');
        $this->db->join('m_user', 'm_user.id_user = m_murid.id_user');
        $this->db->join('m_kursus', 'm_kursus.id_kursus = t_join_kursus.id_kursus');
        $this->db->join('m_mentor', 'm_mentor.id_mentor = m_kursus.id_mentor');
        $this->db->join('m_sub_kategori', 'm_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori');
        $this->db->join('m_kategori', 'm_kategori.id_kategori = m_sub_kategori.id_sub_kategori');
        $this->db->where('m_murid.id_murid',$id_murid);
        $whereQuery = array('m_user.status' => '1','t_join_kursus.status' => '1','m_murid.status' => '1','m_mentor.status' => '1','m_kursus.status' => '1','m_sub_kategori.status' => '1','m_kategori.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }

    public function get_by_id_murid_id_kursus($id_murid, $id_kursus)
    {
        $this->db->select('
                `t_join_kursus`.`id_join_kursus`, 
                `m_murid`.`id_murid`, 
                `m_user`.`id_user`, 
                `m_kursus`.`id_kursus`, 
                `m_mentor`.`id_mentor`, 
                `m_sub_kategori`.`id_sub_kategori`, 
                `m_kategori`.`id_kategori`,
                `m_user`.`nama_user`, 
                `m_user`.`no_hp`, 
                `m_user`.`email`, 
                `m_user`.`alamat`, 
                `m_user`.`tanggal_lahir`, 
                `m_user`.`agama`, 
                `m_user`.`jenis_kelamin`, 
                `m_user`.`path_foto`, 
                `m_kursus`.`nama_kursus`, 
                `m_kursus`.`card_kursus`,
                `m_kategori`.`nama_kategori`,
                `m_kategori`.`icon`,
                `m_kategori`.`icon_web`,
                `m_kategori`.`background`,
                `m_sub_kategori`.`nama_sub_kategori`,
                `m_sub_kategori`.`icon`,
                `m_sub_kategori`.`icon_web`,
                `m_sub_kategori`.`background`
            ');
        $this->db->from('t_join_kursus');
        $this->db->join('m_murid', 'm_murid.id_murid = t_join_kursus.id_murid');
        $this->db->join('m_user', 'm_user.id_user = m_murid.id_user');
        $this->db->join('m_kursus', 'm_kursus.id_kursus = t_join_kursus.id_kursus');
        $this->db->join('m_mentor', 'm_mentor.id_mentor = m_kursus.id_mentor');
        $this->db->join('m_sub_kategori', 'm_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori');
        $this->db->join('m_kategori', 'm_kategori.id_kategori = m_sub_kategori.id_sub_kategori');
        $this->db->where('m_murid.id_murid',$id_murid);
        $this->db->where('m_kursus.id_kursus',$id_kursus);
        $whereQuery = array('m_user.status' => '1','t_join_kursus.status' => '1','m_murid.status' => '1','m_mentor.status' => '1','m_kursus.status' => '1','m_sub_kategori.status' => '1','m_kategori.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }

    public function get_top()
    {
        $this->db->select('
                `m_user`.`id_user`, 
                `m_kursus`.`id_kursus`, 
                `m_mentor`.`id_mentor`, 
                `m_sub_kategori`.`id_sub_kategori`, 
                `m_kategori`.`id_kategori`, 
                `m_user`.`nama_user`, 
                `m_user`.`no_hp`, 
                `m_user`.`email`, 
                `m_user`.`alamat`, 
                `m_user`.`tanggal_lahir`, 
                `m_user`.`agama`, 
                `m_user`.`jenis_kelamin`, 
                `m_user`.`path_foto`, 
                `m_kursus`.`nama_kursus`, 
                `m_kursus`.`card_kursus`, 
                `m_kategori`.`nama_kategori`, 
                `m_kategori`.`icon`, 
                `m_kategori`.`icon_web`, 
                `m_kategori`.`background`, 
                `m_sub_kategori`.`nama_sub_kategori`, 
                `m_sub_kategori`.`icon`, 
                `m_sub_kategori`.`icon_web`, 
                `m_sub_kategori`.`background` 
            ');
        $this->db->from('m_kursus');
        $this->db->join('m_mentor', 'm_mentor.id_mentor = m_kursus.id_mentor');
        $this->db->join('m_user', 'm_user.id_user = m_mentor.id_user');
        $this->db->join('m_sub_kategori', 'm_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori');
        $this->db->join('m_kategori', 'm_kategori.id_kategori = m_sub_kategori.id_sub_kategori');
        $whereQuery = array('m_user.status' => '1','m_mentor.status' => '1','m_kursus.status' => '1','m_sub_kategori.status' => '1','m_kategori.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }

    public function get_by_id_kategori($id_kategori)
    {
        $this->db->select('
                `m_user`.`id_user`, 
                `m_kursus`.`id_kursus`, 
                `m_mentor`.`id_mentor`, 
                `m_sub_kategori`.`id_sub_kategori`, 
                `m_kategori`.`id_kategori`, 
                `m_user`.`nama_user`, 
                `m_user`.`no_hp`, 
                `m_user`.`email`, 
                `m_user`.`alamat`, 
                `m_user`.`tanggal_lahir`, 
                `m_user`.`agama`, 
                `m_user`.`jenis_kelamin`, 
                `m_user`.`path_foto`, 
                `m_kursus`.`nama_kursus`, 
                `m_kursus`.`card_kursus`, 
                `m_kategori`.`nama_kategori`, 
                `m_kategori`.`icon`, 
                `m_kategori`.`icon_web`, 
                `m_kategori`.`background`, 
                `m_sub_kategori`.`nama_sub_kategori`, 
                `m_sub_kategori`.`icon`, 
                `m_sub_kategori`.`icon_web`, 
                `m_sub_kategori`.`background` 
            ');
        $this->db->from('m_kursus');
        $this->db->join('m_mentor', 'm_mentor.id_mentor = m_kursus.id_mentor');
        $this->db->join('m_user', 'm_user.id_user = m_mentor.id_user');
        $this->db->join('m_sub_kategori', 'm_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori');
        $this->db->join('m_kategori', 'm_kategori.id_kategori = m_sub_kategori.id_sub_kategori');
        $this->db->where('m_kategori.id_kategori',$id_kategori);
        $whereQuery = array('m_kursus.status' => '1','m_sub_kategori.status' => '1','m_kategori.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }

    public function get_by_id($id_kursus)
    {
        $this->db->select('
                `t_join_kursus`.`id_join_kursus`, 
                `m_murid`.`id_murid`, 
                `m_user`.`id_user`, 
                `m_kursus`.`id_kursus`, 
                `m_mentor`.`id_mentor`, 
                `m_sub_kategori`.`id_sub_kategori`, 
                `m_kategori`.`id_kategori`,
                `m_user`.`nama_user`, 
                `m_user`.`no_hp`, 
                `m_user`.`email`, 
                `m_user`.`alamat`, 
                `m_user`.`tanggal_lahir`, 
                `m_user`.`agama`, 
                `m_user`.`jenis_kelamin`, 
                `m_user`.`path_foto`, 
                `m_kursus`.`nama_kursus`, 
                `m_kursus`.`card_kursus`,
                `m_kategori`.`nama_kategori`,
                `m_kategori`.`icon`,
                `m_kategori`.`icon_web`,
                `m_kategori`.`background`,
                `m_sub_kategori`.`nama_sub_kategori`,
                `m_sub_kategori`.`icon`,
                `m_sub_kategori`.`icon_web`,
                `m_sub_kategori`.`background`
            ');
        $this->db->from('t_join_kursus');
        $this->db->join('m_murid', 'm_murid.id_murid = t_join_kursus.id_murid');
        $this->db->join('m_user', 'm_user.id_user = m_murid.id_user');
        $this->db->join('m_kursus', 'm_kursus.id_kursus = t_join_kursus.id_kursus');
        $this->db->join('m_mentor', 'm_mentor.id_mentor = m_kursus.id_mentor');
        $this->db->join('m_sub_kategori', 'm_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori');
        $this->db->join('m_kategori', 'm_kategori.id_kategori = m_sub_kategori.id_sub_kategori');
        $this->db->where('m_kursus.id_kursus',$id_kursus);
        $whereQuery = array('m_user.status' => '1','t_join_kursus.status' => '1','m_murid.status' => '1','m_mentor.status' => '1','m_kursus.status' => '1','m_sub_kategori.status' => '1','m_kategori.status' => '1');
        $this->db->where($whereQuery);
        $query = $this->db->get()->row();
        // var_dump($this->db->last_query());
        // exit;
        return $query;
    }

    // public function cek_by_id_murid_id_matapelajaran($id_matapelajaran, $id_murid)
    // {
    //     $this->db->select('
    //             `m_student`.`id_student`, 
    //             `t_kursus`.`id_kursus`, 
    //             `m_materi`.`id_materi`, 
    //             `m_mentor`.`id_mentor`,
    //             `m_materi`.`materi`,
    //             `m_materi`.`harga`, 
    //             `m_user`.`nama`
    //         ');
    //     $this->db->from('t_kursus');
    //     $this->db->join('m_student', '`m_student`.`id_student` = `t_kursus`.`id_student`');
    //     $this->db->join('m_user', 'm_user.id_user = m_student.id_user');
    //     $this->db->join('m_materi', '`m_materi`.`id_materi` = `t_kursus`.`id_materi`' );
    //     $this->db->join('m_mentor', '`m_mentor`.`id_mentor` = `m_materi`.`id_mentor`');
    //     // $this->db->join('m_kategori', '`m_kategori`.`id_kategori` = `m_materi`.`id_kategori`');
    //     $this->db->where('t_kursus.status','1');
    //     $this->db->where('t_kursus.id_student',$id_murid);
    //     $this->db->where('t_kursus.id_materi',$id_matapelajaran);
    //     $query = $this->db->get()->result()
    //     // var_dump($this->db->last_query());
    //     // exit
    //     return $query;
    // }

    public function save($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    } 

}