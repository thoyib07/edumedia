<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Model_service_edumedia extends CI_Model {



    public function __construct() {



        $this->db_edumedia = $this->load->database('default', TRUE);

    }



    public function get_all_kategori_aktif(){



    	$sql = "SELECT * FROM m_kategori where status = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_all_sub_kategori_aktif_by_kategori($id_kategori){



    	$sql = "SELECT * FROM m_sub_kategori where id_kategori = '$id_kategori' and status = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_kursus_aktif_by_sub_kategori($id_sub_kategori){



    	$sql = "SELECT * FROM `m_kursus` 

				JOIN m_sub_kategori on m_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori 

				JOIN m_kategori on m_sub_kategori.id_kategori = m_kategori.id_kategori

				join m_mentor on m_mentor.id_mentor = m_kursus.id_mentor

				JOIN m_user on m_mentor.id_user = m_user.id_user

				WHERE m_kursus.id_sub_kategori = '$id_sub_kategori' and m_kursus.status = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_soal_by_id_kursus($id_kursus){



    	$sql = "SELECT m_kursus.*, m_sub_kategori.*, m_kategori.*, m_mentor.*, m_user.id_user, m_user.nama_user  FROM `m_kursus` 

				JOIN m_sub_kategori on m_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori 

				JOIN m_kategori on m_sub_kategori.id_kategori = m_kategori.id_kategori

				join m_mentor on m_mentor.id_mentor = m_kursus.id_mentor

				JOIN m_user on m_mentor.id_user = m_user.id_user

				WHERE m_kategori.id_kategori = '".$id_kursus."' and m_kursus.status = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_kursus_aktif_by_kategori($id_kategori){



    	$sql = "SELECT m_kursus.*, m_sub_kategori.*, m_kategori.*, m_mentor.*, m_user.id_user, m_user.nama_user  FROM `m_kursus` 

				JOIN m_sub_kategori on m_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori 

				JOIN m_kategori on m_sub_kategori.id_kategori = m_kategori.id_kategori

				join m_mentor on m_mentor.id_mentor = m_kursus.id_mentor

				JOIN m_user on m_mentor.id_user = m_user.id_user

				WHERE m_kategori.id_kategori = '$id_kategori' and m_kursus.status = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_materi_aktif_by_kursus($id_kursus){



    	$sql = "SELECT m_materi.*, m_kategori.*, m_user.nama_user, m_kategori.* FROM `m_materi` 

				join m_sesi on m_sesi.id_sesi = m_materi.id_sesi

				join m_kursus on m_kursus.id_kursus = m_sesi.id_kursus

				join m_sub_kategori on m_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori

				join m_kategori on m_kategori.id_kategori = m_sub_kategori.id_kategori

				join m_mentor on m_mentor.id_mentor = m_kursus.id_mentor

				join m_user on m_user.id_user = m_mentor.id_user

				WHERE m_kursus.id_kursus = '$id_kursus' and m_materi.status_verified = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_soal_kuis_by_materi($id_materi){



    	$sql = "SELECT m_materi.id_materi, m_materi.nama_materi, m_soal_kuis.id_soal_kuis, m_soal_kuis.soal_kuis, m_soal_kuis.id_pilihan_jawaban as id_jawaban_benar, m_pilihan_jawaban.pilihan_jawaban as jawaban_benar  FROM `m_materi` 

				join m_soal_kuis on m_soal_kuis.id_materi = m_materi.id_materi 

				join m_pilihan_jawaban on m_soal_kuis.id_pilihan_jawaban = m_pilihan_jawaban.id_pilihan_jawaban 

    			WHERE m_materi.id_materi = '$id_materi'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_pilihan_jawaban_by_id_soal_kuis($id_soal_kuis){



    	$sql = "SELECT m_pilihan_jawaban.id_pilihan_jawaban, m_pilihan_jawaban.pilihan_jawaban from m_soal_kuis  

				join m_pilihan_jawaban on m_pilihan_jawaban.id_soal_kuis = m_soal_kuis.id_soal_kuis 

    			WHERE m_soal_kuis.id_soal_kuis = '$id_soal_kuis'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_kursus_saya_by_id_murid($id_murid){



    	$sql = "SELECT t_join_kursus.*, m_kursus.*, m_mentor.*, m_user.id_user, m_user.nama_user FROM `t_join_kursus`

				join m_kursus on m_kursus.id_kursus = t_join_kursus.id_kursus

				join m_mentor on m_mentor.id_mentor = m_kursus.id_mentor

				join m_user on m_mentor.id_user = m_user.id_user

    			WHERE t_join_kursus.id_murid = '$id_murid'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_total_materi_by_id_kursus($id_kursus){



    	$sql = "SELECT COUNT(m_kursus.id_kursus) AS total_materi FROM `m_kursus`

				join m_sesi on m_sesi.id_kursus = m_kursus.id_kursus

				join m_materi on m_materi.id_materi = m_sesi.id_sesi

				WHERE m_kursus.id_kursus = '$id_kursus'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    public function get_soal_by_id_soal_id_jawaban($id_soal, $id_jawaban){



    	$sql = "SELECT * FROM `m_soal_kuis`

				WHERE id_soal_kuis = '$id_soal'

				AND id_pilihan_jawaban = '$id_jawaban'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    public function get_materi_selesai_by_join_kursus($id_join_kursus){



    	$sql = "SELECT COUNT(t_belajar.id_belajar) AS total_materi_selesai FROM `t_belajar`

				join t_join_kursus on t_join_kursus.id_join_kursus = t_belajar.id_join_kursus

				where t_join_kursus.id_join_kursus = '$id_join_kursus'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    public function login($email, $password){



    	$sql = "SELECT *, m_murid.status as status_verified FROM `m_murid`

				join m_user on m_user.id_user = m_murid.id_user

				WHERE m_user.email = '$email' and m_user.`password` = '$password'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    function input_data($namaTable, $insert_to){



        $insert = $this->db_edumedia->insert($namaTable, $insert_to);



        return $insert; 

    }



    public function update_data($tableName, $data, $where) {



        $sql = $this->db_edumedia->update($tableName, $data, $where);



        return $sql;



    }



    public function cek_email($email){



    	$sql = "SELECT * FROM `m_user`s

				WHERE email = '$email'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    public function get_profil($id_user){



    	$sql = "SELECT * FROM `m_user`

				where id_user = '$id_user' and status = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    public function get_poin_edupay($id_murid){



    	$sql = "SELECT SUM(poin) as poin from t_join_kursus WHERE id_murid = '$id_murid'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    public function get_jawaban_by_id_soal_id_murid($id_soal, $id_murid){



    	$sql = "SELECT * FROM `t_jawaban_kuis` JOIN m_soal_kuis ON m_soal_kuis.id_soal_kuis = t_jawaban_kuis.id_soal_kuis 

				WHERE t_jawaban_kuis.id_soal_kuis = '$id_soal' AND t_jawaban_kuis.id_murid = '$id_murid'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



	public function top_materi(){



    	$sql = "SELECT m_kursus.*, m_kategori.*, m_user.nama_user, COUNT(t_join_kursus.id_kursus) as peserta 

    	FROM t_join_kursus

		join m_kursus on m_kursus.id_kursus = t_join_kursus.id_kursus

		JOIN m_sub_kategori on m_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori 

		JOIN m_kategori on m_sub_kategori.id_kategori = m_kategori.id_kategori

		join m_mentor on m_mentor.id_mentor = m_kursus.id_mentor

		JOIN m_user on m_mentor.id_user = m_user.id_user

		GROUP BY t_join_kursus.id_kursus

		ORDER BY peserta DESC";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



	public function get_point_by_id_murid_id_materi($id_murid, $id_materi){



    	$sql = "SELECT * FROM `t_jawaban_kuis`

				JOIN m_soal_kuis ON m_soal_kuis.id_pilihan_jawaban =t_jawaban_kuis.id_jawaban_kuis

				JOIN m_materi ON m_materi.id_materi = m_soal_kuis.id_materi

				JOIN m_murid ON m_murid.id_murid = t_jawaban_kuis.id_murid

				WHERE m_murid.id_murid = '$id_murid' AND m_materi.id_materi = '$id_materi'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function get_all_kursus(){



    	$sql = "SELECT m_kursus.*, m_sub_kategori.*, m_kategori.*, m_mentor.*, m_user.id_user, m_user.nama_user  FROM `m_kursus` 

				JOIN m_sub_kategori on m_sub_kategori.id_sub_kategori = m_kursus.id_sub_kategori 

				JOIN m_kategori on m_sub_kategori.id_kategori = m_kategori.id_kategori

				join m_mentor on m_mentor.id_mentor = m_kursus.id_mentor

				JOIN m_user on m_mentor.id_user = m_user.id_user

				WHERE m_kursus.status = '1'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->result_array();



        return $row;



    }



    public function cek_join_kursus($id_murid, $id_kursus){



    	$sql = "SELECT * from t_join_kursus WHERE id_murid = '$id_murid' and id_kursus = '$id_kursus'";



        $query = $this->db_edumedia->query($sql);



        $row = $query->row_array();



        return $row;



    }



    

    

    







    

}