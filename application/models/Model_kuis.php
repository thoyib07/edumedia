<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_kuis extends CI_Model {   

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function update($table, $where, $data)
    {
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    public function save($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function get_by_id_materi($id_sub_materi)
    {
        $this->db->from('m_soal_kuis');
        $this->db->where('id_materi',$id_sub_materi);
        $this->db->where('status', '1');
        $query = $this->db->get()->result();
        // var_dump($this->db->last_query());
        // exit;  
        return $query;
    }

    public function get_by_id_soal_id_jawaban($id_soal, $id_jawaban)
    {
        $this->db->from('m_soal_kuis');
        $this->db->where('id_soal_kuis', $id_soal);
        $this->db->where('id_pilihan_jawaban', $id_jawaban);
        $this->db->where('status', '1');
        $query = $this->db->get()->row();
        // var_dump($this->db->last_query());
        // exit;  
        return $query;
    }

    public function get_jawaban_by_id_soal($id_soal_kuis)
    {
        $this->db->from('m_pilihan_jawaban');
        $this->db->where('id_soal_kuis',$id_soal_kuis);        
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    } 

}