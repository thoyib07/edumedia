<?php defined('BASEPATH') OR exit('No direct script access allowed.');

/*
 * @author      IHSAN MAULANA iisandesign@gmail.com
 * @copyright   Copyright (c) 2017, KOTA TANGERANG 
 */

global $rest_u,$rest_p;
$rest_u = 'r35t51kd4';
$rest_p = '5ksnpcua5x6z79yk5xgbtkg89a4zdwc8ym7p2f4z'; 

function get_login_skpd($username, $password)
{
	$CI =& get_instance();	
	$CI->load->library('curl');		 
	$CI->curl->create('http://opendatav2.tangerangkota.go.id/services/auth/login/uid/'.$username.'/pid/'.$password);		 
	$CI->curl->http_login($GLOBALS['rest_u'], $GLOBALS['rest_p']);	
	$result = json_decode($CI->curl->execute(),true);
	
	return $result;
}

function send_email($setting){
	$CI = get_instance();
	// var_dump($data->detail[0]);
	// die("masuk helper send_email");
	if (isset($setting->alias_from)) {
		$CI->email->from($setting->email_from, $setting->alias_from);
	} else {
		$CI->email->from($setting->email_from);
	}

	if (isset($setting->reply_to)) {
		$CI->email->reply_to($setting->email_from, $setting->alias_from);
	}
	
	$CI->email->to($setting->email_to);

	$CI->email->subject($setting->subject);
	$CI->email->message($setting->message);

	if ($CI->email->send(FALSE)) {
		return TRUE;
	} else {
		// echo ($CI->email->print_debugger(array('header')));
		var_dump($CI->email->print_debugger(array('header')));
		return FALSE;
	}
}

?>