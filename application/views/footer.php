<footer style="background:url('<?=base_url();?>assets/images/BG IMAGE FOOTER.jpg') no-repeat fixed center; background-size: cover;">
    <div class="container margin_120_95" style="padding-top: 0px;padding-bottom: 12px;">
        <div class="row">
            <div class="col-lg-3 col-md-12 p-r-5" style="text-align: center;padding-top: 2%;">
                <p style="padding-top: 3vh;"><img src="<?=base_url();?>assets/images/bird_berdiri_text_putih_new.png" data-retina="true" alt="" style="" width="130"></p>
                <h5 style="margin-bottom: 4px;">Coming Soon On</h5>
                <p style="margin-bottom: 0px;"><img src="<?=base_url();?>assets/images/google_play.png" data-retina="true" alt="" style="" width="130"></p>
                <p><img src="<?=base_url();?>assets/images/app_store.png" data-retina="true" alt="" style="" width="130"></p>
            </div>
            <div class="col-lg-2 col-md-6 ml-lg-auto " >
                <h5>Site Map</h5>
                <ul class="links">
                    <li style="display: none"><a href="javascript:void(0)">Programs</a></li>
                    <li style="display: none"><a href="javascript:void(0)">Student Testimony</a></li>
                    <li style="display: none"><a href="javascript:void(0)">Internship</a></li>
                    <li style="display: none"><a href="javascript:void(0)">Competition</a></li>
                    <li style="display: none"><a href="javascript:void(0)">Edufest</a></li>
                    <li style="display: none"><a href="javascript:void(0)">Partner</a></li>
                    <li><a href="<?= base_url(); ?>tentang_kami">About</a></li>
                    <li style="display: none"><a href="javascript:void(0)">Become Mentor</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 ml-lg-auto " style="">
                <h5>Metode</h5>
                <ul class="links">
                    <li><a href="<?= base_url() ?>metode/#lms">LMS</a></li>
                    <li><a href="<?= base_url() ?>metode/#video_learn">Video Belajar</a></li>
                    <li><a href="<?= base_url() ?>metode/#gamification">Gamification</a></li>
                    <li><a href="<?= base_url() ?>metode/#video_conf">Video Conference</a></li>
                    <li class="d-none"><a href="javascript:void(0)">Chat Mentor</a></li>
                    <li class="d-none"><a href="javascript:void(0)">Encounter</a></li>
                    <li class="d-none"><a href="javascript:void(0)">Community</a></li>
                    <li class="d-none"><a href="javascript:void(0)">Social Club</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 ml-lg-auto" style="display: none;">
                <h5>Follow Us</h5>
                <ul class="links">
                    <li><a href="https://instagram.com/edumediaid_?igshid=w8c339eet34m" target="_blank"><i><img src="<?=base_url();?>assets/images/IG.png" class="img-fluid" alt="" width="20"></i>Instagram</a></li>
                    <li><a href="https://facebook.com/edumedia.id" target="_blank"><i><img src="<?=base_url();?>assets/images/FB.png" class="img-fluid" alt="" width="20"></i>Facebook</a></li>
                    <li><a href="https://twitter.com/EdumediaID_?s=08" target="_blank"><i><img src="<?=base_url();?>assets/images/TW.png" class="img-fluid" alt="" width="20"></i>Twitter</a></li>
                    <li><a href="https://www.youtube.com/channel/UCXCyE8qjfbaYiLqvHyFanZg" target="_blank"><i><img src="<?=base_url();?>assets/images/YT.png" class="img-fluid" alt="" width="20"></i>Youtube</a></li>
                    <li><a href="https://open.spotify.com/show/73ahpf5NiQvLeGbLICKZhg?si=EFRTjXEKT2K1ZEH4xSyvGQ" target="_blank"><i><img src="<?=base_url();?>assets/images/spotify.png" class="img-fluid" alt="" width="20"></i> Spotify</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 ml-lg-auto">
                <h5>Address</h5>
                <ul class="contacts">
                    <li><a href="https://www.google.com/maps/place/Edumedia.id/@-6.2477219,106.9095192,17.72z/data=!4m8!1m2!2m1!1sedumedia!3m4!1s0x2e69f3483b46a3a9:0x7656e350e09e28ef!8m2!3d-6.2481!4d106.910038">Jl. Inpesksi Kalimalang Puri Sentra Niaga E/72 2, 3, 4 Floor, Jakarta Timur, 13620</a></li>
                </ul>
                <h5>Contact</h5>
                <ul class="contacts">
                    <li><a href="tel://+6281285232001"><i class="ti-mobile"></i> + 62 812 8523 2001</a></li>
                    <li><a href="mailto:info@edumedia.id"><i class="ti-email"></i> info@edumedia.id</a></li>
                    <li><a href="https://api.whatsapp.com/send?phone=6281285232001&text=Hello%20Edumedia&source=&data="><i><img src="<?=base_url();?>assets/images/WA.png" class="img-fluid" alt="" style="margin-right: -3px;" width="17"></i> + 62 812 8523 2001</a></li>
                </ul>
            </div>
        </div>
        <!--/row-->
        <hr style="margin: 24px 0 17px 0;">
    </div>
    <div class="row pt-5 pb-5 mx-0 text-white" style="background:#361faa70">
        <div class="col-12 text-center">
            <span>Follow Us On Social Media</span>
        </div>
        <div class="col-12 text-center">
            <a href="https://instagram.com/edumediaid_?igshid=w8c339eet34m" target="_blank" class="mx-3"><i><img src="<?=base_url();?>assets/images/IG.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://facebook.com/edumedia.id" target="_blank" class="mx-3"><i><img src="<?=base_url();?>assets/images/FB.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://twitter.com/EdumediaID_?s=08" target="_blank" class="mx-3"><i><img src="<?=base_url();?>assets/images/TW.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://www.youtube.com/channel/UCXCyE8qjfbaYiLqvHyFanZg" target="_blank" class="mx-3"><i><img src="<?=base_url();?>assets/images/YT.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://open.spotify.com/show/73ahpf5NiQvLeGbLICKZhg?si=EFRTjXEKT2K1ZEH4xSyvGQ" target="_blank" class="mx-3"><i><img src="<?=base_url();?>assets/images/spotify.png" class="img-fluid" alt="" width="20"></i></a>
        </div>
    </div>
    <div class="row py-2 text-white" style="background: #0b0015;margin: auto;">
        <div class="col-md-8">
            <ul id="additional_links">
                <li><a href="javascript:void(0)">Terms and conditions</a></li>
                <li><a href="javascript:void(0)">Privacy</a></li>
            </ul>
        </div>
        <div class="col-md-4 ">
            <div id="copy" class="text-right">© 2020 EDUMEDIA.ID</div>
        </div>
    </div>
</footer>