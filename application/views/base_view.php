<!DOCTYPE html>

<?php include('head.php');?>
<html lang="en">

<body style="height: 100%;">
    <?php 
        // var_dump($_SESSION);
    ?>
    <div id="page">

    <!-- HEADER MENU START -->
    <?php echo $header_menu;?>
    <!-- HEADER MENU END -->
    
    <!-- CONTENT STAR -->
    <?PHP echo $content?>
    <!-- CONTENT END -->

    <!-- FOOTER STAR -->
    <?PHP echo $footer?>
    <!-- FOOTER END -->

    </div>
    <!-- page -->

    <?php include('jquery.php');?>
    
</body>
</html>