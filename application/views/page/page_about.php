<style>
  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }
  #myImg:hover {opacity: 0.7;}
  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }
  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }
  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }
  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }
  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }
  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }
  /* The Close Button */
  .close {
    position: absolute;
    top: 67px;
    right: 7px;
    color: #ffffff;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }
  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }
  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
  div .btn_mobile {
    display: none !important;
  }
  @media screen and (max-width: 600px) {
    #mobile_show_yow {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
    #mobile_show_yow_1 {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
  }
</style>
<main style="background-color: #0c0016;padding-top: 3%;">
    <div class="main_title_2 hidden_tablet" style="margin-bottom: 15px;">
        <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;max-width: 100%;">
            <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #0b0015; height: 100%;">
                <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: fill;padding-right: unset;padding-left: unset;" src="<?= base_url(); ?>assets/images/banner about us-01.png">
                <div class="" style="text-align: left;margin-top: 0%;margin-right: 5%;">
                    <div class="" style="padding-left: 60%;">
                        <h2 style="position: relative;color: #fff;">Filosofi Edumedia</h2>
                        <p style="position: relative;">Sebagai pendukung bagi pendidik dan siswa yang bebas untuk menjadi kreatif dan mendapat ilmu yang berkualitas dari cara rumit berfikir menjadi mudah</p>
                        <!-- <a style="margin-top: 3%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.8rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?= base_url(); ?>Registration" class="btn_1 rounded">Mulai Belajar</a> -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
        <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;">
            <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #37386f;height: 98%;">
                <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: cover;padding-right: unset;padding-left: unset;" src="<?= base_url(); ?>assets/images/banner about us-01.png">
                <div class="" style="text-align: initial;padding-right: 10%;padding-left: 5%;padding-top: 0%;">
                    <div class="" >
                        <h2 style="position: relative;color: #fff;font-size: 1.3rem;margin: 0px 0 0 0;">Gali potensi skill mu,<br> raih suksess karir mu</h2>
                        <p style="position: relative;font-size: 1rem;">Belajar dengan mentor terbaik<br> di edumedia.</p>
                        <!-- <a style="margin-top: 3%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.8rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="http://edumedia.id/injeksi_registrasi" class="btn_1 rounded">Mulai Belajar</a> -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px; padding-bottom: 0px;">
        <!-- <hr style="margin: 24px 0 17px 0;"> -->
        <h2 style="color: #ffffff;margin-bottom: 5%; margin-top: 6%; text-align: center;">Fitur-fitur Edumedia.id</h2>
    </div>
    <div class="p-5 mx-auto my-5" style="grid-gap: 10px;background-color: #171219;max-width: 90% !important;">
        <div class="row" style="display: flex; flex-wrap: wrap;">
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/lms_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Berbasis LMS</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Mendistribusikan program pelatihan dan pendidikan melalui internet dan berbasis online dengan berbagai fitur yang ada pada edumedia.id</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/gamifikasi_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Gamification</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Memotivasi siswa belajar dengan menggunakan desain video game dan elemen-elemen dalam lingkungan belajar</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/scoring_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Real TIme Scoring</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Nilai dan capaian siswa secara langsung akan terpantau oleh mentor diwaktu yang bersamaan melalui apps</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/encounter_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Encounter</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Student bertanggung jawab dari hasil belajar online-nya, bisa melalui tatap muka atau video conference</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/chat_mentor_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Chat Mentor</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Berdiskusi langsung dengan mentor profesional dan bimbingan khusus sesuai dengan kebutuhan insdustri</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/mentor_program_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Mentor Program</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Fitur lanjutan dari program pembelajaran dibuat khusus oleh mentor untuk mendapatkan tips dan trik secara singkat dan dengan metode boarding</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/student_project_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Student Project</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Project Base adalah inisiatif siswa untuk membuatnya proyek untuk teknik ilmiah dan menjadi produk.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/sertifikat_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Sertifikasi</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Tempat belajar online kejuruan di setiap kota yang menjadi basecamp siswa dengan instruktur pendamping yang telah disertifikasi sebagai mitra edumedia</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4" >
                <div class="row">
                    <div class="col-lg-2">
                        <img class="mx-auto d-block py-1"  src="<?= base_url(); ?>assets/images/edubank_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" >
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">EduBank</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Siswa dan mentor dapat mengumpulkan poin sebagai nilai tukar mereka.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container margin_30_95" style="text-align: center;">
        <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Menghubungkan siswa di seluruh indonesia dengan instruktur terbaik,</p>
        <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Edumedia.id membantu individu mencapai tujuan mereka dan mengejar impian mereka.</p>
    </div> -->
    <!-- Tentang Kami Section -->
    <section id="Tentang" class="kami" style="width: 100%; background-color: #06052f; ">
        <div class="container py-5" style="grid-gap: 10px; max-width: 90% !important;">
            <!-- <hr style="margin: 24px 0 17px 0;"> -->
            <h2 style="color: #ffffff;margin-bottom: 5%; text-align: center; padding-top: 70px">Tentang Kami</h2>
            <div class="row">            
                <div class="col-lg-6 order-md-2" >
                    <div>
                        <img class="w-100 px-5"  src="<?= base_url(); ?>assets/images/content_thumb.png">
                    </div>
                </div>
                <div class="col-lg-6 px-5 order-md-1 py-5" >
                    <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 13px;">Edumedia dimulai pada tahun 2014, ketika PT Holomoc Indonesia menerima magang, sebuah perusahaan yang menjadi fokus multimedia IT, memiliki 15 lulusan magang. 1 batch 30 orang, banyak magang yang diperoleh meningkat keterampilan untuk sekolah mereka. Pendaftaran pemagangan terlalu tinggi, perusahaan membuat peraturan, dan jumlah peserta maksimal adalah 15 per kelas, dengan peserta yang telah diseleksi secara ketat.  Kemudian ide untuk membuat platform pendidikan pertama kali diwujudkan dengan membuat kursus singkat untuk magang, dan dilanjutkan dengan membuat aplikasi pendidikan untuk menjembatani pertemuan siswa dan guru secara online oleh membuat video pembelajaran yang biasa kami sajikan, dan membuat tugas proyek sesuai industri standar.</p>
                    <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 13px;">Kami melakukan penelitian tentang kursus online terbuka besar-besaran (MOOC) dengan kebutuhan industri saat ini untuk Milenial Indonesia, kami belajar mengajar di sekolah kejuruan, perguruan tinggi dan universitas. Belajar di online universitas, ambil kursus online dan offline di beberapa platform kursus online. Setelah  penelitian kami menyimpulkan pembuatan aplikasi pendidikan berbasis LMS, gamification, real time skor, rapat, Edu-Bank, dana kerumunan, mentor obrolan, proyek Mahasiswa, Program Mentor, Magang dan lain-lain.</p>
                    <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 13px;">Kami membuat aplikasi untuk memulai berdasarkan pengalaman industri dalam mengumpulkanpengetahuan kami, tentang IT, Game, Media Sosial, Pemasaran Digital, dan manajemen. Jadi nilai jual unik edumedia adalah gabungan Platform Pendidikan, Fintech dan Crowd Funding. Membuat siswa belajar dengan mentoring dari industri.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- === Team Section === -->
    <section id="team" class="kita p-5" style="width: 100%;background-color: #0c0016; ">
        <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px; padding-bottom: 0px;">
            <!-- <hr style="margin: 24px 0 17px 0;"> -->
            <h2 style="color: #ffffff;margin-bottom: 5%; text-align: center; padding-top: 70px">Team Kami</h2>
        </div>
        <div class="container pt-1" style="grid-gap: 10px; max-width: 90% !important;">
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Pak chan.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Chandra <abbr title="Kirana MNG">KMNG</abbr></h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Design Thinking</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Chandra@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">                    
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Pak Dirgantara.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Jaha</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Staff Ahli / Pembimbing Teknologi Pendidikan</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Jahaa@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">                    
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Pak Dirgantara.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Dr.Dirgantara Wicaksono, CH, Cht, S.Pd, M.Pd,MM</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Staff Ahli / Pembimbing Teknologi Pendidikan</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Dr.Dirgantara@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/poto-01.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Mus Mulyadi M.A, M.pd</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Analyst</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Musmulyadi@edu.id</p>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Rizqy.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Rizqy Liquify</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">UI/UX Designer</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Riszqy@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Dio.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Rio Proxy</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Mobile Developer</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">DioHalilintar666@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/poto-01.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Thoyib Hueh</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Web Developer</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Thoyib@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Andini.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Dini Vector</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Video Editor / Socmed</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Dini@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Fika.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Fika Vlowy</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Analyst</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Fika@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/poto-01.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Fitri Farhana M.Pd</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Instructional Design</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Fitri@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/Fadil.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Fadhil Paintool</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Telent / Broadcaster</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">Fadhil@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/poto-01.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Oktha Anchor</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Ilustrator</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">ODP@edu.id</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 p-3" style="border: 7px solid #0c0016;background-color: #58525d;">
                    <div class="row">
                        <!-- <div class="col-lg-4 order-md-2" >
                            <img class="img-fluid" src="<?= base_url(); ?>assets/images/poto-01.png">
                        </div> -->
                        <div class="col-lg-8 order-md-1 py-4" >
                            <h5 style="position: relative;color: #fff; font-size: 22px;">Ravi Dashboard</h5>
                            <p class="m-1" style="position: relative; color: #ea3f61; font-size: 15px;">Support System</p>
                            <p class="m-1" style=" position: relative;color: #fff; font-size: 17px;">RaviUtama@edu.id</p>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </section>
</main>
<script type="text/javascript">
</script>