<style>
  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }
  #myImg:hover {opacity: 0.7;}
  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }
  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }
  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }
  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }
  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }
  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }
  /* The Close Button */
  .close {
    position: absolute;
    top: 67px;
    right: 7px;
    color: #ffffff;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }
  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }
  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
  div .btn_mobile {
    display: none !important;
  }
  @media screen and (max-width: 600px) {
    #mobile_show_yow {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
    #mobile_show_yow_1 {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
      $.ajax({
          url : "<?php echo base_url('kategori/get_all_kategori_aktif')?>",
          type: "GET",
          dataType: "JSON",
          success: function(response){
              response.forEach(function(element) {
                  // $('#kategori_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a href="<?php echo base_url();?>Materi/materi_by_id_materi/'+element.id_materi+'" class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><div class="block-horizzontal" style="animation: none;background: none;"></div><img src="https://img.youtube.com/vi/' + element.tumnile + '/sddefault.jpg" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 0px 0px 3px 6px;animation: color 0.7s ease-in-out; animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: center;"><p style="color: #ffffff;"><b>'+ element.nama_materi +'</b></p></div></figure></a></div>');
                  $('#kategori_show_chat_mentor').append('<div class="col-lg-2 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;text-align: center;margin-bottom: 2%;"><a href="https://api.whatsapp.com/send?phone=6281315343205&text=Hello%20Edumedia.id, Nama Saya <?php echo $this->session->userdata('nama_user');?>, Saya tertarik dengan kurusus yang di selengarakan oleh edumedia, dalam bidang '+response.nama_kategori+'. &source=&data=" target="_blank" class="grid_item"><img src="<?php echo base_url();?>'+  element.icon_app +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 72%;"><p style="color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_kategori +'</p></a></div>');
                  $('#kategori_show_chat_mentor_mobile').append('<div class="col-lg-2 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;text-align: center;margin-bottom: 10%;"><a href="https://api.whatsapp.com/send?phone=6281315343205&text=Hello%20Edumedia.id, Nama Saya <?php echo $this->session->userdata('nama_user');?>, Saya tertarik dengan kurusus yang di selengarakan oleh edumedia, dalam bidang '+response.nama_kategori+'. &source=&data=" target="_blank" class="grid_item"><img src="<?php echo base_url();?>'+  element.icon_app +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 50%;"><p style="color: #fff;font-size: 15px;margin-bottom: unset;">'+ element.nama_kategori +'</p></a></div>');
              });
          },
          error: function (jqXHR, textStatus, errorThrown){
              alert('terjadi kesalahan, coba beberapa saat lagi');
          }
      });
  });
</script>
<main style="background-color: #0c0016;padding-top: 3%;">
  <div class="main_title_2 hidden_tablet" style="margin-bottom: 15px;">
    <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;max-width: 100%;height: 597px;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #0b0015;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: fill;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/Banner_home_edumedia2.png">
        <div class="" style="text-align: left;margin-top: 17%;margin-right: 16%;">
          <div class="" style="padding-left: 14%;">
              <div style="position: relative;">
                  <h2 style="color: #fff;font-size: 53px;">Chat Mentor</h2>
                  <p style="margin-top: 3%;margin-right: 52%;margin-bottom: 35%;">Konsultasi Pelajaran apa yang kamu senangi,<br> Konsultasikan kendala belajar kamu, atau dapatkan pengatahuan baru dari mentor berpengalaman secara privat.</p>
                  <!-- <a style="margin-top: 7%;margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="#" class="btn_1 rounded">Jadilah Mentor</a> -->
              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
    <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #37386f;height: 98%;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: cover;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/Banner_home_edumedia2.png">
        <div class="" style="text-align: initial;padding-right: 10%;padding-left: 10%;padding-top: 4%;">
          <div class="" style="">
              <div style="position: relative;">
                  <h2 style="color: #fff;font-size: 25px;">Chat Mentor</h2>
                  <p style="margin-top: 3%;margin-right: 52%;margin-bottom: 35%;font-size: 0.8rem;">Konsultasi Pelajaran apa yang kamu senangi,<br> Konsultasikan kendala belajar kamu, atau dapatkan pengatahuan baru dari mentor berpengalaman secara privat.</p>
                  <!-- <a style="margin-top: 7%;margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="#" class="btn_1 rounded">Jadilah Mentor</a> -->
              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- ====================== START CONTENT ===================== -->
  <div class="main_title_2 hidden_tablet" style="margin-bottom: unset;">
      <div class="filters_listing" style="margin: 4%;padding: 2%;background: #151219;text-align: center;">
          <h2 style="color: #fff;">Temukan Potensi Anda</h2>
          <div class="row" style="margin-top: 3%;">
              <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                  <img src="<?php echo base_url();?>/assets/images/icon1.png">
                  <!-- <h2 style="color: #fff;margin-top: 2%;">Hasilkan Uang</h2> -->
                  <p style="color: #fff;font-size: 18px;margin-top: 5%;">Chat dengan mentor berpengalaman dan tersetifikasi dengan beragam pilihan profesi dan keahlian di seluruh indonesia</p>
                  <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
              </div>
              <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                  <img src="<?php echo base_url();?>/assets/images/icon2.png">
                  <!-- <h2 style="color: #fff;margin-top: 2%;">Menginspirasi Mereka</h2> -->
                  <p style="color: #fff;font-size: 18px;margin-top: 5%;">Mendapatkan penjelasan dan saran serta tips & trik yang terbaik dari sumber profesional</p>
                  <hr style="margin: 57px 40% 30px 40%;border-color: #6d26b9;">
              </div>
              <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                  <img src="<?php echo base_url();?>/assets/images/icon3.png">
                  <!-- <h2 style="color: #fff;margin-top: 2%;">Jadilah Mentor Kami</h2> -->
                  <p style="color: #fff;font-size: 18px;margin-top: 5%;">Bimbingan sesuai dengan skill yang kamu senangi</p>
                  <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
              </div>
          </div>
      </div>
     <div class="container margin_30_95" style="text-align: center;padding-bottom: 58px;">
        <p style="margin-bottom: 3px;font-size: 181%;color: #ffffff;">Chat dengan spesialis kami</p>
        <p style="margin-bottom: 3px;font-size: 105%;color: #ffffff;">Pilih kategori dan temukan mentor untuk menjawab pertanyaan seputar skill yang kamu miliki</p>
    </div>
    <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px;">
        <!-- <hr style="margin: 24px 0 17px 0;"> -->
        <div class="row" id="kategori_show_chat_mentor" style="padding-right: 15px;padding-left: 15px;">
          <!-- <div class="col-lg-2 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;text-align: center;margin-bottom: 2%;">
            <a href="<?php echo base_url();?>Materi/materi_by_id_materi/1" class="grid_item">
              <img src="http://localhost/spero.id/EDUMEDIA/admin_v5/assets/images/icon_app_kategori/multimedia_xdpi_1.png" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 72%;">
              <p style="color: #fff;font-size: 17px;margin-bottom: unset;">Nama Mentor</p>
            </a>
          </div> -->
        </div>
    </div>
    <!-- <div class="container margin_30_95" style="text-align: center;">
        <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Menghubungkan siswa di seluruh indonesia dengan instruktur terbaik,</p>
        <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Edumedia.id membantu individu mencapai tujuan mereka dan mengejar impian mereka.</p>
    </div> -->
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
      <div class="filters_listing" style="margin: 4%;padding: 2%;background: #151219;text-align: center;">
          <h2 style="color: #fff;font-size: 25px;">Temukan Potensi Anda</h2>
          <div class="row" style="margin-top: 3%;">
              <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                  <img src="<?php echo base_url();?>/assets/images/icon1.png" style="width: 36%;">
                  <!-- <h2 style="color: #fff;margin-top: 2%;">Hasilkan Uang</h2> -->
                  <p style="color: #fff;font-size: 14px;margin-top: 5%;">Chat dengan mentor berpengalaman dan tersetifikasi dengan beragam pilihan profesi dan keahlian di seluruh indonesia</p>
                  <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
              </div>
              <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                  <img src="<?php echo base_url();?>/assets/images/icon2.png" style="width: 36%;">
                  <!-- <h2 style="color: #fff;margin-top: 2%;">Menginspirasi Mereka</h2> -->
                  <p style="color: #fff;font-size: 14px;margin-top: 5%;">Mendapatkan penjelasan dan saran serta tips & trik yang terbaik dari sumber profesional</p>
                  <hr style="margin: 57px 40% 30px 40%;border-color: #6d26b9;">
              </div>
              <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                  <img src="<?php echo base_url();?>/assets/images/icon3.png" style="width: 36%;">
                  <!-- <h2 style="color: #fff;margin-top: 2%;">Jadilah Mentor Kami</h2> -->
                  <p style="color: #fff;font-size: 14px;margin-top: 5%;">Bimbingan sesuai dengan skill yang kamu senangi</p>
                  <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
              </div>
          </div>
      </div>
     <div class="container margin_30_95" style="text-align: center;padding-bottom: 58px;">
        <p style="margin-bottom: 3px;font-size: 160%;color: #ffffff;">Chat dengan spesialis kami</p>
        <p style="margin-bottom: 3px;font-size: 95%;color: #ffffff;">Pilih kategori dan temukan mentor untuk menjawab pertanyaan seputar skill yang kamu miliki</p>
    </div>
    <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px;">
        <!-- <hr style="margin: 24px 0 17px 0;"> -->
        <div class="row" id="kategori_show_chat_mentor_mobile" style="padding-right: 15px;padding-left: 15px;">
          <!-- <div class="col-lg-2 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;text-align: center;margin-bottom: 2%;">
            <a href="<?php echo base_url();?>Materi/materi_by_id_materi/1" class="grid_item">
              <img src="http://localhost/spero.id/EDUMEDIA/admin_v5/assets/images/icon_app_kategori/multimedia_xdpi_1.png" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 72%;">
              <p style="color: #fff;font-size: 17px;margin-bottom: unset;">Nama Mentor</p>
            </a>
          </div> -->
        </div>
    </div>
    <!-- <div class="container margin_30_95" style="text-align: center;">
        <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Menghubungkan siswa di seluruh indonesia dengan instruktur terbaik,</p>
        <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Edumedia.id membantu individu mencapai tujuan mereka dan mengejar impian mereka.</p>
    </div> -->
  </div>
</main>
<div id="myModal" class="modal">
  <a href="https://api.whatsapp.com/send?phone=6281285232001&text=Hello... apakah maskernya masih tersedia...?">
    <img class="modal-content" id="img01" src="<?php echo base_url();?>assets/images/iklan1.jpeg">
  </a>
  <span class="close">&times;</span>
</div>
<script type="text/javascript">
</script>