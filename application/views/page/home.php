<style>
  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }
  #myImg:hover {opacity: 0.7;}
  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }
  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }
  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }
  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }
  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }
  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }
  /* The Close Button */
  .close {
    position: absolute;
    top: 67px;
    right: 7px;
    color: #ffffff;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }
  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }
  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
  div .btn_mobile {
    display: none !important;
  }
  @media screen and (max-width: 600px) {
    #mobile_show_yow {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
    #mobile_show_yow_1 {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
      $.ajax({
          url : "<?php echo base_url('kursus/get_top_kursus')?>",
          type: "GET",
          dataType: "JSON",
          success: function(response){
              response.forEach(function(element) {
                  // $('#kategori_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a href="<?php echo base_url();?>Materi/materi_by_id_materi/'+element.id_materi+'" class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><div class="block-horizzontal" style="animation: none;background: none;"></div><img src="https://img.youtube.com/vi/' + element.tumnile + '/sddefault.jpg" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 0px 0px 3px 6px;animation: color 0.7s ease-in-out; animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: center;"><p style="color: #ffffff;"><b>'+ element.nama_materi +'</b></p></div></figure></a></div>');
                  if (element.status_belajar == '1') {
                    $('#kursus_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><img src="<?= MENTOR_URL ?>/'+ element.card_kursus +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 3px 18px 13px 18px;animation: color 0.7s ease-in-out;animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: left;"><p style="color: #f14068;font-size: 11px;margin-bottom: unset;">'+ element.nama_sub_kategori +'</p><p style="color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_kursus +'</p><div class="row"><p style="color: #fff;font-size: 11px;width: 50%;margin-left: 4%;margin-bottom: unset;">'+ element.nama_user +'</p><p style="color: #fff;font-size: 11px;width: 39%;text-align: end;"></p></div><a style="width: 100%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #6a25b3;color: #ffffff;font-size: 0.9rem;text-transform: capitalize;padding: 10px 13px;letter-spacing: initial;" href="<?php echo base_url();?>Kursus/index_by_id_kursus/'+element.id_kursus+'" class="btn_1 rounded">Lanjut</a></div></figure></a></div>');
                  }else{
                    $('#kursus_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><img src="<?= MENTOR_URL ?>/'+ element.card_kursus +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 3px 18px 13px 18px;animation: color 0.7s ease-in-out;animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: left;"><p style="color: #f14068;font-size: 11px;margin-bottom: unset;">'+ element.nama_sub_kategori +'</p><p style="color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_kursus +'</p><div class="row"><p style="color: #fff;font-size: 11px;width: 50%;margin-left: 4%;margin-bottom: unset;">'+ element.nama_user +'</p><p style="color: #fff;font-size: 11px;width: 39%;text-align: end;"></p></div><a style="width: 100%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #6a25b3;color: #ffffff;font-size: 0.9rem;text-transform: capitalize;padding: 10px 13px;letter-spacing: initial;" href="<?php echo base_url();?>kursus/gabung_kursus/'+element.id_kursus+'" class="btn_1 rounded">Gabung</a></div></figure></a></div>');
                  };
              });
          },
          error: function (jqXHR, textStatus, errorThrown){
              alert('terjadi kesalahan, coba beberapa saat lagi');
          }
      });
  });
</script>
<main style="background-color: #0c0016;padding-top: 3%;">
  <div class="main_title_2 hidden_tablet" style="margin-bottom: 15px;">
    <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;max-width: 100%;height: 866px;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #0b0015;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: fill;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/BANNER WEB 2.jpg">
        <div class="" style="text-align: left;margin-top: 0%;margin-right: 16%;">
          <div class="" style="padding-left: 18%;">
            <h2 style="position: relative;color: #fff;">Gali potensi skill mu, raih sukses karir mu</h2>
            <p style="position: relative;">Belajar dengan mentor terbaik di edumedia.</p>
            <a style="margin-top: 3%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.8rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?= base_url('Registration'); ?>" class="btn_1 rounded">Mulai Belajar</a>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
    <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #37386f;height: 98%;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: cover;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/BANNER MOBILE 2.jpg">
        <div class="" style="text-align: initial;padding-right: 10%;padding-left: 5%;padding-top: 0%;">
          <div class="" style="">
            <h2 style="position: relative;color: #fff;font-size: 1.3rem;margin: 0px 0 0 0;">Gali potensi skill mu,<br> raih suksess karir mu</h2>
            <p style="position: relative;font-size: 1rem;">Belajar dengan mentor terbaik<br> di edumedia.</p>
            <!-- <a style="margin-top: 3%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.8rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="http://edumedia.id/injeksi_registrasi" class="btn_1 rounded">Mulai Belajar</a> -->
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px;">
      <hr style="margin: 24px 0 17px 0;">
      <h3 style="color: #ffffff;margin-bottom: 3%;">Top Materi</h3>
      <div class="row" id="kursus_show_home" style="padding-right: 15px;padding-left: 15px;">
        <!-- <div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;">
          <figure class="block-reveal" style="height: 100% !important;">
            <img src="http://localhost/spero.id/EDUMEDIA/admin_v5/assets/images/card_kursus/Kursus 11_20201223092019.png" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;">
            <div class="info" style="padding: 3px 18px 13px 18px;animation: color 0.7s ease-in-out;animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: left;">
              <p style="color: #f14068;font-size: 11px;margin-bottom: unset;">kategori</p>
              <p style="color: #fff;font-size: 17px;margin-bottom: unset;">nama kursus</p>
              <div class="row">
                <p style="color: #fff;font-size: 11px;width: 50%;margin-left: 4%;">Nama user</p>
                <p class="d-none" style="color: #fff;font-size: 11px;width: 39%;text-align: end;">View</p>
              </div>
              <a  href="<?php echo base_url();?>kursus/gabung_kursus/1"  style="width: 100%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #6a25b3;color: #ffffff;font-size: 0.9rem;text-transform: capitalize;padding: 10px 13px;letter-spacing: initial;" class="btn_1 rounded">Gabung</a>
            </div>
          </figure>
        </div> -->
      </div>
  </div>
  <!-- <div class="container margin_30_95" style="text-align: center;">
      <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Menghubungkan siswa di seluruh indonesia dengan instruktur terbaik,</p>
      <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Edumedia.id membantu individu mencapai tujuan mereka dan mengejar impian mereka.</p>
  </div> -->
</main>
<div id="myModal" class="modal">
  <a href="https://api.whatsapp.com/send?phone=6281285232001&text=Hello... apakah maskernya masih tersedia...?">
    <img class="modal-content" id="img01" src="<?php echo base_url();?>assets/images/iklan1.jpeg">
  </a>
  <span class="close">&times;</span>
</div>
<script type="text/javascript">
</script>