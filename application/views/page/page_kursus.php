<main style="background-color: #332f37;">
  <script type="text/javascript">
    var id_kursus;
    // var id_sub_materi = null;
    // var id_sub_materi_ser;
    $(document).ready(function() {
          var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
          var pathArray = window.location.pathname.split( '/' );
		//   var id_kursus = pathArray[4];
		      var id_kursus = <?= $id_kursus ?>;
          console.log(id_kursus);
        $.ajax({
            url : "<?php echo base_url('kursus/get_by_id_kursus')?>/"+id_kursus,
            type: "GET",
            dataType: "JSON",
            success: function(response){
                var id_materi_aktif;
                $('#nama_kursus').html(response.kursus.nama_kursus);
                $('#nama_materi').html(response.mater_aktif.nama_materi);
                $('#video').html('<figure style="height: 507px;"><iframe src="'+response.mater_aktif.embed_video+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>');
                $('#description').html('<iframe style="height: 600px;" src="<?= MENTOR_URL ?>/'+response.mater_aktif.path_pdf+'" width="auto" height="auto"> </iframe>');
                response.materi_sudah.forEach(function(element) {
                    $('#list_pelajaran').append('<tr><td width="50%"><div style="margin-bottom: 4%;"><a href="<?php echo base_url();?>Materi/materi_by_id_materi/1" class="grid_item"><figure class="block-reveal" style="height: 100% !important;margin: 0px 0 0px;"><div class="block-horizzontal" style="animation: none;background: none;"></div><img src="'+ element.tumnile +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"></figure></a></div></td><td valign="top"  width="50%"><div style="background: #171219;height: 150px;"><p style="margin-left: 5%;color: #f14068;font-size: 11px;margin-bottom: unset;padding-top: 3%;">'+ element.nama_kategori +'</p><p style="margin-left: 5%;color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_materi +'</p><p style="margin-left: 5%;color: #fff;font-size: 11px;width: 50%;margin-bottom: unset;">'+ element.nama_user +'</p></div></td></tr>');
                });
                response.materi_belum.forEach(function(element) {
                    $('#list_pelajaran').append('<tr><td width="50%"><div style="margin-bottom: 4%;"><a href="<?php echo base_url();?>Materi/materi_by_id_materi/1" class="grid_item"><figure class="block-reveal" style="height: 100% !important;margin: 0px 0 0px;"><div class="block-horizzontal" style="animation: none;background: none;"></div><img src="'+ element.tumnile +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"></figure></a></div></td><td valign="top"  width="50%"><div style="background: #171219;height: 150px;"><p style="margin-left: 5%;color: #f14068;font-size: 11px;margin-bottom: unset;padding-top: 3%;">'+ element.nama_kategori +'</p><p style="margin-left: 5%;color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_materi +'</p><p style="margin-left: 5%;color: #fff;font-size: 11px;width: 50%;margin-bottom: unset;">'+ element.nama_user +'</p></div></td></tr>');
                });
				if (response.jml_materi_belum > 0) {
                	$('#button_kuis').append('<a onclick="quis('+response.mater_aktif.id_materi+', '+response.kursus.id_join_kursus+')" style="color: #fff;background: #f14068;width: 22%;margin-left: 2%;margin-bottom: 6%;margin-top: 6%;" class="btn_1 full-width">Kuis</a>');
				};
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('terjadi kesalahan, coba beberapa saat lagi');
            }
        });
      });
    function quis(id_materi_aktif, id_join_kursus){
      window.location.href = "<?php echo base_url('kuis/get_kuis_by_id_materi')?>/"+id_materi_aktif+"/"+id_join_kursus;
      // $.ajax({
      //     url : "<?php echo base_url('kuis/get_kuis_by_id_materi')?>/" + id_materi_aktif,
      //     type: "GET",
      //     dataType: "JSON",
      //     success: function(data)
      //     {
      //       if (data.status == true) {
      //         window.location.href = "http://localhost/spero.id/EDUMEDIA/lms_v5/Kuis/get_kuis/"+id_materi_aktif;
      //       }else{
      //         swal({
      //             type: 'warning',
      //             title: '',
      //             text: data.pesan,
      //             footer: '<a href>Why do I have this issue?</a>',
      //         });
      //       };
      //     },
      //     error: function (jqXHR, textStatus, errorThrown)
      //     {
      //         swal("Transaksi gagal", "Membatalkan transaksi", "error");
      //     }
      // });
    }
  </script>
  <!-- <script src="https://js.pusher.com/7.0/pusher.min.js"></script> -->
  <div class="">
    <div style="background-color: #0c0016;">
      <div class="container margin_60_35" style="padding-top: 82px;max-width: 1398px;">
        <div class="row">
          <div class="col-lg-8">
            <!-- /top-wizard -->
            <form name="form_mendaftar" id="form_mendaftar" >
              <input id="id_mata_pelajaran" name="id_mata_pelajaran" type="hidden">
            </form>
            <h2 style="color: #ffffff;" id="nama_kursus"> </h2>
            <p style="color: #ffffff;" id="nama_materi"></p>
            <section id="video">
            </section>
            <hr style="margin: 17px 0 17px 0;border-top: 3px solid rgb(255 255 255 / 45%);">
            <h2 style="color: #ffffff;margin-bottom: 5%;margin-top: 5%;"> Pendalaman Materi</h2>
            <section id="description" style="color: #ffffff;border-bottom: unset;">
            </section>
              <div id="button_kuis"> </div>
            <!-- <a style="background: #662d91;width: 80%;margin-left: 9%;margin-bottom: 6%;margin-top: 6%;" href="<?php echo base_url();?>Kuis/get_kuis/" class="btn_1 full-width">Kuis</a> -->
          </div>        
          <aside class="col-lg-4" id="sidebar1">
            <h2 style="color: #ffffff;"> Daftar Materi</h2>
            <p style="color: #ffffff;" > Materi yang akan di bahas</p>
            <div class="card" style="background: #0c0016;">
              <div class="list_lessons_2" style="height: 100vh;overflow: auto;">
                <!-- <ul id="list_pelajaran" style="height: 412px;overflow: auto;color: #ffffff;">
                </ul> -->
                <table style="color: #ffffff;" width="100%">
                  <tbody id="list_pelajaran">
                  </tbody>
                </table>
              </div>
              <!-- <div id="button_kuis"> </div> -->
            </div>
            <hr style="margin: 17px 0 17px 0;border-top: 3px solid rgb(255 255 255 / 45%);">
            <div class="card" style="background: transparent; display: none; ">
              <h1 style="color: white;">Diskusi Kelas</h1>
              <div id="div_chat">
                <div id="chat_area" class="chat_area" style="background: #d8d8d8; ">
                  <ul id="appendchat" class="list-unstyled">
                  </ul>
                </div><!--chat_area-->
                <div class="message_write">
                  <textarea class="form-control" id="message" placeholder="type a message"></textarea>
                  <div class="clearfix"></div>
                  <div id="divBtnSend" class="chat_bottom">
                    <span id="sendchat" class="pull-right btn btn-success" style="width: 100%;">
                      <i class="fa fa-paper-plane" aria-hidden="true"></i> Kirim
                    </span>
                  </div>
                </div>	
                <!-- Image loader -->
                <div class="overlay"></div>
                <div class="spanner">
                  <div class="loader"></div>
                  <p>Sedang memproses data...</p>
                </div>
                <!-- Image loader -->									
              </div>
            </div>
          </aside>
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>
    <!-- /bg_color_1 -->
  </div>
</main>
<!--/main-->