
<style>
  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }

  #myImg:hover {opacity: 0.7;}

  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }

  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }

  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }

  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }

  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }

  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }

  /* The Close Button */
  .close {
    position: absolute;
    top: 67px;
    right: 7px;
    color: #ffffff;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }

  div .btn_mobile {
    display: none !important;
  }

  @media screen and (max-width: 600px) {
    #mobile_show_yow {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
    #mobile_show_yow_1 {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
  }
</style>
<script type="text/javascript">      
  
  var status_modal;
  var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
  var pathArray = window.location.pathname.split( '/' );
  var id_mentor = pathArray[4];

  $(document).ready(function() {
      document.getElementById("id_mentor").value=id_mentor;
  });

  function submit(){
      var form = document.getElementById("form_tambah");
      var fd = new FormData(form);
      $.ajax({
          url : "<?php echo base_url('become_mentor/cek_kuesioner')?>",
          type: "POST",
          data: fd,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {   
              if (data.status) {
                  location.replace('<?= MENTOR_URL ?>');
              }else{
                  swal({
                      type: 'warning',
                      title: '',
                      text: 'Yang ber tanda (*) wajib di isi!',
                      footer: '<a href>Why do I have this issue?</a>',
                      });

              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal({
                  type: 'warning',
                  title: '',
                  text: misage_error,
                  footer: '<a href>Why do I have this issue?</a>',
                  });
          }
      });   
  }

  function submit_mobile(){
      var form = document.getElementById("form_tambah_mobile");
      var fd = new FormData(form);
      $.ajax({
          url : "<?php echo base_url('become_mentor/cek_kuesioner')?>",
          type: "POST",
          data: fd,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {   
              if (data.status) {
                  location.replace('<?= MENTOR_URL ?>');
              }else{
                  swal({
                      type: 'warning',
                      title: '',
                      text: 'Yang ber tanda (*) wajib di isi!',
                      footer: '<a href>Why do I have this issue?</a>',
                      });

              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal({
                  type: 'warning',
                  title: '',
                  text: misage_error,
                  footer: '<a href>Why do I have this issue?</a>',
                  });
          }
      });   
  }

  function back() {
      location.replace("<?php echo base_url();?>become_mentor/kuesioner_2/"+id_mentor);
  }

  function back_mobile() {
      location.replace("<?php echo base_url();?>become_mentor/kuesioner_2/"+id_mentor);
  }

</script>

<main style="background-color: #0c0016;padding-top: 3%;">
  <!-- ====================== START CONTENT ===================== -->
  <div class="wrapper">
      <div class="container">
          <div class="bs-wizard clearfix">
              <div class="bs-wizard-step active">
                  <div class="text-center bs-wizard-stepnum">Tahap 1</div>
                  <div class="progress">
                      <div class="progress-bar"></div>
                  </div>
                  <a href="#0" class="bs-wizard-dot"></a>
              </div>

              <div class="bs-wizard-step active">
                  <div class="text-center bs-wizard-stepnum">Tahap 2</div>
                  <div class="progress">
                      <div class="progress-bar"></div>
                  </div>
                  <a href="#0" class="bs-wizard-dot"></a>
              </div>

              <div class="bs-wizard-step active">
                  <div class="text-center bs-wizard-stepnum">Tahap 3!</div>
                  <div class="progress">
                      <div class="progress-bar"></div>
                  </div>
                  <a href="#0" class="bs-wizard-dot"></a>
              </div>
          </div>
          <!-- End bs-wizard -->
      </div>
  </div>
  <div class="main_title_2 hidden_tablet" style="margin-bottom: unset;text-align: start;">
      <div style="margin-top: 0%;padding-left: 30%;padding-right: 30%;">
          <h2 style="color: #fff;margin-top: 4%;">Perluas jangkauan anda</h2>
          <hr style="margin: 40px 48% 40px 48%;border-color: #1c153f;">
          <p style="color: #fff;font-size: 18px;margin-top: 5%;">Setelah menerbitkan kursus, anda dapat mengembangkan audiens peserta dan membuat dampak, dengan dukungan dari promosi pasar edumedia serta melalui upaya pemasaran anda sendiri. Bersama-sama, kita akan membantu peserta yang tepat untuk menemukan kursus anda.</p>
          <p style="color: #fff;font-size: 18px;margin-top: 6%;">Apa ada audients yang dapat anda ajak berbagi kursus anda?</p>
          <form class="form-horizontal" id="form_tambah">
            <input type="hidden" value="" name="id_mentor" id="id_mentor" />
            <input type="hidden" value="3" name="id_soal_kuesioner" id="id_soal_kuesioner" />
            <div class="row">
                <div class="col-lg-12 col-md-12" style="background-color: #151219;padding: 2%;text-align: start;margin-bottom: 2%;">
                    <label style="color: #fff;margin-bottom: unset;"><input style="color: #fff;" type="radio" name="jawaban" id="jawaban" value="9"> Saat ini tidak</label>
                </div>
                <div class="col-lg-12 col-md-12" style="background-color: #151219;padding: 2%;text-align: start;margin-bottom: 2%;">
                    <label style="color: #fff;margin-bottom: unset;"><input style="color: #fff;" type="radio" name="jawaban" id="jawaban" value="10"> Saya memiliki sedikit pengikut</label>
                </div>
                <div class="col-lg-12 col-md-12" style="background-color: #151219;padding: 2%;text-align: start;margin-bottom: 2%;">
                    <label style="color: #fff;margin-bottom: unset;"><input style="color: #fff;" type="radio" name="jawaban" id="jawaban" value="11"> Pengikut saya cukup banyak</label>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6" style="margin-top: 9%;">
                    <!-- <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/kuesioner_2" class="btn_1 rounded">Sebelumnya</a> -->
                    <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" onclick="back()" class="btn_1 rounded">Sebelumnya</a>
                </div>
                <div class="col-lg-6 col-md-6" style="margin-top: 9%;text-align: end;">
                    <!-- <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/kuesioner_save" class="btn_1 rounded">Daftar</a> -->
                    <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" onclick="submit()" class="btn_1 rounded">Daftar</a>
                </div>                        
            </div>            
          </form>
      </div>
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
      <div style="margin-top: 0%;padding-left: 6%;padding-right: 6%;">
          <h2 style="color: #fff;margin-top: 4%;font-size: 1rem;">Perluas jangkauan anda</h2>
          <hr style="margin: 14px 48% 14px 48%;border-color: #1c153f;">
          <p style="color: #fff;font-size: 13px;margin-top: 5%;">Setelah menerbitkan kursus, anda dapat mengembangkan audiens peserta dan membuat dampak, dengan dukungan dari promosi pasar edumedia serta melalui upaya pemasaran anda sendiri. Bersama-sama, kita akan membantu peserta yang tepat untuk menemukan kursus anda.</p>
          <p style="color: #fff;font-size: 13px;margin-top: 6%;">Apa ada audients yang dapat anda ajak berbagi kursus anda?</p>
          <form class="form-horizontal" id="form_tambah_mobile">
            <input type="hidden" value="" name="id_mentor" id="id_mentor" />
            <input type="hidden" value="3" name="id_soal_kuesioner" id="id_soal_kuesioner" />
            <div class="row">
                <div class="col-lg-12 col-md-12" style="background-color: #151219;padding: 2%;text-align: start;margin-bottom: 2%;">
                    <label style="color: #fff;margin-bottom: unset;font-size: 0.8rem;"><input style="color: #fff;" type="radio" name="jawaban" id="jawaban" value="9"> Saat ini tidak</label>
                </div>
                <div class="col-lg-12 col-md-12" style="background-color: #151219;padding: 2%;text-align: start;margin-bottom: 2%;">
                    <label style="color: #fff;margin-bottom: unset;font-size: 0.8rem;"><input style="color: #fff;" type="radio" name="jawaban" id="jawaban" value="10"> Saya memiliki sedikit pengikut</label>
                </div>
                <div class="col-lg-12 col-md-12" style="background-color: #151219;padding: 2%;text-align: start;margin-bottom: 2%;">
                    <label style="color: #fff;margin-bottom: unset;font-size: 0.8rem;"><input style="color: #fff;" type="radio" name="jawaban" id="jawaban" value="11"> Pengikut saya cukup banyak</label>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6" style="margin-top: 5%;margin-bottom: 5%;text-align: left;width: 50%;">
                    <!-- <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/kuesioner_2" class="btn_1 rounded">Sebelumnya</a> -->
                    <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 0.7px solid white;border-color: #f14068;color: #ffffff;font-size: 0.7rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" onclick="back_mobile()" class="btn_1 rounded">Sebelumnya</a>
                </div>
                <div class="col-lg-6 col-md-6" style="margin-top: 5%;margin-bottom: 5%;text-align: end;width: 50%;">
                    <!-- <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/kuesioner_save" class="btn_1 rounded">Daftar</a> -->
                    <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 0.7px solid white;border-color: #f14068;color: #ffffff;font-size: 0.7rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" onclick="submit_mobile()" class="btn_1 rounded">Daftar</a>
                </div>                        
            </div>            
          </form>
      </div>
  </div>
  <!-- ====================== END CONTENT ===================== -->

</main>