<style>
  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }
  #myImg:hover {opacity: 0.7;}
  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }
  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }
  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }
  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }
  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }
  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }
  /* The Close Button */
  .close {
    position: absolute;
    top: 67px;
    right: 7px;
    color: #ffffff;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }
  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }
  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
  div .btn_mobile {
    display: none !important;
  }
  @media screen and (max-width: 600px) {
    #mobile_show_yow {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
    #mobile_show_yow_1 {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
      $.ajax({
          url : "<?php echo base_url('kategori/get_all_kategori_aktif')?>",
          type: "GET",
          dataType: "JSON",
          success: function(response){
              response.forEach(function(element) {
                  // $('#kategori_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a href="<?php echo base_url();?>Materi/materi_by_id_materi/'+element.id_materi+'" class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><div class="block-horizzontal" style="animation: none;background: none;"></div><img src="https://img.youtube.com/vi/' + element.tumnile + '/sddefault.jpg" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 0px 0px 3px 6px;animation: color 0.7s ease-in-out; animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: center;"><p style="color: #ffffff;"><b>'+ element.nama_materi +'</b></p></div></figure></a></div>');
                  $('#kategori_show_chat_mentor').append('<div class="col-lg-2 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;text-align: center;margin-bottom: 2%;"><a href="<?php echo base_url();?>Materi/materi_by_id_materi/'+ element.id_kategori +'" class="grid_item"><img src="<?= MENTOR_URL ?>/'+  element.icon_app +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 72%;"><p style="color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_kategori +'</p></a></div>');
              });
          },
          error: function (jqXHR, textStatus, errorThrown){
              alert('terjadi kesalahan, coba beberapa saat lagi');
          }
      });
  });
</script>
<main style="background-color: #0c0016;padding-top: 3%;">
  <!-- ==================== START SLIDER BANER ==================== -->
  <div class="main_title_2 hidden_tablet" style="margin-bottom: 15px;">
    <section id="hero_in" class="courses container start_bg_zoom" style="margin-right: 13%;padding-right: unset;padding-left: unset;max-width: 100%;height: 597px;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #0b0015;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: fill;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/Banner Become Mentor.jpg">
        <div class="" style="text-align: left;margin-right: 12%;">
          <div class="" style="text-align: start;margin-left: 61%;">
              <div style="position: relative;">
                  <h2 style="color: #fff;font-size: 53px;">Menjadi Mentor</h2>
                  <p style="margin-top: 6%;">Menjadi Mentor Ciptakan para champion baru dengan pengetahuan, keahlian, serta pengalaman yang anda bagikan kepada para pembelajar dan dapatkan penghasilan tanpa batas.</p>
                <?php if ($this->session->userdata('id_murid')) {?>
                    <a style="margin-top: 3%;margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/save_registration_murid" class="btn_1 rounded">Jadilah Mentor</a>
                <?php }else{ ?>
                    <a style="margin-top: 3%;margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/index_save_registration" class="btn_1 rounded">Jadilah Mentor</a>
                <?php } ?>
                <a style="margin-top: 3%;margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?= MENTOR_URL; ?>" class="btn_1 rounded" target="_blank">Login Mentor</a>
              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
    <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #37386f;height: 98%;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: cover;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/Banner Become Mentor.jpg">
        <div class="" style="text-align: left;margin-right: 12%;">
          <div class="" style="text-align: start;margin-left: 33%;">
              <div style="position: relative;">
                  <h2 style="color: #fff;font-size: 26px;">Menjadi Mentor</h2>
                  <p style="margin-top: 6%;font-size: 0.9rem;">Menjadi Mentor Ciptakan para champion baru dengan pengetahuan, keahlian, serta pengalaman yang anda bagikan kepada para pembelajar dan dapatkan penghasilan tanpa batas.</p>
                <?php if ($this->session->userdata('id_murid')) {?>
                    <a style="margin-top: 7%;margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.7rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;min-width: 175px;" href="<?php echo base_url();?>become_mentor/save_registration_murid" class="btn_1 rounded">Jadilah Mentor</a>
                <?php }else{ ?>
                    <a style="margin-top: 7%;margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.7rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;min-width: 175px;" href="<?php echo base_url();?>become_mentor/index_save_registration" class="btn_1 rounded">Jadilah Mentor</a>
                <?php } ?>
                <a style="margin-bottom: 11%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.7rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;min-width: 175px;" href="<?= MENTOR_URL; ?>" class="btn_1 rounded" target="_blank">Login Mentor</a>
              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- ==================== END SLIDER BANER ==================== -->
  <!-- ====================== START CONTENT ===================== -->
  <div class="main_title_2 hidden_tablet" style="margin-bottom: unset;">
    <div class="filters_listing" style="margin: 4vh;padding: 2vh;background: #151219;text-align: center;">
        <h2 style="color: #fff;">Temukan Potensi Anda</h2>
        <div class="row" style="margin-top: 3%;">
            <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                <img src="<?php echo base_url();?>assets/images/icons8-expensive-price-96.png" style="height: 75px;">
                <h3 style="color: #fff;margin-top: 2%;">Hasilkan Uang</h3>
                <p style="color: #fff;font-size: 15px;margin-top: 5%;">Kumpulkan poin dari setiap video pembelajaran yang kamu buat yang bisa dikonversikan dengan rupiah</p>
                <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
            </div>
            <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                <img src="<?php echo base_url();?>assets/images/icons8-tv-show-96.png" style="height: 75px;">
                <h3 style="color: #fff;margin-top: 2%;">Menginspirasi Mereka</h3>
                <p style="color: #fff;font-size: 15px;margin-top: 5%;">Bantu orang mempelajari keahlian baru, memajukan kariernya dan menjelajahi hobinya dengan cara membagikan pengetahuan anda</p>
                <hr style="margin: 57px 40% 30px 40%;border-color: #6d26b9;">
            </div>
            <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                <img src="<?php echo base_url();?>assets/images/icons8-teacher-96.png" style="height: 75px;">
                <h3 style="color: #fff;margin-top: 2%;">Jadilah Mentor Kami</h3>
                <p style="color: #fff;font-size: 15px;margin-top: 5%;">Edumedia akan membantu anda menggunakan dashboard mentor untuk menghasilkan kelas berkualitas tinggi</p>
                <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
            </div>
        </div>
    </div>
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
    <div class="filters_listing" style="margin: 4%;padding: 2%;background: #151219;text-align: center;">
        <h2 style="color: #fff;font-size: 1.4rem;">Temukan Potensi Anda</h2>
        <div class="row" style="margin-top: 3%;">
            <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                <img src="<?php echo base_url();?>assets/images/icons8-expensive-price-96.png" style="height: 75px;">
                <h3 style="color: #fff;margin-top: 2%;font-size: 1.4rem;">Hasilkan Uang</h3>
                <p style="color: #fff;font-size: 15px;margin-top: 5%;">Kumpulkan poin dari setiap video pembelajaran yang kamu buat yang bisa dikonversikan dengan rupiah</p>
                <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
            </div>
            <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                <img src="<?php echo base_url();?>assets/images/icons8-tv-show-96.png" style="height: 75px;">
                <h3 style="color: #fff;margin-top: 2%;font-size: 1.4rem;">Menginspirasi Mereka</h3>
                <p style="color: #fff;font-size: 15px;margin-top: 5%;">Bantu orang mempelajari keahlian baru, memajukan kariernya dan menjelajahi hobinya dengan cara membagikan pengetahuan anda</p>
                <hr style="margin: 57px 40% 30px 40%;border-color: #6d26b9;">
            </div>
            <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                <img src="<?php echo base_url();?>assets/images/icons8-teacher-96.png" style="height: 75px;">
                <h3 style="color: #fff;margin-top: 2%;font-size: 1.4rem;">Jadilah Mentor Kami</h3>
                <p style="color: #fff;font-size: 15px;margin-top: 5%;">Edumedia akan membantu anda menggunakan dashboard mentor untuk menghasilkan kelas berkualitas tinggi</p>
                <hr style="margin: 57px 40% 30px 40%;border-color: #ea3f61;">
            </div>
        </div>
    </div>
  </div>
  <div class="main_title_2 hidden_tablet" style="margin-bottom: unset;">
      <section id="hero_in" class="courses container start_bg_zoom" style="margin-right: 13%;padding-right: unset;padding-left: unset;max-width: 100%;height: 597px;">
          <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #0b0015;">
            <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: fill;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/Banner Become Mentor 2.jpg">
            <div >
              <div class="" style="text-align: center;">
                  <div style="position: relative;margin: 4%;">
                      <h3 style="color: #fff;font-size: 35px;">Peluang Tak Biasa</h3>
                      <p style="margin-top: 1%;">Bergabunglah bersama kami edumedia dan ciptakan pencapaian yang luar biasa</p>                            
                      <div class="row" style="margin-top: 7%;">
                          <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                              <h3 style="color: #fff;margin-top: 2%;font-size: 56px;">25 Jt</h3>
                              <p style="color: #fff;font-size: 22px;margin-top: 5%;">Lebih dari 25 juta siswa pembelajar potensi dari seluruh indonesia</p>
                          </div>
                          <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                              <h3 style="color: #fff;margin-top: 2%;font-size: 56px;">200</h3>
                              <p style="color: #fff;font-size: 22px;margin-top: 5%;">Kelas keahlian milik anda salah satunya</p>
                          </div>
                          <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                              <h3 style="color: #fff;margin-top: 2%;font-size: 56px;">150</h3>
                              <p style="color: #fff;font-size: 22px;margin-top: 5%;">Rata rata jumlah siswa per kelas termasuk siswa anda</p>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
      </section>
      <h2 style="color: #fff;margin-top: 4%;">Kami Siap Membantu</h2>
      <hr style="margin: 40px 48% 40px 48%;border-color: #1c153f;">
      <div style="margin-top: 0%;padding-left: 30%;padding-right: 30%;">
          <p style="color: #fff;font-size: 18px;margin-top: 5%;">Tim pendukung kami siap membantu Anda 24/7 untuk membantu kebutuhan pembuatan kursus Anda. Gunakan Teaching Center kami, pusat sumber daya untuk membantu Anda melalui prosesnya. Bergabung dengan Studio kami dan dapatkan dukungan dari rekan sejawat dari komunitas instruktur kami. Grup komunitas ini selalu aktif, selalu ada, dan selalu membantu.</p>
          <p style="color: #fff;font-size: 18px;margin-top: 6%;">Mulai mentor sekarang juga</p>
          <?php if ($this->session->userdata('id_murid')) {?>
              <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/save_registration_murid" class="btn_1 rounded">Jadilah Mentor</a>
          <?php }else{ ?>
              <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 1rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/index_save_registration" class="btn_1 rounded">Jadilah Mentor</a>
          <?php } ?>
      </div>
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
      <section id="hero_in" class="courses container start_bg_zoom" style="margin-right: 13%;padding-right: unset;padding-left: unset;max-width: 100%;height: 597px;">
          <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #0b0015;">
            <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: fill;padding-right: unset;padding-left: unset;" src="<?php echo base_url();?>assets/images/Banner Become Mentor 2.jpg">
            <div >
              <div class="" style="text-align: center;">
                  <div style="position: relative;margin: 4%;">
                      <h2 style="color: #fff;font-size: 25px;">Peluang Tak Biasa</h2>
                      <p style="margin-top: 1%;font-size: 1rem;">Bergabunglah bersama kami edumedia dan ciptakan pencapaian yang luar biasa</p>                            
                      <div class="row" style="margin-top: 7%;">
                          <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                              <h2 style="color: #fff;margin-top: 2%;font-size: 40px;">25 Jt</h2>
                              <p style="color: #fff;font-size: 22px;margin-top: 5%;font-size: 1rem;">Lebih dari 25 juta siswa pembelajar potensi dari seluruh indonesia</p>
                          </div>
                          <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                              <h2 style="color: #fff;margin-top: 2%;font-size: 40px;">200</h2>
                              <p style="color: #fff;font-size: 22px;margin-top: 5%;">Kelas keahlian milik anda salah satunya</p>
                          </div>
                          <div class="col-lg-4 col-md-12" style="padding-left: 5%;padding-right: 5%;">
                              <h2 style="color: #fff;margin-top: 2%;font-size: 40px;">150</h2>
                              <p style="color: #fff;font-size: 22px;margin-top: 5%;font-size: 1rem;">Rata rata jumlah siswa per kelas termasuk siswa anda</p>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
      </section>
      <h2 style="color: #fff;margin-top: 4%;">Kami Siap Membantu</h2>
      <hr style="margin: 20px 48% 20px 48%;border-color: #1c153f;">
      <div style="margin-top: 0%;padding-left: 6%;padding-right: 6%;">
          <p style="color: #fff;font-size: 16px;margin-top: 5%;">Tim pendukung kami siap membantu Anda 24/7 untuk membantu kebutuhan pembuatan kursus Anda. Gunakan Teaching Center kami, pusat sumber daya untuk membantu Anda melalui prosesnya. Bergabung dengan Studio kami dan dapatkan dukungan dari rekan sejawat dari komunitas instruktur kami. Grup komunitas ini selalu aktif, selalu ada, dan selalu membantu.</p>
          <p style="color: #fff;font-size: 16px;margin-top: 6%;">Jadilah mentor sekarang juga</p>
          <?php if ($this->session->userdata('id_murid')) {?>
              <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.7rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/save_registration_murid" class="btn_1 rounded">Mulai Mentor</a>
          <?php }else{ ?>
              <a style="margin-top: 0%;margin-bottom: 9%;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.7rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?php echo base_url();?>become_mentor/index_save_registration" class="btn_1 rounded">Mulai Mentor</a>
          <?php } ?>
      </div>
<!-- ====================== END CONTENT ===================== -->
</main>