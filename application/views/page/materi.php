<main style="background-color: #000000;">

  <script type="text/javascript">

    var id_mata_pelajaran;

    $(document).ready(function() {

        $("body").removeClass("loaded");

        var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;

        var pathArray = window.location.pathname.split( '/' );

        id_mata_pelajaran = pathArray[4];

        // document.getElementById("id_materi").value=id_materi;

        $.ajax({

            url : "<?php echo base_url('Materi/get_materi_by_mata_pelajaran')?>/"+id_mata_pelajaran,

            type: "GET",

            dataType: "JSON",

            success: function(response){

              // $('#video_materi').html('<iframe width="560" height="315" src="'+ response.mata_pelajaran.path_card +'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');

              $('#video_materi').html('<img style="width: 100%;" src="<?= MENTOR_URL ?>/'+ response.mata_pelajaran.path_card +'" width="149" data-retina="true" alt="">');

              response.materi_semester_1.forEach(function(element) {

                  $('#materi_semester_1').append(' <li><a style="color: #ffffff;" href="#0"><i class="icon_clock_alt" style="color: #ff9943;margin-right: 1%;"></i>'+ element.nama_materi +'</a></li>');
                  
              });

              response.materi_semester_2.forEach(function(element) {

                  $('#materi_semester_2').append(' <li><a style="color: #ffffff;" href="#0"><i class="icon_clock_alt" style="color: #ff9943;margin-right: 1%;"></i>'+ element.nama_materi +'</a></li>');
                  
              });

              $('#waktu_pembelajaran').append('<li><i class="icon_clock_alt" style="color: #ff9943;"></i>Mulai Pukul <strong style="color: #ff9943;">'+ response.mata_pelajaran.waktu_mulai +'</strong></li>');

              $('#waktu_pembelajaran').append('<li><i class="icon_clock_alt" style="color: #ff9943;"></i>Selesai Pukul <strong style="color: #ff9943;">'+ response.mata_pelajaran.waktu_selesai +'</strong></li>');

              $('#mata_pelajaranmata').html(response.mata_pelajaran.nama_mata_pelajaran);

              $('#nama_guru').html(response.mata_pelajaran.nama_guru);

              // $('#tombol_mulai').html('<div id="tombol_daftar"><p style="margin-bottom: unset;color: #ff9943;" class="text-center"><a href="<?php echo base_url();?>Belajar/materi_by_mata_pelajaran/'+id_mata_pelajaran+'" style="color: #fff;background: #ff9943;" class="btn_1 full-width">Mulai</a></p></div>');

              $('#tombol_mulai').html('<div id="tombol_daftar"><p style="margin-bottom: unset;color: #ff9943;" class="text-center"><a onclick="get_jam_belajar('+id_mata_pelajaran+')" style="color: #fff;background: #ff9943;" class="btn_1 full-width">Mulai</a></p></div>');

            },

            error: function (jqXHR, textStatus, errorThrown){

                alert('terjadi kesalahan, coba beberapa saat lagi');

            }

        });

      });

    function get_jam_belajar(id_mata_pelajaran){

      $.ajax({
          url : "<?php echo base_url('materi/get_jam_belajar')?>/" + id_mata_pelajaran,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            if (data.status == true) {

              window.location.href = "<?php echo base_url();?>Belajar/materi_by_mata_pelajaran/"+id_mata_pelajaran;

            }else{

              swal({

                  type: 'warning',

                  title: '',

                  text: data.pesan,

                  footer: '<a href>Why do I have this issue?</a>',

              });

            };
            
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal("Transaksi gagal", "Membatalkan transaksi", "error");
          }
      });

    }



    function mendaftar(){

            var form = document.getElementById("form_mendaftar");

            var fd = new FormData(form);

          $.ajax({

              url : "<?php echo base_url('Belajar/save_materi')?>",

              type: "POST",

              data: fd,

              contentType: false,

              processData: false,

              dataType: "JSON",

              success: function(data)

              {   

                  if (data.status == true) {

                      location.replace("<?php echo base_url();?>Belajar/detail/"+id_materi)

                  }else{

                      swal({

                          type: 'warning',

                          title: '',

                          text: 'Terjadi Kesalahan, Silahkan Cobalagi Nanti',

                          footer: '<a href>Why do I have this issue?</a>',

                      });

                  }

              },

              error: function (jqXHR, textStatus, errorThrown)

              {

                  swal({

                      type: 'warning',

                      title: '',

                      text: 'Terjadi Kesalahan, Silahkan Cobalagi Nanti',

                      footer: '<a href>Why do I have this issue?</a>',

                      });

       

              }

          }); 

    }

  </script>

  <!-- <section id="hero_in" class="courses">

    <div class="wrapper">

      <img style="width: 100%;position: absolute;margin-top: 35px;height: 100%;object-fit: cover;" src="<?php echo base_url();?>assets/images/banner_web.png">

      <div class="container" style="">

        <div>

          <h1 style="color: #00387e;font-size: 280%;margin-top: -20px;" class="fadeInUp">Detail Kursus</h1>

        </div>

      </div>

    </div>

  </section> -->

  <!-- /hero_single -->

  <div style="background-color: #37386f;margin-top: 54px;">

    <div class="container margin_60_35">

      <div class="row">

        <div class="col-lg-8">

          <section id="description" style="border-bottom: unset;">

            <h2 id="mata_pelajaranmata" style="color: #ffffff;"> </h2>

            <p id="nama_guru" style="color: #ffffff;"> Nama Guru </p>

          </section>      

            
          <section id="lessons" style="border-bottom: unset;">
            
            <div id="accordion_lessons" role="tablist" class="add_bottom_45">
            
              <div class="card" style="background-color: #201f597a;">
            
                <div class="card-header" role="tab" id="headingOne" style="border-radius: unset;background-color: #201f59;border-bottom: 1px solid rgba(0,0,0,.125);">
            
                  <h5 class="mb-0">
            
                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color: #ffffff;"><i class="indicator ti-minus" style="color: #ffffff;"></i> Semester 1</a>
            
                  </h5>
            
                </div>

                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion_lessons">

                  <div class="card-body">
                  
                    <div class="list_lessons">
                  
                      <ul id="materi_semester_1">
                  
                      </ul>
                  
                    </div>
                  
                  </div>
                
                </div>
              
              </div>
              
              <!-- /card -->
              
              <div class="card" style="background-color: #201f597a;">
              
                <div class="card-header" role="tab" id="headingTwo" style="border-radius: unset;background-color: #201f59;border-bottom: 1px solid rgba(0,0,0,.125);">
              
                  <h5 class="mb-0">
              
                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color: #ffffff;"><i class="indicator ti-plus" style="color: #ffffff;"></i> Semester 2</a>
              
                  </h5>
              
                </div>
              
                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion_lessons">
              
                  <div class="card-body">
              
                    <div class="list_lessons">
              
                      <ul id="materi_semester_2">
              
                      </ul>
              
                    </div>
              
                  </div>
              
                </div>
              
              </div>

            </div>

            <!-- /accordion -->

          </section>          

          <!-- /section -->

        </div>

        <!-- /col -->

        

        <aside class="col-lg-4" id="sidebar1">

          <div class="box_detail" style="background-color: #201f597a;border: 1px solid #201f597a;">

            <figure>

              <div id="video_materi">
                
              </div>

              <div class="info" style="padding: 10px 0px 12px 0px;animation: color 0.7s ease-in-out; animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #201f59;text-align: center;"><p style="color: #ffffff;margin-bottom: unset;"><b>Ilmu Pengetahuan Alam ( I P A )</b></p></div>

            </figure>           

            <!-- /top-wizard -->

            <form name="form_mendaftar" id="form_mendaftar" >

              <input id="id_mata_pelajaran" name="id_mata_pelajaran" type="hidden">

            </form>

            <div id="tombol_daftar">

              

            </div>

            <div style="color: #ffffff;margin-top: 13%;" id="list_feat">

              <h3 style="color: #ffffff;margin-bottom: 8%;">Waktu Belajar</h3>

              <ul id="waktu_pembelajaran">

                <!-- <li><i class="icon_clock_alt"></i>Mulai Pukul <strong>08:00</strong></li>

                <li><i class="icon_clock_alt"></i>Selesai Pukul <strong>08:00</strong></li> -->

              </ul>

              <div id="tombol_mulai" style="margin-top: 9%;"></div>

              <!-- <div id="tombol_daftar"><p style="margin-bottom: unset;" class="text-center"><a href="http://edumedia.id/Login" style="color: #fff;background: #6a25b3;" class="btn_1 full-width">Mulai</a></p></div> -->

            </div>

          </div>

        </aside>

      </div>

      <!-- /row -->

    </div>

    <!-- /container -->

  </div>

  <!-- /bg_color_1 -->



</main>

<!-- /main -->