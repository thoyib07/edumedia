<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Edumedia Intuition - Creative - Emotion">
    <meta name="keywords" content="Edumedia,Intuition,Creative,Emotion">
    <meta name="author" content="Edumedia">
    <meta property="og:image" content="https://edumedia.id/assets/images/logo.png">
    <meta property="og:type" content="website" />
    <title>Edumedia | Intuition - Creative - Emotion</title>
    <!-- Favicons-->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/logo2.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url(); ?>assets/menu1/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url(); ?>assets/menu1/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url(); ?>assets/menu1/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url(); ?>assets/menu1/img/apple-touch-icon-144x144-precomposed.png">
    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
    <!-- BASE CSS -->
    <link href="<?= base_url(); ?>assets/menu1/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/menu1/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/menu1/css/vendors.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/menu1/css/icon_fonts/css/all_icons.min.css" rel="stylesheet">
    <!-- YOUR CUSTOM CSS -->
    <link href="<?= base_url(); ?>assets/menu1/css/custom.css" rel="stylesheet">
    <script src="<?= base_url(); ?>assets/menu1/js/jquery-2.2.4.min.js"></script>
</head><html lang="en">
<body style="height: 100%;">
    <div id="page">
    <!-- HEADER MENU START -->
    <header class="header menu_2" style="background: #0d022c;padding: 8px 30px;">
    <style type="text/css">
        .main-menu ul ul.menu-edit-spero:before {
            bottom: 100%;
            left: 8%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            border-bottom-color: #fff;
            border-width: 7px;
            margin-left: 227px;
        }
        .main-menu ul ul.menu-edit-spero-kiri:before {
            bottom: 100%;
            left: -10%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            border-bottom-color: #fff0;
            border-width: 7px;
            margin-left: 227px;
        }
        .main-menu ul ul ul:before {
            border: unset;
        }
        span.tes_hend:hover  {
          background-color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajax({
                url : "https://edumedia.id/Kategori/get_parent_kategori",
                type: "GET",
                dataType: "JSON",
                success: function(response){
                    response.forEach(function(element) {
                        // if(element.sub_kategori != '0'){
                        //     $('#kategori_show').append('<li style="width: 100%;margin: -3px 0px 0px 0px;"><span><a href="#0" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;"><i><img src="http://admin.edumedia.id/'+element.icon_web+'" class="img-fluid" alt="" style="width: 13%;margin-right: 6%;"></i>'+ element.nama_kategori +'</a></span><ul class="menu-edit-spero-kiri" style="left: 100%;right: unset;top: 0%;position: fixed;padding-top: 13px;padding-bottom: 13px;height: -webkit-fill-available;" id="kategori_show'+ element.id_kategori +'"></ul></li>');
                        //     element.sub_kategori.forEach(function(element_sub) {
                        //         $('#kategori_show'+ element.id_kategori +'').append('<li style="width: 100%;margin: -3px 0px 4px 0px;"><span><a href="<?= base_url(); ?>Materi/get_materi/'+element_sub.id_kategori+'" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;">'+ element_sub.nama_kategori +'</a></span></li>');
                        //     });
                        // }else{
                            $('#kategori_show').append('<li style="width: 100%;margin: -3px 0px 0px 0px;"><span><a href="<?= base_url(); ?>Kategori/get_by_id_kategori/'+element.id_kategori+'" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;"><img src="http://admin.edumedia.id/'+element.icon_web+'" class="img-fluid" alt="" style="width: 13%;margin-right: 6%;">'+ element.nama_kategori +'</a></span></li>');
                        // }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('terjadi kesalahan, coba beberapa saat lagi');
                }
            });
        });
    </script>
    <div id="preloader"><div data-loader="circle-side"></div></div>
    <!-- <ul id="top_menu">
        <li><a href="login.html" class="login">Login</a></li>
        <li><a href="#0" class="search-overlay-menu-btn">Search</a></li>
        <li class="hidden_tablet"><a href="admission.html" class="btn_1 rounded">Admission</a></li>
    </ul> -->
    <!-- /top_menu -->
    <a href="#menu" class="btn_mobile">
        <div class="hamburger hamburger--spin" id="hamburger">
            <div class="hamburger-box" style="margin-top: 10%;margin-bottom: -5%;">
                <div class="hamburger-inner"    ></div>
            </div>
        </div>
    </a>
    <div class="hidden_tablet">
        <div id="logo" style="margin-top: -15px;">
            <a href="<?= base_url(); ?>"><img style="margin-top: 9%;" src="<?= base_url(); ?>assets/images/bird_rebahan_putih_new.png" width="139" data-retina="true" alt=""></a>
        </div>
        <ul id="top_menu" style="margin: -6px 0 0 10px;">
                <nav style="margin: 9px 22px 0px 0px;float: left;" id="menu1" class="main-menu">
                    <ul>
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                        <li style=""><span><a href="#0" style="text-transform: capitalize;font-size: 0.975rem;font-weight: 100;letter-spacing: inherit;color: #fff;letter-spacing: initial;"><i class="ti-layout-grid3-alt" style="margin-right: 6px;font-size: 12px;"></i>Kategori</a></span>
                            <ul class="menu-edit-spero-kiri" style="left: unset;right: 3px;padding-top: 20px;padding-bottom: 20px;" id="kategori_show">
                            </ul>
                        </li>
                    </ul>
                </nav>
                <nav style="margin: 0px 23px 0px 0px;" id="menu1" class="main-menu">
                    <ul>
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                        <li style=""><span><a href="#0" style="text-transform: capitalize;font-size: 0.975rem;font-weight: 100;letter-spacing: inherit;color: #fff;letter-spacing: initial;">Program</a></span>
                            <ul class="menu-edit-spero-kiri" style="left: unset;right: 3px;padding-top: 13px;padding-bottom: 13px;max-height: 2000%;">
                                <li style="width: 100%;margin: -3px 0px 0px 0px;">
                                    <span>
                                        <a href="#0" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;">
                                            <div style="margin-right: 20px;">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%" valign="center">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2">
                                                                    <!-- <img id="img_circle" src="<?= base_url(); ?>assets/images/85-Matematika.jpg" height="42" alt="Profile Imag"> -->
                                                                    <img id="img_circle" src="http://admin.edumedia.id/assets/images/icon_katagori/Student_Project.png" height="30" alt="Profile Imag">
                                                                </span>
                                                            </td>
                                                            <td width="80%" valign="center">
                                                                <b style="font-size: 15px;" >Student Project</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Pelajar Membuat Karya Sendiri
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Dengan Biaya Pribadi atau
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Pembiayaan Masal.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 100%;margin: -3px 0px 0px 0px;">
                                    <span>
                                        <a href="#0" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;">
                                            <div style="margin-right: 20px;">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%" valign="center">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2">
                                                                    <!-- <img id="img_circle" src="<?= base_url(); ?>assets/images/85-Matematika.jpg" height="42" alt="Profile Imag"> -->
                                                                    <img id="img_circle" src="http://admin.edumedia.id/assets/images/icon_katagori/Mentoring.png" height="30" alt="Profile Imag">
                                                                </span>
                                                            </td>
                                                            <td width="80%" valign="center">
                                                                <b style="font-size: 15px;" >Mentor Program</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Pengajar Dapat Membagi Ilmu
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Lanjutan Dengan Mengatur
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Jadwalnya Sendiri.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 100%;margin: -3px 0px 0px 0px;">
                                    <span>
                                        <a href="#0" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;">
                                            <div style="margin-right: 20px;">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%" valign="center">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2">
                                                                    <!-- <img id="img_circle" src="<?= base_url(); ?>assets/images/85-Matematika.jpg" height="42" alt="Profile Imag"> -->
                                                                    <img id="img_circle" src="http://admin.edumedia.id/assets/images/icon_katagori/Internship.png" height="30" alt="Profile Imag">
                                                                </span>
                                                            </td>
                                                            <td width="80%" valign="center">
                                                                <b style="font-size: 15px;" >Internship</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Calon Pelajar Bisa Mendaftar
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Kerja Industri Sesuai Industri
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Yang di Pilih.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                        <li style=""><span><a href="<?= base_url(); ?>chat_mentor" style="text-transform: capitalize;font-size: 0.975rem;font-weight: 100;letter-spacing: inherit;color: #fff;letter-spacing: initial;">Chat Mentor</a></span>
                        </li>
                            <li style=""><span><a href="http://admin.edumedia.id" style="text-transform: capitalize;font-size: 0.975rem;font-weight: 100;letter-spacing: inherit;color: #fff;letter-spacing: initial;">Mentor</a></span>
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                            <!-- <ul class="menu-edit-spero-kiri" style="left: unset;right: 3px;" id="program_show">
                            </ul> -->
                        </li>
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;color: #000;" href="">Pengajar</a></span></li> -->
                        <li style=""><span><a href="#0" style="text-transform: capitalize;font-size: 0.975rem;font-weight: 100;letter-spacing: inherit;color: #fff;letter-spacing: initial;">Kursus Saya</a></span>
                        </li>
                        <!-- <li class="hidden_tablet" style="margin-right: unset;"><a style="font-size: 28px;color: #000;position: relative;top: 7px;" href="#0" class="pe-7s-like"></a></li> -->
                        <!-- <li class="hidden_tablet" style="margin-right: unset;"><a style="font-size: 28px;color: #000;position: relative;top: 7px;" href="#0" class="pe-7s-cart"></a></li> -->
                        <!-- <li class="hidden_tablet" style="margin-right: unset;"><a style="font-size: 28px;color: #000;position: relative;top: 7px;" href="#0" class="pe-7s-bell"></a></li> -->
                        <li style="">
                            <span>
                                <a href="#0" style="text-transform: capitalize;font-size: 0.975rem;font-weight: 100;letter-spacing: inherit;color: #fff;letter-spacing: initial;">
                                    <img style="width: 31px;" class="img-circle img-user media-object" src="<?= base_url(); ?>assets/images/822762_user_512x512.png" alt="Profile Picture">
                                </a>
                            </span>
                            <ul class="menu-edit-spero-kiri" style="left: unset;right: 3px;padding-top: 20px;padding-bottom: 20px;" id="kategori_show">
                                <li style="width: 100%;margin: -3px 0px 4px 0px;">
                                    <span>
                                        <a href="#" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;color: #000;">
                                            <div style="margin-right: 17px;">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2"><img style="width: 45px;" class="img-circle img-user media-object" src="<?= base_url(); ?>assets/images/822762_user_512x512.png" alt="Profile Picture"> </span>
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                <p style="font-size: small;color: #000;margin-bottom: 5px;"></p>
                                                                <p style="font-size: small;color: #6f6c6c;margin-bottom: 5px;">raviutama123@gmail.com</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 100%;margin: -3px 0px 4px 12px;">
                                    <span>
                                        <a href="#" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;">
                                            My Profile
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 100%;margin: -3px 0px 4px 12px;">
                                    <span>
                                        <a href="#" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;">
                                            Help
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 100%;margin: -3px 0px 4px 12px;">
                                    <span>
                                        <a href="<?= base_url(); ?>Login/logout" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px 13px;letter-spacing: initial;">
                                            Log Out
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <nav style="margin: -7px 23px 0px 0px;" id="menu1" class="main-menu">
                    <ul>
                    </ul>
                </nav>
        </ul>
    </div>
    <div class="btn_mobile">
        <ul id="top_menu" style="margin: -37px 0 0 10px;">
                <!-- <li style="margin: -2px 0 0 10px;"><a style="font-size: 28px;color: #fff;position: relative;top: 7px;" href="#0" class="pe-7s-search"></a></li> -->
                <!-- <li><a href="#0" class="search-overlay-menu-btn">Search</a></li> -->
                <div style="width: 105px;margin-left: 13%;left: 21%;" id="logo">
                    <a href="<?= base_url(); ?>">
                        <img style="height: 37px;margin-top: 5%;" src="<?= base_url(); ?>assets/images/bird_rebahan_putih_new.png" width="149" height="42" data-retina="true" alt="">
                    </a>
                </div>
                <nav id="menu" class="main-menu">
                    <ul>  
                        <li class="" style="margin-right: unset;">
                            <span>
                                <a>
                                    <span class="pull-right"> <img style="width: 31px;" class="img-circle img-user media-object" src="<?= base_url(); ?>assets/images/822762_user_512x512.png" alt="Profile Picture"> </span>
                                </a>
                            </span>
                            <ul class="menu-edit-spero" style="left: unset;right: 3px;margin-top: 10px;">
                                <li style="width: 283px;">                                       
                                    <span>
                                        <a href="#" style="color: #000000">
                                            <div>
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2"><img style="width: 45px;" class="img-circle img-user media-object" src="<?= base_url(); ?>assets/images/822762_user_512x512.png" alt="Profile Picture"></span>
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                <p style="font-size: small;color: #000;margin-bottom: 5px;"></p>
                                                                <p style="font-size: small;color: #6f6c6c;margin-bottom: 5px;">raviutama123@gmail.com</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 283px;">                                       
                                    <span>
                                        <a href="#" style="font-size: 15px;color: #6f6c6c;">
                                            My Profile
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 283px;">                                       
                                    <span>
                                        <a href="#" style="font-size: 15px;color: #6f6c6c;">
                                            Help
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 283px;">                                       
                                    <span>
                                        <a href="<?= base_url(); ?>Login/logout" style="font-size: 15px;color: #6f6c6c;">
                                            Log out
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <!-- li><span><a style="text-transform: capitalize;font-size: 15px;font-weight: 100;letter-spacing: inherit;" href="">Pengajar</a></span></li -->
                        <li><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="">Kursus saya</a></span>
                            <ul id="kursus_saya">
                            </ul>
                        </li>
                        <li><span><a href="#0" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">Kategori</a></span>
                            <ul class="menu-edit-spero-kiri" style="left: unset;right: 3px;" >
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Multimedia
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Games
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Interior
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Youtuber
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Entrepreneur
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Apps
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Internet Of Things
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Digital Marketing
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Custom Shop
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Office
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Edulove
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Computer Network 
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Management
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Accountant & Financial
                                            </a>
                                        </span>
                                    </li> 
                                   <li style="width: 100%;">                                       
                                        <span>
                                            <a href="" style="font-size: small;color: #6f6c6c;">
                                                Human Resource
                                            </a>
                                        </span>
                                    </li> 
                            </ul>
                        </li>
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                        <li style=""><span><a href="#0" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">Program</a></span>
                            <ul class="menu-edit-spero-kiri" style="left: unset;right: 3px;">
                                <li style="width: 100%;">
                                    <span>
                                        <a href="#0" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px;letter-spacing: initial;">
                                            <div style="margin-right: 20px;">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%" valign="center">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2">
                                                                    <!-- <img id="img_circle" src="<?= base_url(); ?>assets/images/85-Matematika.jpg" height="42" alt="Profile Imag"> -->
                                                                    <img id="img_circle" src="http://admin.edumedia.id/assets/images/icon_katagori/Student_Project.png" height="30" alt="Profile Imag">
                                                                </span>
                                                            </td>
                                                            <td width="80%" valign="center">
                                                                <b style="font-size: 15px;" >Student Project</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Pelajar Membuat Karya Sendiri
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Dengan Biaya Pribadi atau
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Pembiayaan Masal.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 100%;margin: -3px 0px 0px 0px;">
                                    <span>
                                        <a href="#0" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px;letter-spacing: initial;">
                                            <div style="margin-right: 20px;">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%" valign="center">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2">
                                                                    <!-- <img id="img_circle" src="<?= base_url(); ?>assets/images/85-Matematika.jpg" height="42" alt="Profile Imag"> -->
                                                                    <img id="img_circle" src="http://admin.edumedia.id/assets/images/icon_katagori/Mentoring.png" height="30" alt="Profile Imag">
                                                                </span>
                                                            </td>
                                                            <td width="80%" valign="center">
                                                                <b style="font-size: 15px;" >Mentor Program</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Pengajar Dapat Membagi Ilmu
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Lanjutan Dengan Mengatur
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Jadwalnya Sendiri.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                                <li style="width: 100%;margin: -3px 0px 0px 0px;">
                                    <span>
                                        <a href="#0" style="font-size: small;color: #6f6c6c;font-weight: 10;padding: 5px;letter-spacing: initial;">
                                            <div style="margin-right: 20px;">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="20%" valign="center">
                                                                <span class="image col-md-2 col-sm-2 col-xs-2">
                                                                    <!-- <img id="img_circle" src="<?= base_url(); ?>assets/images/85-Matematika.jpg" height="42" alt="Profile Imag"> -->
                                                                    <img id="img_circle" src="http://admin.edumedia.id/assets/images/icon_katagori/Internship.png" height="30" alt="Profile Imag">
                                                                </span>
                                                            </td>
                                                            <td width="80%" valign="center">
                                                                <b style="font-size: 15px;" >Internship</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Calon Pelajar Bisa Mendaftar
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Kerja Industri Sesuai Industri
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" valign="top">
                                                                Yang di Pilih.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <!-- <li><span><a href="#0" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">About</a></span>
                        </li> -->
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                        <!-- <li><span><a href="#0" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">Become Mentor</a></span>
                        </li> -->
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                        <li><span><a href="<?= base_url(); ?>Chat_mentor" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">Chat Mentor</a></span>
                        </li>
                            <li><span><a href="http://admin.edumedia.id" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">Mentor</a></span>
                            </li>
                        <!-- li><span><a href="<?= base_url(); ?>Universitas" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">Universitas</a></span>
                        </li -->
                        <!-- <li style="margin-right: unset;"><span><a style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;" href="<?= base_url(); ?>Pages">Pengajar</a></span></li> -->
                        <!-- li><span><a href="#0" style="text-transform: capitalize;font-size: small;font-weight: 100;letter-spacing: inherit;">Partner</a></span>
                        </li -->
                    </ul>
                </nav>
        </ul>
    </div>
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><span class="closebt"><i class="ti-close"></i></span></span>
        <!-- <form  method="POST" action=""> -->
        <form  method="POST" action="#">
            <input value="" name="matapelajaran" type="search" placeholder="Apa yang ingin kamu pelajari. . ." />
            <button type="submit"><i class="icon_search"></i>
            </button>
        </form>
    </div><!-- End Search Menu -->
</header>
<!-- /header -->    <!-- HEADER MENU END -->
    <!-- CONTENT STAR -->
<style>
  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }
  #myImg:hover {opacity: 0.7;}
  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }
  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }
  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }
  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }
  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }
  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }
  /* The Close Button */
  .close {
    position: absolute;
    top: 67px;
    right: 7px;
    color: #ffffff;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }
  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }
  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
  div .btn_mobile {
    display: none !important;
  }
  @media screen and (max-width: 600px) {
    #mobile_show_yow {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
    #mobile_show_yow_1 {
      display: inherit !important;
      clear: both;
      display: inherit 
    }
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
      $.ajax({
          url : "https://edumedia.id/kursus/get_top_kursus",
          type: "GET",
          dataType: "JSON",
          success: function(response){
              response.forEach(function(element) {
                  // $('#kategori_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a href="<?= base_url(); ?>Materi/materi_by_id_materi/'+element.id_materi+'" class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><div class="block-horizzontal" style="animation: none;background: none;"></div><img src="https://img.youtube.com/vi/' + element.tumnile + '/sddefault.jpg" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 0px 0px 3px 6px;animation: color 0.7s ease-in-out; animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: center;"><p style="color: #ffffff;"><b>'+ element.nama_materi +'</b></p></div></figure></a></div>');
                  if (element.status_belajar == '1') {
                    $('#kursus_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><img src="http://admin.edumedia.id/'+ element.card_kursus +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 3px 18px 13px 18px;animation: color 0.7s ease-in-out;animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: left;"><p style="color: #f14068;font-size: 11px;margin-bottom: unset;">'+ element.nama_sub_kategori +'</p><p style="color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_kursus +'</p><div class="row"><p style="color: #fff;font-size: 11px;width: 50%;margin-left: 4%;margin-bottom: unset;">'+ element.nama_user +'</p><p style="color: #fff;font-size: 11px;width: 39%;text-align: end;"></p></div><a style="width: 100%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #6a25b3;color: #ffffff;font-size: 0.9rem;text-transform: capitalize;padding: 10px 13px;letter-spacing: initial;" href="<?= base_url(); ?>Kursus/index_by_id_kursus/'+element.id_kursus+'" class="btn_1 rounded">Lanjut</a></div></figure></a></div>');
                  }else{
                    $('#kursus_show_home').append('<div class="col-lg-3 col-md-2" data-wow-offset="150" style="width: 50%;padding-right: 5px; padding-left: 5px;"><a class="grid_item"><figure class="block-reveal" style="height: 100% !important;"><img src="http://admin.edumedia.id/'+ element.card_kursus +'" class="img-fluid" alt="" style="animation: color 0.5s ease-in-out;animation-fill-mode: forwards;width: 100%;"><div class="info" style="padding: 3px 18px 13px 18px;animation: color 0.7s ease-in-out;animation-delay: 0.7s;-webkit-animation-delay: 0.7s;-moz-animation-delay: 0.7s;opacity: 0;animation-fill-mode: forwards;-webkit-animation-fill-mode: forwards;background: #171219;text-align: left;"><p style="color: #f14068;font-size: 11px;margin-bottom: unset;">'+ element.nama_sub_kategori +'</p><p style="color: #fff;font-size: 17px;margin-bottom: unset;">'+ element.nama_kursus +'</p><div class="row"><p style="color: #fff;font-size: 11px;width: 50%;margin-left: 4%;margin-bottom: unset;">'+ element.nama_user +'</p><p style="color: #fff;font-size: 11px;width: 39%;text-align: end;"></p></div><a style="width: 100%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #6a25b3;color: #ffffff;font-size: 0.9rem;text-transform: capitalize;padding: 10px 13px;letter-spacing: initial;" href="<?= base_url(); ?>kursus/gabung_kursus/'+element.id_kursus+'" class="btn_1 rounded">Gabung</a></div></figure></a></div>');
                  };
              });
          },
          error: function (jqXHR, textStatus, errorThrown){
              alert('terjadi kesalahan, coba beberapa saat lagi');
          }
      });
  });
</script>
<main style="background-color: #0c0016;padding-top: 3%;">
  <div class="main_title_2 hidden_tablet" style="margin-bottom: 15px;">
    <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;max-width: 100%;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #0b0015; height: 100%;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: fill;padding-right: unset;padding-left: unset;" src="file:///C:/xampp/htdocs/Edumedia/Asset/banner.png">
        <div class="" style="text-align: left;margin-top: 0%;margin-right: 5%;">
          <div class="" style="padding-left: 60%;">
            <h2 style="position: relative;color: #fff;">Filosofi Edumedia</h2>
            <p style="position: relative;">Sebagai pendukung bagi pendidik dan siswa yang bebas untuk menjadi kreatif dan mendapat ilmu yang berkualitas dari cara rumit berfikir menjadi mudah</p>
            <!-- <a style="margin-top: 3%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.8rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="<?= base_url(); ?>Registration" class="btn_1 rounded">Mulai Belajar</a> -->
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="main_title_2 btn_mobile" id="mobile_show_yow" style="margin-bottom: -22px;margin-top: 9.5%;">
    <section id="hero_in" class="courses container" style="margin-right: 13%;padding-right: unset;padding-left: unset;">
      <div class="wrapper col-xl-12 col-lg-12 col-md-12" style="padding-right: unset;padding-left: unset;background-color: #37386f;height: 98%;">
        <img class=" col-xl-12 col-lg-12 col-md-12" style="width: 100%;position: absolute;height: 100%;object-fit: cover;padding-right: unset;padding-left: unset;" src="<?= base_url(); ?>assets/images/BANNER MOBILE 2.jpg">
        <div class="" style="text-align: initial;padding-right: 10%;padding-left: 5%;padding-top: 0%;">
          <div class="" style="">
            <h2 style="position: relative;color: #fff;font-size: 1.3rem;margin: 0px 0 0 0;">Gali potensi skill mu,<br> raih suksess karir mu</h2>
            <p style="position: relative;font-size: 1rem;">Belajar dengan mentor terbaik<br> di edumedia.</p>
            <!-- <a style="margin-top: 3%;position: relative;border-radius: 1.25rem !important;background-color: #f14068;border: 1px solid white;border-color: #f14068;color: #ffffff;font-size: 0.8rem;text-transform: capitalize;padding: 11px 41px;letter-spacing: initial;" href="http://edumedia.id/injeksi_registrasi" class="btn_1 rounded">Mulai Belajar</a> -->
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px; padding-bottom: 0px;">
      <!-- <hr style="margin: 24px 0 17px 0;"> -->
      <h2 style="color: #ffffff;margin-bottom: 5%; margin-top: 6%; text-align: center;">Fitur-fitur Edumedia.id</h2>
  </div>
    <div class="grid-container" style="grid-gap: 10px; background-color: #171219; padding: 50px;margin: 0 100px 85px 100px;">
        <div class="row" style="display: flex; flex-wrap: wrap;">
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/lms_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Berbasis LMS</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Mendistribusikan program pelatihan dan pendidikan melalui internet dan berbasis online dengan berbagai fitur yang ada pada edumedia.id</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/gamifikasi_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Gamification</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Memotivasi siswa belajar dengan menggunakan desai video game dan elemen-elemen dalam lingkungan belajar</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/scoring_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Real TIme Scoring</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Nilai dan capaian siswa secara langsung akan terpantau oleh mentor diwaktu yang bersamaan melalui apps</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/encounter_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Encounter</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Student bertanggung jawab dari hasil belajar online-nya, bisa melalui tatap muka atau video conference</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/chat_mentor_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Chat Mentor</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Berdiskusi langsung dengan mentor profesional dan bimbingan khusus sesuai dengan kebutuhan insdustri</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/mentor_program_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Mentor Program</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Fitur lanjutan dari program pembelajaran dibuat khusus oleh mentor untuk mendapatkan tips dan trik secara singkat dan dengan metode boarding</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/student_project_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Student Project</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Project Base adalah inisiatif siswa untuk membuatnya proyek untuk teknik ilmiah dan menjadi produk.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/sertifikat_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">Sertifikasi</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Tempat belajar online kejuruan di setiap kota yang menjadi basecamp siswa dengan instruktur pendamping yang telah telah disertifikasi sebagai mitra edumedia</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="">
                <div class="row">
                    <div class="col-lg-2">
                        <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/IconMobile/edubank_mdpi_1.png">
                    </div>
                    <div class="col-lg-10" style="">
                        <h4 style="position: relative;color: #fff; margin-left: 10px;">EduBank</h4>
                        <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 12px;">Siswa dan mentor dapat mengumpulkan poin sebagai nilai tukar mereka.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!-- <div class="container margin_30_95" style="text-align: center;">
      <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Menghubungkan siswa di seluruh indonesia dengan instruktur terbaik,</p>
      <p style="margin-bottom: 3px;font-size: 124%;color: #ffffff;">Edumedia.id membantu individu mencapai tujuan mereka dan mengejar impian mereka.</p>
  </div> -->
    <!-- Tentang Kami Section -->
    <section id="Tentang" class="kami" style="width: 100%; height: 100vh; background-color: #06052f; ">
        <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px; padding-bottom: 0px;">
            <!-- <hr style="margin: 24px 0 17px 0;"> -->
            <h2 style="color: #ffffff;margin-bottom: 5%; text-align: center; padding-top: 70px">Tentang Kami</h2>
        </div>
        <div class="row">
            <div class="col-sm-6" style="">
            <div style="margin-left: 50px;">
                <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 13px;">Edumedia dimulai pada tahun 2014, ketika PT Holomoc Indonesia menerima magang, sebuah perusahaan yang menjadi fokus multimedia IT, memiliki 15 lulusan magang. 1 batch 30 orang, banyak magang yang diperoleh meningkat keterampilan untuk sekolah mereka. Pendaftaran pemagangan terlalu tinggi, perusahaan membuat peraturan, dan jumlah peserta maksimal adalah 15 per kelas, dengan peserta yang telah diseleksi secara ketat.  Kemudian ide untuk membuat platform pendidikan pertama kali diwujudkan dengan membuat kursus singkat untuk magang, dan dilanjutkan denga membuat aplikasi pendidikan untuk menjembatani pertemuan siswa dan guru secara online oleh membuat video pembelajaran yang biasa kami sajikan, dan membuat tugas proyek sesuai industri standar.</p>
                <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 13px;">Kami melakukan penelitian tentang kursus online terbuka besar-besaran (MOOC) dengan kebutuhan industri saat ini untuk Milenial Indonesia, kami belajar mengajar di sekolah kejuruan, perguruan tinggi dan universitas. Belajar di online universitas, ambil kursus online dan offline di beberapa platform kursus online. Setelah  penelitian kami menyimpulkan pembuatan aplikasi pendidikan berbasis LMS, gamification, real time skor, rapat, Edu-Bank, dana kerumunan, mentor obrolan, proyek Mahasiswa, Program Mentor, Magang dan lain-lain.
                </p>
                <p style="position: relative; color: #fff; margin-left: 10px; margin-right: 24px; font-size: 13px;">Kami membuat aplikasi untuk memulai berdasarkan pengalaman industri dalam mengumpulkanpengetahuan kami, tentang IT, Game, Media Sosial, Pemasaran Digital, dan manajemen. Jadi nilai jual unik edumedia adalah gabungan Platform Pendidikan, Fintech dan Crowd Funding. Membuat siswa belajar dengan mentoring dari industri.</p>
            </div>
        </div>
        <div class="col-sm-6" style="">
            <div>
                <img class="" style="" src="file:///C:/xampp/htdocs/Edumedia/Asset/content_thumb.png">
            </div>
        </div>
        </div>
    </section>
    <!-- === Team Section === -->
    <section id="team" class="kita" style="width: 100%; height: 125vh; background-color: #0c0016; ">
        <div class="container margin_30_95" style="padding-top: unset;margin-top: unset;padding-right: unset;padding-left: unset;max-width: 1398px; padding-bottom: 0px;">
            <!-- <hr style="margin: 24px 0 17px 0;"> -->
            <h2 style="color: #ffffff;margin-bottom: 5%; text-align: center; padding-top: 70px">Team Kami</h2>
        </div>
        <div class="grid-container" style="grid-gap: 10px; padding: 0 0 0 0;margin: 0 100px 4px 100px;">
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: -16px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 63px; font-size: 22px;">Chandra KMNG</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Design Thinking</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Chandra@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 22vh; margin-top: 9px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/Pak chan.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 14px;">Dr.Dirgantara Wicaksono, CH, Cht, S.Pd, M.Pd,MM</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Staff Ahli / Pembimbing Teknologi Pendidikan</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Dr.Dirgantara@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 23vh; margin-top: 9px;padding: 10px 11px 0 0px;"src="file:///C:/xampp/htdocs/Edumedia/Asset/Pak Dirgantara.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 63px; font-size: 22px;">Mus Mulyadi M.A, M.pd</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Analyst</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Musmulyadi@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 18vh; margin: 26px 0px 0 12px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/poto-01.png">
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <div class="grid-container" style="grid-gap: 10px; padding: 0 0 0 0;margin: 0 100px 4px 100px;">
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: -16px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Rizqy Liquify</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">UI/UX Designer</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Riszqy@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 18vh; margin-top: 9px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/Rizqy.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Rio Proxy</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Mobile Developer</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">DioHalilintar666@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 18vh; margin-top: 9px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/Dio.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Thoyib Hueh</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Web Developer</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Thoyib@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 16vh;margin: 11px 0px 0px 19px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/poto-01.png">
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <div class="grid-container" style="grid-gap: 10px; padding: 0 0 0 0;margin: 0 100px 4px 100px;">
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: -16px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Dini Vector</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Video Editor / Socmed</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Dini@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 18vh; margin-top: 9px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/Andini.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Fika Vlowy</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Analyst</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Fika@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 18vh; margin-top: 9px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/Fika.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Fitri Farhana M.Pd</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Instructional Design</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Fitri@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 16vh;margin: 11px 0px 0px 19px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/poto-01.png">
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <div class="grid-container" style="grid-gap: 10px; padding: 0 0 0 0;margin: 0 100px 4px 100px;">
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: -16px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Fadhil Paintool</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Telent / Broadcaster</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">Fadhil@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 18vh; margin-top: 9px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/Fadil.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Oktha Anchor</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Ilustrator</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">ODP@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 18vh; margin-top: 9px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/poto-01.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="border: 1px solid; background-color: #58525d; margin-left: 5px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding: 20px 0 12px 20px; margin-right: -25px;">
                            <h5 style="position: relative;color: #fff; margin-left: 5px; margin-bottom: 30px; font-size: 22px;">Ravi Dashboard</h5>
                            <p style="position: relative; color: #ea3f61; margin-left: 5px; font-size: 15px;margin-bottom: 0px;">Support System</p>
                            <p style=" position: relative;color: #fff; margin-left: 5px; font-size: 17px; margin-bottom: 5px;">RaviUtama@edu.id</p>
                        </div>
                        <div class="col-lg-4" style="">
                            <img class="" style="width: 16vh;margin: 11px 0px 0px 19px;" src="file:///C:/xampp/htdocs/Edumedia/Asset/poto-01.png">
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </section>
</main>
<div id="myModal" class="modal">
  <a href="https://api.whatsapp.com/send?phone=6281285232001&text=Hello... apakah maskernya masih tersedia...?">
    <img class="modal-content" id="img01" src="<?= base_url(); ?>assets/images/iklan1.jpeg">
  </a>
  <span class="close">&times;</span>
</div>
<script type="text/javascript">
</script>    <!-- CONTENT END -->
    <!-- FOOTER STAR -->
    <footer style="background:url('https://edumedia.id/assets/images/BG IMAGE FOOTER.jpg') no-repeat fixed center; background-size: cover;">
    <div class="container margin_120_95" style="padding-top: 0px;padding-bottom: 12px;">
        <div class="row">
            <div class="col-lg-3 col-md-12 p-r-5" style="text-align: center;padding-top: 2%;">
                <p><img src="<?= base_url(); ?>assets/images/bird_berdiri_text_putih_new.png" data-retina="true" alt="" style="" width="130"></p>
                <h5 style="margin-bottom: 4px;">Coming Soon On</h5>
                <p style="margin-bottom: 0px;"><img src="<?= base_url(); ?>assets/images/google_play.png" data-retina="true" alt="" style="" width="130"></p>
                <p><img src="<?= base_url(); ?>assets/images/app_store.png" data-retina="true" alt="" style="" width="130"></p>
            </div>
            <div class="col-lg-2 col-md-6 ml-lg-auto hidden_tablet" style="display: none">
                <h5>Site Map</h5>
                <ul class="links">
                    <li><a href="javascript:void(0)">Programs</a></li>
                    <li><a href="javascript:void(0)">Student Testimony</a></li>
                    <li><a href="javascript:void(0)">Internship</a></li>
                    <li><a href="javascript:void(0)">Competition</a></li>
                    <li><a href="javascript:void(0)">Edufest</a></li>
                    <li><a href="javascript:void(0)">Partner</a></li>
                    <li><a href="javascript:void(0)">About</a></li>
                    <li><a href="javascript:void(0)">Become Mentor</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 ml-lg-auto hidden_tablet" style="display: none">
                <h5>Metode</h5>
                <ul class="links">
                    <li><a href="javascript:void(0)">LMS</a></li>
                    <li><a href="javascript:void(0)">Video Belajar</a></li>
                    <li><a href="javascript:void(0)">Gamification</a></li>
                    <li><a href="javascript:void(0)">Video Conference</a></li>
                    <li><a href="javascript:void(0)">Chat Mentor</a></li>
                    <li><a href="javascript:void(0)">Encounter</a></li>
                    <li><a href="javascript:void(0)">Community</a></li>
                    <li><a href="javascript:void(0)">Social Club</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 ml-lg-auto" style="display: none;">
                <h5>Follow Us</h5>
                <ul class="links">
                    <li><a href="https://instagram.com/edumediaid_?igshid=w8c339eet34m" target="_blank"><i><img src="<?= base_url(); ?>assets/images/IG.png" class="img-fluid" alt="" width="20"></i>Instagram</a></li>
                    <li><a href="https://facebook.com/edumedia.id" target="_blank"><i><img src="<?= base_url(); ?>assets/images/FB.png" class="img-fluid" alt="" width="20"></i>Facebook</a></li>
                    <li><a href="https://twitter.com/EdumediaID_?s=08" target="_blank"><i><img src="<?= base_url(); ?>assets/images/TW.png" class="img-fluid" alt="" width="20"></i>Twitter</a></li>
                    <li><a href="https://www.youtube.com/channel/UCXCyE8qjfbaYiLqvHyFanZg" target="_blank"><i><img src="<?= base_url(); ?>assets/images/YT.png" class="img-fluid" alt="" width="20"></i>Youtube</a></li>
                    <li><a href="https://open.spotify.com/show/73ahpf5NiQvLeGbLICKZhg?si=EFRTjXEKT2K1ZEH4xSyvGQ" target="_blank"><i><img src="<?= base_url(); ?>assets/images/spotify.png" class="img-fluid" alt="" width="20"></i> Spotify</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 ml-lg-auto">
                <h5>Address</h5>
                <ul class="contacts">
                    <li><a href="https://www.google.com/maps/place/Edumedia.id/@-6.2477219,106.9095192,17.72z/data=!4m8!1m2!2m1!1sedumedia!3m4!1s0x2e69f3483b46a3a9:0x7656e350e09e28ef!8m2!3d-6.2481!4d106.910038">Jl. Inpesksi Kalimalang Puri Sentra Niaga E/72 2, 3, 4 Floor, Jakarta Timur, 13620</a></li>
                </ul>
                <h5>Contact</h5>
                <ul class="contacts">
                    <li><a href="tel://+6281285232001"><i class="ti-mobile"></i> + 62 812 8523 2001</a></li>
                    <li><a href="mailto:info@edumedia.id"><i class="ti-email"></i> info@edumedia.id</a></li>
                    <li><a href="https://api.whatsapp.com/send?phone=6281285232001&text=Hello%20Edumedia&source=&data="><i><img src="<?= base_url(); ?>assets/images/WA.png" class="img-fluid" alt="" style="margin-right: -3px;" width="17"></i> + 62 812 8523 2001</a></li>
                </ul>
            </div>
        </div>
        <!--/row-->
        <hr style="margin: 24px 0 17px 0;">
    </div>
    <div class="row pt-5 pb-5 mx-0 text-white" style="background:#361faa70">
        <div class="col-12 text-center">
            <span>Follow Us On Social Media</span>
        </div>
        <div class="col-12 text-center">
            <a href="https://instagram.com/edumediaid_?igshid=w8c339eet34m" target="_blank" class="mx-3"><i><img src="<?= base_url(); ?>assets/images/IG.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://facebook.com/edumedia.id" target="_blank" class="mx-3"><i><img src="<?= base_url(); ?>assets/images/FB.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://twitter.com/EdumediaID_?s=08" target="_blank" class="mx-3"><i><img src="<?= base_url(); ?>assets/images/TW.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://www.youtube.com/channel/UCXCyE8qjfbaYiLqvHyFanZg" target="_blank" class="mx-3"><i><img src="<?= base_url(); ?>assets/images/YT.png" class="img-fluid" alt="" width="20"></i></a>
            <a href="https://open.spotify.com/show/73ahpf5NiQvLeGbLICKZhg?si=EFRTjXEKT2K1ZEH4xSyvGQ" target="_blank" class="mx-3"><i><img src="<?= base_url(); ?>assets/images/spotify.png" class="img-fluid" alt="" width="20"></i></a>
        </div>
    </div>
    <div class="row py-2 text-white" style="background: #0b0015;margin: auto;">
        <div class="col-md-8">
            <ul id="additional_links">
                <li><a href="javascript:void(0)">Terms and conditions</a></li>
                <li><a href="javascript:void(0)">Privacy</a></li>
            </ul>
        </div>
        <div class="col-md-4 ">
            <div id="copy" class="text-right">© 2020 EDUMEDIA.ID</div>
        </div>
    </div>
</footer>    <!-- FOOTER END -->
    </div>
    <!-- page -->
    <!-- COMMON SCRIPTS -->
<script src="<?= base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/sweetalert/sweetalert.css">
<script src="<?= base_url(); ?>assets/menu1/js/common_scripts.js"></script>
<script src="<?= base_url(); ?>assets/menu1/js/main.js"></script>
<script src="<?= base_url(); ?>assets/menu1/assets/validate.js"></script>    
</body>
</html>