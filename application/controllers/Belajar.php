<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Belajar extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_mata_pelajaran');
        $this->load->model('model_materi');
        $this->load->model('model_pembelajaran');
        $this->load->model('model_squrity');  
        $this->model_squrity->getsqurity(); 
    }
    public function index() {
        $get_mata_pelajaran = $this->model_mata_pelajaran->get_all_aktif();
        $kategori_result = array();
        $data_header['kategori_list'] = $kategori_result;
        $data['header_menu'] = $this->load->view('header_menu',$data_header,true);
        $data['content'] = $this->load->view('page/home','',true);
        $data['footer'] = $this->load->view('footer','',true);
        $this->load->view('base_view', $data);
        // echo json_encode($list_kursus_saya_data);
    }
    public function materi_by_mata_pelajaran($id_mata_pelajaran)
    {
        $get_mata_pelajaran = $this->model_mata_pelajaran->get_all_aktif();
        $kategori_result = array();
        $data_header['kategori_list'] = $kategori_result;
        $data['id_mata_pelajaran'] = $id_mata_pelajaran;
        $data['content'] = $this->load->view('page/belajar',$data,true);
        $data['header_menu'] = $this->load->view('header_menu',$data_header,true);
        $data['footer'] = $this->load->view('footer','',true);
        $this->load->view('base_view', $data);
        // echo json_encode($list_kursus_saya_data);
    }
    public function get_materi_by_mata_pelajaran_edit($id_mata_pelajaran){
        $semester1 = '1';
        $id_murid = $this->session->userdata('id_murid');
        $mata_pelajaran_by_id = $this->model_mata_pelajaran->get_by_id($id_mata_pelajaran);
        $materi_by_id_mata_pelajaran_1 = $this->model_materi->get_all_by_id_mata_pelajaran($id_mata_pelajaran, $semester1);
        $get_pembelajaran_by_id_materi = $this->model_pembelajaran->get_by_id_materi($row_data_semester_1->id_materi);
        $mater_result_semester_1 = array();
        foreach ($materi_by_id_mata_pelajaran_1 as $row_data_semester_1) {
            $get_pembelajaran_by_id_materi = $this->model_pembelajaran->get_by_id_materi($row_data_semester_1->id_materi);
            if (empty($get_pembelajaran_by_id_materi)) {
                $data_materi_semester_1['id_materi'] = $row_data_semester_1->id_materi;
                $data_materi_semester_1['id_mata_pelajaran'] = $row_data_semester_1->id_mata_pelajaran;
                $data_materi_semester_1['id_tingkat'] = $row_data_semester_1->id_tingkat;
                $data_materi_semester_1['id_jurusan'] = $row_data_semester_1->id_jurusan;
                $data_materi_semester_1['nama_mata_pelajaran'] = $row_data_semester_1->nama_mata_pelajaran;
                $data_materi_semester_1['nama_tingkat'] = $row_data_semester_1->nama_tingkat;
                $data_materi_semester_1['nama_jurusan'] = $row_data_semester_1->nama_jurusan;
                $data_materi_semester_1['nama_materi'] = $row_data_semester_1->nama_materi;
                $data_materi_semester_1['path_pdf'] = $row_data_semester_1->path_pdf;
                $data_materi_semester_1['path_video'] = $row_data_semester_1->path_video;
                $data_materi_semester_1['status'] = '0';
                $mater_result_semester_1[] = $data_materi_semester_1;
            }else{
                $data_materi_semester_1['id_materi'] = $row_data_semester_1->id_materi;
                $data_materi_semester_1['id_mata_pelajaran'] = $row_data_semester_1->id_mata_pelajaran;
                $data_materi_semester_1['id_tingkat'] = $row_data_semester_1->id_tingkat;
                $data_materi_semester_1['id_jurusan'] = $row_data_semester_1->id_jurusan;
                $data_materi_semester_1['nama_mata_pelajaran'] = $row_data_semester_1->nama_mata_pelajaran;
                $data_materi_semester_1['nama_tingkat'] = $row_data_semester_1->nama_tingkat;
                $data_materi_semester_1['nama_jurusan'] = $row_data_semester_1->nama_jurusan;
                $data_materi_semester_1['nama_materi'] = $row_data_semester_1->nama_materi;
                $data_materi_semester_1['path_pdf'] = $row_data_semester_1->path_pdf;
                $data_materi_semester_1['path_video'] = $row_data_semester_1->path_video;
                $data_materi_semester_1['status'] = $row_data_semester_1->status;
                $mater_result_semester_1[] = $data_materi_semester_1;
            }
        }
        $data['status'] = true;
        $data['misage'] = 'sukses';
        $data['mata_pelajaran'] = $mata_pelajaran_by_id;
        $data['materi_semester_1'] = $mater_result_semester_1;
         header('Content-Type: application/json');
         echo json_encode($data);
    }
    public function get_materi_by_mata_pelajaran_old($id_mata_pelajaran){
        $semester1 = '1';
        $id_murid = $this->session->userdata('id_murid');
        // echo json_encode($id_murid);
        // die();
        $mata_pelajaran_by_id = $this->model_mata_pelajaran->get_by_id($id_mata_pelajaran);
        $pelajaran_semester_1 = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_murid($id_mata_pelajaran, $id_murid);
        $materi_by_id_mata_pelajaran_1 = $this->model_materi->get_all_by_id_mata_pelajaran($id_mata_pelajaran, $semester1);
        $materi_non_pelajaran = array();
        foreach ($materi_by_id_mata_pelajaran_1 as $row_data_semester_1) {
            $get_pembelajaran_by_id_materi = $this->model_pembelajaran->get_by_id_materi_id_murid($row_data_semester_1->id_materi, $id_murid);
            if (empty($get_pembelajaran_by_id_materi)) {
                $data_materi_semester_1['id_materi'] = $row_data_semester_1->id_materi;
                $data_materi_semester_1['id_murid'] = $id_murid;
                $data_materi_semester_1['id_mata_pelajaran'] = $row_data_semester_1->id_mata_pelajaran;
                $data_materi_semester_1['id_tingkat'] = $row_data_semester_1->id_tingkat;
                $data_materi_semester_1['id_jurusan'] = $row_data_semester_1->id_jurusan;
                $data_materi_semester_1['nama_mata_pelajaran'] = $row_data_semester_1->nama_mata_pelajaran;
                $data_materi_semester_1['nama_tingkat'] = $row_data_semester_1->nama_tingkat;
                $data_materi_semester_1['nama_jurusan'] = $row_data_semester_1->nama_jurusan;
                $data_materi_semester_1['nama_materi'] = $row_data_semester_1->nama_materi;
                $data_materi_semester_1['path_pdf'] = $row_data_semester_1->path_pdf;
                $data_materi_semester_1['path_video'] = $row_data_semester_1->path_video;
                $data_materi_semester_1['status'] = '0';
                $materi_non_pelajaran[] = $data_materi_semester_1;
            }         
        }
        if ($materi_non_pelajaran) {
            $status = '1';    
            $status_pelajaran_semester_1 = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_materi_id_murid_status($id_mata_pelajaran, $materi_non_pelajaran[0]['id_materi'], $id_murid, $status);
            if (empty($status_pelajaran_semester_1)) {
                $data_pelajaran = array(
                    'id_mata_pelajaran' => $id_mata_pelajaran,
                    'id_materi' => $materi_non_pelajaran[0]['id_materi'],
                    'id_murid' => $id_murid,
                    'status' => '1',
                );
                $simpan_data_user = $this->model_pembelajaran->save('t_pembelajaran', $data_pelajaran);
                if ($simpan_data_user) {
                    $pelajaran_semester_1 = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_murid($id_mata_pelajaran, $id_murid);
                    $materi_non_pelajaran = array();
                    foreach ($materi_by_id_mata_pelajaran_1 as $row_data_semester_1_add) {
                        $get_pembelajaran_by_id_materi = $this->model_pembelajaran->get_by_id_materi_id_murid($row_data_semester_1_add->id_materi, $id_murid);
                        if (empty($get_pembelajaran_by_id_materi)) {
                            $data_materi_semester_1['id_materi'] = $row_data_semester_1_add->id_materi;
                            $data_materi_semester_1['id_murid'] = $id_murid;
                            $data_materi_semester_1['id_mata_pelajaran'] = $row_data_semester_1_add->id_mata_pelajaran;
                            $data_materi_semester_1['id_tingkat'] = $row_data_semester_1_add->id_tingkat;
                            $data_materi_semester_1['id_jurusan'] = $row_data_semester_1_add->id_jurusan;
                            $data_materi_semester_1['nama_mata_pelajaran'] = $row_data_semester_1_add->nama_mata_pelajaran;
                            $data_materi_semester_1['nama_tingkat'] = $row_data_semester_1_add->nama_tingkat;
                            $data_materi_semester_1['nama_jurusan'] = $row_data_semester_1_add->nama_jurusan;
                            $data_materi_semester_1['nama_materi'] = $row_data_semester_1_add->nama_materi;
                            $data_materi_semester_1['path_pdf'] = $row_data_semester_1_add->path_pdf;
                            $data_materi_semester_1['path_video'] = $row_data_semester_1_add->path_video;
                            $data_materi_semester_1['status'] = '0';
                            $materi_non_pelajaran[] = $data_materi_semester_1;
                        }         
                    }
                    $data['status'] = true;
                    $data['misage'] = 'sukses';
                    $data['mata_pelajaran'] = $mata_pelajaran_by_id;
                    $data['non_pelajaran_semester_1'] = $materi_non_pelajaran;
                    $data['pelajaran_semester_1'] = $pelajaran_semester_1;
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    $data['status'] = false;
                    $data['misage'] = 'gagal upload data';
                    $data['mata_pelajaran'] = $mata_pelajaran_by_id;
                    $data['non_pelajaran_semester_1'] = $materi_non_pelajaran;
                    $data['pelajaran_semester_1'] = $pelajaran_semester_1;
                    header('Content-Type: application/json');
                    echo json_encode($data);
                }
            }  else {
                $data['status'] = false;
                $data['misage'] = 'gagal get data';
                $data['mata_pelajaran'] = $mata_pelajaran_by_id;
                $data['non_pelajaran_semester_1'] = $materi_non_pelajaran;
                $data['pelajaran_semester_1'] = $status_pelajaran_semester_1;
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        } else {
            $data['status'] = true;
            $data['misage'] = 'sukses';
            $data['mata_pelajaran'] = $mata_pelajaran_by_id;
            $data['non_pelajaran_semester_1'] = $materi_non_pelajaran;
            $data['pelajaran_semester_1'] = $pelajaran_semester_1;
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }
    // public function get_materi_by_mata_pelajaran($id_mata_pelajaran){
    //     date_default_timezone_set('Asia/Jakarta');  
    //     $date_now = date('Y-m-d H-i-s');
    //     $semester1 = '1';
    //     $id_murid = $this->session->userdata('id_murid');
    //     // echo json_encode($id_murid);
    //     // die();
    //     $mata_pelajaran_by_id = $this->model_mata_pelajaran->get_by_id($id_mata_pelajaran);
    //     $get_pelajaran_by_id_matapelejaran_by_id_murid = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_murid($id_mata_pelajaran, $id_murid);
    //     $cek_pelajaran_by_id_matapelajaran_by_id_murid_by_status = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_murid_status($id_mata_pelajaran, $id_murid, '1');
    //     if (empty($cek_pelajaran_by_id_matapelajaran_by_id_murid_by_status)) {
    //         $materi_by_id_mata_pelajaran_1 = $this->model_materi->get_all_by_id_mata_pelajaran($id_mata_pelajaran, $semester1);
    //         $materi_non_pelajaran = array();
    //         foreach ($materi_by_id_mata_pelajaran_1 as $row_data) {
    //             $get_pembelajaran_by_id_materi = $this->model_pembelajaran->get_by_id_materi_id_murid($row_data->id_materi, $id_murid);
    //             if (empty($get_pembelajaran_by_id_materi)) {
    //                 $data_materi['id_materi'] = $row_data->id_materi;
    //                 $data_materi['id_murid'] = $id_murid;
    //                 $data_materi['id_mata_pelajaran'] = $row_data->id_mata_pelajaran;
    //                 $data_materi['id_tingkat'] = $row_data->id_tingkat;
    //                 $data_materi['id_jurusan'] = $row_data->id_jurusan;
    //                 $data_materi['nama_mata_pelajaran'] = $row_data->nama_mata_pelajaran;
    //                 $data_materi['nama_tingkat'] = $row_data->nama_tingkat;
    //                 $data_materi['nama_jurusan'] = $row_data->nama_jurusan;
    //                 $data_materi['nama_materi'] = $row_data->nama_materi;
    //                 $data_materi['path_pdf'] = $row_data->path_pdf;
    //                 $data_materi['path_video'] = $row_data->path_video;
    //                 $data_materi['status'] = '0';
    //                 $materi_non_pelajaran[] = $data_materi;
    //             }         
    //         }
    //         if ($materi_non_pelajaran) {
    //             $status = '1';    
    //             $status_pelajaran_semester_1 = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_materi_id_murid_status($id_mata_pelajaran, $materi_non_pelajaran[0]['id_materi'], $id_murid, $status);
    //             if (empty($status_pelajaran_semester_1)) {
    //                 $data_pelajaran = array(
    //                     'id_mata_pelajaran' => $id_mata_pelajaran,
    //                     'id_materi' => $materi_non_pelajaran[0]['id_materi'],
    //                     'id_murid' => $id_murid,
    //                     'status' => '1',
    //                 );
    //                 $simpan_data_user = $this->model_pembelajaran->save('t_pembelajaran', $data_pelajaran);
    //                 if ($simpan_data_user) {
    //                     $get_pelajaran_by_id_matapelejaran_by_id_murid = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_murid($id_mata_pelajaran, $id_murid);
    //                     $data['status'] = true;
    //                     $data['misage'] = 'sukses';
    //                     $data['mata_pelajaran'] = $mata_pelajaran_by_id;
    //                     $data['pelajaran'] = $get_pelajaran_by_id_matapelejaran_by_id_murid;
    //                     header('Content-Type: application/json');
    //                     echo json_encode($data);
    //                 } else {
    //                     $data['status'] = false;
    //                     $data['misage'] = 'gagal upload data';
    //                     $data['mata_pelajaran'] = $mata_pelajaran_by_id;
    //                     $data['pelajaran'] = $get_pelajaran_by_id_matapelejaran_by_id_murid;
    //                     header('Content-Type: application/json');
    //                     echo json_encode($data);
    //                 }
    //             }  else {
    //                 $data['status'] = false;
    //                 $data['misage'] = 'gagal get data';
    //                 $data['mata_pelajaran'] = $mata_pelajaran_by_id;
    //                 $data['pelajaran'] = $get_pelajaran_by_id_matapelejaran_by_id_murid;
    //                 header('Content-Type: application/json');
    //                 echo json_encode($data);
    //             }
    //         } else {
    //             $data['status'] = true;
    //             $data['misage'] = 'sukses';
    //             $data['mata_pelajaran'] = $mata_pelajaran_by_id;
    //             $data['pelajaran'] = $get_pelajaran_by_id_matapelejaran_by_id_murid;
    //             header('Content-Type: application/json');
    //             echo json_encode($data);
    //         }
    //     }else{
    //         $data['status'] = false;
    //         $data['misage'] = 'tugas belum di selesaikan';
    //         $data['mata_pelajaran'] = $mata_pelajaran_by_id;
    //         $data['pelajaran'] = $get_pelajaran_by_id_matapelejaran_by_id_murid;
    //         header('Content-Type: application/json');
    //         echo json_encode($data);
    //     }
    // }
    public function get_materi_by_mata_pelajaran($id_mata_pelajaran){
        date_default_timezone_set('Asia/Jakarta');  
        $date_now = date('Y-m-d H-i-s');
        $semester1 = '1';
        $id_murid = $this->session->userdata('id_murid');
        // echo json_encode($id_murid);
        // die();
        $mata_pelajaran_by_id = $this->model_mata_pelajaran->get_by_id($id_mata_pelajaran);
        $get_pelajaran_by_id_matapelejaran_by_id_murid = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_murid($id_mata_pelajaran, $id_murid);
        $cek_pelajaran_by_id_matapelajaran_by_id_murid_by_status = $this->model_pembelajaran->get_by_id_mata_pelajaran_id_murid_status($id_mata_pelajaran, $id_murid, '1');
        $data['status'] = true;
        $data['misage'] = 'Pembelajaran sukses di ambil';
        $data['mata_pelajaran'] = $mata_pelajaran_by_id;
        $data['pelajaran'] = $get_pelajaran_by_id_matapelejaran_by_id_murid;
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}