<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_service_edumedia extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
        $this->load->model('Model_service_edumedia'); 
        // $this->db_grafik_live = $this->load->database('db_grafik_live', TRUE);

    }

	public function tes(){
		echo "testing koneksi";
	}

	public function get_all_kategori_aktif(){
		$result = $this->Model_service_edumedia->get_all_kategori_aktif();

		foreach ($result as $hasil) {
			$hasil['icon'] =  "https://edumedia.id/".$hasil['icon_app'];
			$hasil['icon_web'] =  "https://edumedia.id/".$hasil['icon_web'];
			$hasil['background'] =  "https://edumedia.id/".$hasil['background'];
			$result2[] = $hasil;
		}

        header('Content-Type: application/json');
		echo json_encode($result2);
	}

	public function get_all_sub_kategori_aktif_by_kategori(){
		$id_kategori = $this->input->post('id_kategori');
		$result = $this->Model_service_edumedia->get_all_sub_kategori_aktif_by_kategori($id_kategori);

        header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_kursus_aktif_by_sub_kategori(){
		$id_sub_kategori = $this->input->post('id_sub_kategori');
		$result = $this->Model_service_edumedia->get_kursus_aktif_by_sub_kategori($id_sub_kategori);

        header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_kursus_aktif_by_kategori(){
		$id_kategori = $this->input->post('id_kategori');
		$result = $this->Model_service_edumedia->get_kursus_aktif_by_kategori($id_kategori);

		if(empty($result)){
			$hasil2 = $result;
		}
		else {
			foreach ($result as $hasil) {
				$hasil['card_kursus'] =  "https://admin.edumedia.id/".$hasil['card_kursus'];
				$hasil2[] = $hasil;
			}
		}

		

        header('Content-Type: application/json');
		echo json_encode($hasil2);
	}

	public function get_materi_aktif_by_kursus(){
		$id_kursus = $this->input->post('id_kursus');
		$result = $this->Model_service_edumedia->get_materi_aktif_by_kursus($id_kursus);

        header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_kuis_by_materi(){
		$id_materi = $this->input->post('id_materi');
		$result = $this->Model_service_edumedia->get_soal_kuis_by_materi($id_materi);

		foreach ($result as $hasil) {
			$kuis['id_soal_kuis'] = $hasil['id_soal_kuis'];
			$kuis['soal_kuis'] = $hasil['soal_kuis'];
			$kuis['id_jawaban_benar'] = $hasil['id_jawaban_benar'];
			$kuis['jawaban_benar'] = $hasil['jawaban_benar'];

			$result2 = $this->Model_service_edumedia->get_pilihan_jawaban_by_id_soal_kuis($hasil['id_soal_kuis']);
			$kuis['opsi_jawaban'] = $result2;


			$kumpulankuis[] = $kuis;
		}
	
        header('Content-Type: application/json');
		echo json_encode($hasil_kuis = array('id_materi' => $hasil['id_materi'], 'nama_materi' => $hasil['nama_materi'], 'kuis' => $kumpulankuis));
	}

	public function save_jawaban_kuis(){
		$id_materi = $this->input->post('id_materi');
		$id_murid = $this->input->post('id_murid');
		$id_soal = $this->input->post('id_soal');
		$id_jawaban = $this->input->post('id_jawaban');

		$cek_jawaban_by_id_soal_id_murid = $this->Model_service_edumedia->get_jawaban_by_id_soal_id_murid($id_soal, $id_murid);

		if ($cek_jawaban_by_id_soal_id_murid) {

			$data_kuis = array('id_pilihan_jawaban' => $id_jawaban);

			$update_jawaban_kuis = $this->Model_service_edumedia->update_data('t_jawaban_kuis', $data_kuis, array('id_murid' => $id_murid, 'id_soal_kuis' => $id_soal));

			$data['status'] = true;
			$data['mesage_error'] = 'sukses tambah data';

			header('Content-Type: application/json');
			echo json_encode($data);
		}else{

			$data_kuis = array('id_soal_kuis' => $id_soal,
	                        'id_pilihan_jawaban' => $id_jawaban,
	                        'id_murid' => $id_murid);

			$save_jawaban_soal = $this->Model_service_edumedia->input_data('t_jawaban_kuis', $data_kuis);

			if ($save_jawaban_soal) {
				$data['status'] = true;
				$data['mesage_error'] = 'sukses tambah data';

				header('Content-Type: application/json');
				echo json_encode($data);
			} else {
				$data['status'] = false;
				$data['mesage_error'] = 'gagal tambah data';

				header('Content-Type: application/json');
				echo json_encode($data);
			}

		}
	}

	public function save_jawaban_kuis_akhir(){
		$id_materi = $this->input->post('id_materi');
		$id_murid = $this->input->post('id_murid');
		$id_soal = $this->input->post('id_soal');
		$id_jawaban = $this->input->post('id_jawaban');
		$id_join_kursus = $this->input->post('id_join_kursus');

		$cek_jawaban_by_id_soal_id_murid = $this->Model_service_edumedia->get_jawaban_by_id_soal_id_murid($id_soal, $id_murid);

		$data_kuis = array('id_soal_kuis' => $id_soal,
                        'id_pilihan_jawaban' => $id_jawaban,
                        'id_murid' => $id_murid);

		$save_jawaban_soal = $this->Model_service_edumedia->input_data('t_jawaban_kuis', $data_kuis);

		if ($save_jawaban_soal) {
			
			$get_point = $this->Model_service_edumedia->get_point_by_id_murid_id_materi($id_murid, $id_materi);
			$poin = 20*count($get_point);

			$data_belajar = array('id_join_kursus' => $id_join_kursus,
                        'id_materi' => $id_materi,
                        'nilai_kuis' => $poin);

			$save_jawaban_soal = $this->Model_service_edumedia->input_data('t_belajar', $data_belajar);

			if ($save_jawaban_soal) {
				
				$data['status'] = true;
				$data['poin'] = $poin;
				$data['mesage_error'] = 'sukses tambah data';

				header('Content-Type: application/json');
				echo json_encode($data);
				
			}else{
				
				$data['status'] = false;
				$data['poin'] = '-';
				$data['mesage_error'] = 'gagal tambah data';

				header('Content-Type: application/json');
				echo json_encode($data);

			}

		} else {
			$data['status'] = false;
			$data['mesage_error'] = 'gagal tambah data';

			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function get_soal_1($id_materi){
		$result = $this->Model_service_edumedia->get_soal_kuis_by_materi($id_materi);

		foreach ($result as $hasil) {
			$kuis['id_soal_kuis'] = $hasil['id_soal_kuis'];
			$kuis['soal_kuis'] = $hasil['soal_kuis'];
			$kuis['id_jawaban_benar'] = $hasil['id_jawaban_benar'];
			$kuis['jawaban_benar'] = $hasil['jawaban_benar'];

			$result2 = $this->Model_service_edumedia->get_pilihan_jawaban_by_id_soal_kuis($hasil['id_soal_kuis']);
			$kuis['opsi_jawaban'] = $result2;


			$kumpulankuis[] = $kuis;
		}
	
        header('Content-Type: application/json');
		echo json_encode($kumpulankuis[0]);
	}

	public function get_soal_2($id_materi){
		$result = $this->Model_service_edumedia->get_soal_kuis_by_materi($id_materi);

		foreach ($result as $hasil) {
			$kuis['id_soal_kuis'] = $hasil['id_soal_kuis'];
			$kuis['soal_kuis'] = $hasil['soal_kuis'];
			$kuis['id_jawaban_benar'] = $hasil['id_jawaban_benar'];
			$kuis['jawaban_benar'] = $hasil['jawaban_benar'];

			$result2 = $this->Model_service_edumedia->get_pilihan_jawaban_by_id_soal_kuis($hasil['id_soal_kuis']);
			$kuis['opsi_jawaban'] = $result2;


			$kumpulankuis[] = $kuis;
		}
	
        header('Content-Type: application/json');
		echo json_encode($kumpulankuis[1]);
	}

	public function get_soal_3($id_materi){
		$result = $this->Model_service_edumedia->get_soal_kuis_by_materi($id_materi);

		foreach ($result as $hasil) {
			$kuis['id_soal_kuis'] = $hasil['id_soal_kuis'];
			$kuis['soal_kuis'] = $hasil['soal_kuis'];
			$kuis['id_jawaban_benar'] = $hasil['id_jawaban_benar'];
			$kuis['jawaban_benar'] = $hasil['jawaban_benar'];

			$result2 = $this->Model_service_edumedia->get_pilihan_jawaban_by_id_soal_kuis($hasil['id_soal_kuis']);
			$kuis['opsi_jawaban'] = $result2;


			$kumpulankuis[] = $kuis;
		}
	
        header('Content-Type: application/json');
		echo json_encode($kumpulankuis[2]);
	}

	public function get_soal_4($id_materi){
		$result = $this->Model_service_edumedia->get_soal_kuis_by_materi($id_materi);

		foreach ($result as $hasil) {
			$kuis['id_soal_kuis'] = $hasil['id_soal_kuis'];
			$kuis['soal_kuis'] = $hasil['soal_kuis'];
			$kuis['id_jawaban_benar'] = $hasil['id_jawaban_benar'];
			$kuis['jawaban_benar'] = $hasil['jawaban_benar'];

			$result2 = $this->Model_service_edumedia->get_pilihan_jawaban_by_id_soal_kuis($hasil['id_soal_kuis']);
			$kuis['opsi_jawaban'] = $result2;


			$kumpulankuis[] = $kuis;
		}
	
        header('Content-Type: application/json');
		echo json_encode($kumpulankuis[3]);
	}

	public function get_soal_5($id_materi){
		$result = $this->Model_service_edumedia->get_soal_kuis_by_materi($id_materi);

		foreach ($result as $hasil) {
			$kuis['id_soal_kuis'] = $hasil['id_soal_kuis'];
			$kuis['soal_kuis'] = $hasil['soal_kuis'];
			$kuis['id_jawaban_benar'] = $hasil['id_jawaban_benar'];
			$kuis['jawaban_benar'] = $hasil['jawaban_benar'];

			$result2 = $this->Model_service_edumedia->get_pilihan_jawaban_by_id_soal_kuis($hasil['id_soal_kuis']);
			$kuis['opsi_jawaban'] = $result2;


			$kumpulankuis[] = $kuis;
		}
	
        header('Content-Type: application/json');
		echo json_encode($kumpulankuis[4]);
	}

	public function kursus_saya(){
		$id_murid = $this->input->post('id_murid');
		$result = $this->Model_service_edumedia->get_kursus_saya_by_id_murid($id_murid);

		foreach ($result as $hasil) {

			$hasil['card_kursus'] =  "https://admin.edumedia.id/".$hasil['card_kursus'];
			$total_materi_kursus = $this->Model_service_edumedia->get_total_materi_by_id_kursus($hasil['id_kursus']);
			$total_materi_selesai = $this->Model_service_edumedia->get_materi_selesai_by_join_kursus($hasil['id_join_kursus']);


			if((int)$total_materi_selesai['total_materi_selesai'] < 1){
				$persentase_progress = 0;
			}
			else{
				$persentase_progress = (int)$total_materi_selesai/(int)$total_materi_kursus*100;
			}

			$hasil['persentase_progress'] = $persentase_progress;
			$kursus_saya[] = $hasil;
			
		
		}

        header('Content-Type: application/json');
		echo json_encode($kursus_saya);
	}

	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$password = md5($password);
		$result = $this->Model_service_edumedia->login($email, $password);

		if (empty($result)) {
			$hasil['status_login'] = false;
			$hasil['pesan'] = "Periksa kembali akun anda";
		}
		else {

			
			if($result['status_verified'] == 0){
				$hasil['status_login'] = false;
				$hasil['pesan'] = "Akun anda belum diverifikasi";
			}
			else {
				$hasil = $result;
				$hasil['status_login'] = true;
				$hasil['pesan'] = "sukses";
			}
		}

        header('Content-Type: application/json');
		echo json_encode($hasil);
	}

	public function registrasi(){
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$no_hp = $this->input->post('no_hp');
		$password = $this->input->post('password');
		$result = $this->Model_service_edumedia->cek_email($email);
		$password = md5($password);

		if (empty($result) || $result == null || $result == 'null') {

			$data_user = array('nama_user' => $nama,
                            'email' => $email,
                            'no_hp' => $no_hp,
                            'password' => $password);

			$input = $this->Model_service_edumedia->input_data("m_user", $data_user);

			if(isset($input)){
				$result2 = $this->Model_service_edumedia->cek_email($email);

				$data_murid = array('id_user' => $result2['id_user'], 'status' => "1");

				$input_murid = $this->Model_service_edumedia->input_data("m_murid", $data_murid);

				if(isset($input_murid)){

					$hasil['status'] = true;
					$hasil['pesan'] = "Registrasi berhasil";
				}
				else {

					$hasil['status'] = false;
					$hasil['pesan'] = "Registrasi murid gagal";
				}

			}
			else {

					$hasil['status'] = false;
					$hasil['pesan'] = "Registrasi user gagal";
			}
		}
		else {
			
			$hasil['status'] = false;
			$hasil['pesan'] = "Email sudah terdaftar";
		}

        header('Content-Type: application/json');
		echo json_encode($hasil);
	}

	public function get_profil(){
		$id_user = $this->input->post('id_user');
		$id_murid = $this->input->post('id_murid');
		$result = $this->Model_service_edumedia->get_profil($id_user);
		$result2 = $this->Model_service_edumedia->get_poin_edupay($id_user);

		$hasil = $result;
		$hasil['poin_edupay'] = $result2['poin'];

        header('Content-Type: application/json');
		echo json_encode($hasil);
	}

	public function top_materi(){
		$result = $this->Model_service_edumedia->top_materi();

		foreach ($result as $hasil) {
				$hasil['card_kursus'] =  "https://admin.edumedia.id/".$hasil['card_kursus'];
				$hasil2[] = $hasil;
		}

        header('Content-Type: application/json');
		echo json_encode($hasil2);
	}

	public function get_all_kursus(){
		$result = $this->Model_service_edumedia->get_all_kursus();

		if(empty($result)){
			$hasil2 = $result;
		}
		else {
			foreach ($result as $hasil) {
				$hasil['card_kursus'] =  "https://admin.edumedia.id/".$hasil['card_kursus'];
				$hasil2[] = $hasil;
			}
		}

		

        header('Content-Type: application/json');
		echo json_encode($hasil2);
	}

	public function join_kursus(){
		$id_murid = $this->input->post('id_murid');
		$id_kursus = $this->input->post('id_kursus');
		
		$data_join = array('id_kursus' => $id_kursus,
                            'id_murid' => $id_murid,
                            'poin' => "0");

		$input = $this->Model_service_edumedia->input_data("t_join_kursus", $data_join);

		if(isset($input)){
			$result = $this->Model_service_edumedia->cek_join_kursus($id_murid, $id_kursus);
			$hasil = $result;
			$hasil['status'] = true;
			$hasil['pesan'] = "Berhasil gabung";
		}
		else {
			$hasil['status'] = false;
			$hasil['pesan'] = "Gagal gabung";
		}

        header('Content-Type: application/json');
		echo json_encode($hasil);
	}

	public function cek_join_kursus(){
		$id_murid = $this->input->post('id_murid');
		$id_kursus = $this->input->post('id_kursus');
		$result = $this->Model_service_edumedia->cek_join_kursus($id_murid, $id_kursus);
		

		if(empty($result)){
			$hasil = $result;
			$hasil['status_join'] = false;
			$hasil['pesan'] = "belum join";
		}
		else {

			$hasil = $result;
			$hasil['status_join'] = true;
			$hasil['pesan'] = "sudah join";
		}

        header('Content-Type: application/json');
		echo json_encode($hasil);
	}



}
