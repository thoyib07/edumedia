<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('model_login');
    }

    public function index()
    {
        $this->load->view('page/login');
    }

    public function cek_login()
    {   
        date_default_timezone_set('Asia/Jakarta');
        $date_now = date('Y-m-d H-i-s');
        $username = $this->input->post('username');
        $password = md5($this->input->post('kata_sandi'));
        $cek_data_login = $this->model_login->cek_login($username, $password);
        // echo json_encode($cek_data_login);
        // die();
        if ( $cek_data_login ) {
            $data['id_user'] = $cek_data_login->id_user;
            if ($cek_data_login->id_admin !== NULL) {
                $data['id_admin'] = $cek_data_login->id_admin;
            }
            if ($cek_data_login->id_mentor !== NULL) {
                $data['id_mentor'] = $cek_data_login->id_mentor;
            }
            $data['id_murid'] = $cek_data_login->id_murid;
            $data['nama_user'] = $cek_data_login->nama_user;
            $data['no_hp'] = $cek_data_login->no_hp;
            $data['email'] = $cek_data_login->email;
            $data['tanggal_lahir'] = $cek_data_login->tanggal_lahir;
            $data['agama'] = $cek_data_login->agama;
            $data['jenis_kelamin'] = $cek_data_login->jenis_kelamin;
            $data['path_foto'] = $cek_data_login->path_foto;
            $data['status'] = $cek_data_login->status;
            $this->session->set_userdata($data);
            redirect('home');
            // echo json_encode('sukses data');
            // echo json_encode($data);
        }else{
            $this->session->set_flashdata('pesan','Username atau Password Salah');
            // echo json_encode('gagal ambil data');
        }
        redirect('login');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url().'home');
    }

}

