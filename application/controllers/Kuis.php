<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuis extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('model_kuis');   
        $this->load->model('model_belajar');         
        // $this->load->model('model_squrity');  
        // $this->model_squrity->getsqurity();  
    }

	public function index() {
		$get_soal_by_id_materi = $this->model_kuis->get_by_id_materi($id_materi);
		$data_result_soal =array();
		foreach ($get_soal_by_id_materi as $row) {
			$row_soal['id_soal'] = $row->id_soal;
			$row_soal['id_materi'] = $row->id_materi;
			$row_soal['id_jawaban'] = $row->id_jawaban;
			$row_soal['nama_soal'] = $row->nama_soal;
			$get_jawaban_by_id_soal = $this->model_kuis->get_jawaban_by_id_soal($row->id_soal);
			$row_soal['jawaban_show'] = $get_jawaban_by_id_soal;
			$data_result_soal[] = $row_soal; 
		}
		$data_soal['id_materi'] = $id_materi;
		$data_soal['data_soal'] = $data_result_soal;	
		$this->load->view('page/kuis', $data_soal, false);
	}

	public function get_kuis_by_id_materi($id_materi = null, $id_join_kursus = null)
	{
		$get_soal_by_id_materi = $this->model_kuis->get_by_id_materi($id_materi);
		$data_result_soal =array();
		foreach ($get_soal_by_id_materi as $row) {
			$row_soal['id_soal_kuis'] = $row->id_soal_kuis;
			$row_soal['soal_kuis'] = $row->soal_kuis;
			$row_soal['point'] = $row->point;
			$row_soal['id_materi'] = $row->id_materi;
			$row_soal['id_pilihan_jawaban'] = $row->id_pilihan_jawaban;

			$get_jawaban_by_id_soal = $this->model_kuis->get_jawaban_by_id_soal($row->id_soal_kuis);
			$row_soal['jawaban_show'] = $get_jawaban_by_id_soal;
			$data_result_soal[] = $row_soal; 
		}
		$data_soal['id_materi'] = $id_materi;
		$data_soal['id_join_kursus'] = $id_join_kursus;
		$data_soal['data_soal'] = $data_result_soal;	
		$this->load->view('page/kuis', $data_soal, false);
	}

	function cek_kuis(){
        date_default_timezone_set('Asia/Jakarta');  
        $date_now = date('Y-m-d H-i-s');
        $id_murid = $this->session->userdata('id_murid');
        $id_join_kursus =  $this->input->post('id_join_kursus'); 
        $id_materi =  $this->input->post('id_materi'); 

        $no = 0;
        $point = 0;
        for ($i=0; $i < 5; $i++) { 
            $no++;
            $id_soal =  $this->input->post('soal_'.$no.'');
            $id_jawaban =  $this->input->post('jawaban_'.$no.''); 
            $get_kunci_jawaban_by_id_soal_id_jawaban = $this->model_kuis->get_by_id_soal_id_jawaban($id_soal, $id_jawaban);
            if ($get_kunci_jawaban_by_id_soal_id_jawaban) {
            	$point = $point + $get_kunci_jawaban_by_id_soal_id_jawaban->point;
            }

	        $data_jawaban_kuis = array(
	            'id_soal_kuis' => $id_soal,
	            'id_pilihan_jawaban' => $id_jawaban,
	            'id_murid' => $id_murid
	        );

        	$simpan_jawaban_kuis = $this->model_kuis->save('t_jawaban_kuis',$data_jawaban_kuis);
        }

        $data_belajar = array(
            'id_join_kursus' => $id_join_kursus,
            'id_materi' => $id_materi,
            'nilai_kuis' => $point, 
            'create_date' => $date_now,
            'status' => '1', 
        );

        $simpan_hasil_kuis = $this->model_kuis->save('t_belajar',$data_belajar);

        redirect('Kuis/report_kuis/'.$simpan_hasil_kuis);
	}

	function report_kuis($id_belajar = null){

		$belajar_by_id_belajar = $this->model_belajar->get_by_id($id_belajar);

		$this->load->view('page/report_kuis', $belajar_by_id_belajar, false);

	}

}

