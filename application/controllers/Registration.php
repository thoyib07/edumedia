<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('model_registration');
    }

    public function index()
    {
        $this->load->view('page/registration');
    }

    public function save_registration(){
        $nama_lengkap = $this->input->post('nama_lengkap');
        $email = $this->input->post('email');
        $no_hp = $this->input->post('no_hp');
        $kata_sandi = $this->input->post('kata_sandi');
        $konfirmasi_kata_sandi = $this->input->post('konfirmasi_kata_sandi');

        $data_registration = array(
            'nama_user' => $nama_lengkap,
            'email' => $email,
            'no_hp' => $no_hp,
            'password' => md5($kata_sandi)
        );

        $simpan_registration = $this->model_registration->save('m_user',$data_registration);

        if ($simpan_registration) { 
            $data_murid = array(
                'id_user' => $simpan_registration
            );

            $simpan_murid = $this->model_registration->save('m_murid',$data_murid);
            if ($simpan_murid) {
                $data['id_user'] = $simpan_registration;
                $data['id_murid'] = $simpan_murid;
                $data['nama_user'] = $nama_lengkap;
                $data['no_hp'] = $no_hp;
                $data['email'] = $email;
                $this->session->set_userdata($data);
                redirect('home');                
            } else {
                $this->session->set_flashdata('pesan','Gagal simpan data');                
            }            
        } else {
            $this->session->set_flashdata('pesan','Gagal simpan data');
        }
        redirect('Registrastion');
    }

}

