<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_materi');  
        // $this->model_squrity->getsqurity(); 
    }

    public function index()
    {
        $get_mata_pelajaran = $this->model_mata_pelajaran->get_all_aktif();
        $kategori_result = array();
        $data_header['kategori_list'] = $kategori_result;
        $data['header_menu'] = $this->load->view('header_menu',$data_header,true);
        $data['content'] = $this->load->view('page/home','',true);
        $data['footer'] = $this->load->view('footer','',true);
        $this->load->view('base_view', $data);
        // echo json_encode($list_kursus_saya_data);
    }

    public function get_top_materi(){
        $top_materi = $this->model_materi->get_top();
        $resoult_materi = array();
        foreach ($top_materi as $row_data) {
            $data_materi['id_materi'] = $row_data->id_materi;
            $data_materi['id_sesi'] = $row_data->id_sesi;
            $data_materi['id_kursus'] = $row_data->id_kursus;
            $data_materi['id_mentor'] = $row_data->id_mentor;
            $data_materi['id_user'] = $row_data->id_user;
            $data_materi['id_sub_kategori'] = $row_data->id_sub_kategori;
            $data_materi['id_kategori'] = $row_data->id_kategori;
            $data_materi['nama_materi'] = $row_data->nama_materi;
            $data_materi['embed_video'] = $row_data->embed_video;

            $tumnile_video = $row_data->embed_video;
            $tumnile_video = str_replace('"', '', $tumnile_video);
            $tumnile_video = explode('/', $tumnile_video);

            $data_materi['tumnile'] = $tumnile_video[4];
            $data_materi['path_pdf'] = $row_data->path_pdf;
            $data_materi['nama_sesi'] = $row_data->nama_sesi;
            $data_materi['nama_kursus'] = $row_data->nama_kursus;
            $data_materi['card_kursus'] = $row_data->card_kursus;
            $data_materi['nama_user'] = $row_data->nama_user;
            $data_materi['nama_sub_kategori'] = $row_data->nama_sub_kategori;
            $data_materi['nama_kategori'] = $row_data->nama_kategori;
            $data_materi['icon'] = $row_data->icon;
            $data_materi['icon_web'] = $row_data->icon_web;
            $data_materi['background'] = $row_data->background;
            $data_materi['status'] = $row_data->status;

            $resoult_materi[] = $data_materi;
        }

        header('Content-Type: application/json');
        echo json_encode($resoult_materi);
    }

}

