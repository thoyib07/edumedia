<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('model_Kategori');
        $this->load->model('model_mentor');
        $this->load->model('model_kursus');
        // $this->load->model('model_squrity'); 
        // $this->model_squrity->getsqurity(); 
    }

	public function index()
	{
		$list_kursus_saya_data = $this->model_kursus->get_by_id_murid($this->session->userdata('id_murid'));
		$kursus_saya_result = array();
		foreach ($list_kursus_saya_data as $row) {
			$data_kursus_row['id_join_kursus'] = $row->id_join_kursus;
			$data_kursus_row['id_murid'] = $row->id_murid;
			$data_kursus_row['id_mentor'] = $row->id_mentor;
			$data_kursus_row['id_user'] = $row->id_user;
			$data_kursus_row['id_kursus'] = $row->id_kursus;
			$data_kursus_row['nama_user'] = $row->nama_user;
			// $nama_matapelajaran = $row->materi;
			// $matapelajaran = substr($nama_matapelajaran,0,30); // ambil sebanyak 30 karakter
			// $matapelajaran = substr($nama_matapelajaran,0,strrpos($matapelajaran," ")); // potong per spasi kalimat
			// $data_kursus_row['matapelajaran'] = $matapelajaran.'...';
			$data_kursus_row['no_hp'] = $row->no_hp;
			$data_kursus_row['email'] = $row->email;
			$data_kursus_row['alamat'] = $row->alamat;
			$data_kursus_row['tanggal_lahir'] = $row->tanggal_lahir;
			$data_kursus_row['agama'] = $row->agama;
			$data_kursus_row['jenis_kelamin'] = $row->jenis_kelamin;
			$data_kursus_row['path_foto'] = $row->path_foto;
			$data_kursus_row['nama_kursus'] = $row->nama_kursus;
			$data_kursus_row['card_kursus'] = $row->card_kursus;
			$nana_mentor = $this->model_mentor->get_by_id($row->id_mentor);
			$data_kursus_row['nama_mentor'] = $nana_mentor->nama_user;
			$data_kursus_row['nama_kategori'] = $row->nama_kategori;
			$data_kursus_row['background'] = $row->background;
			$kursus_saya_result[] = $data_kursus_row;
		}
		$get_parent_kategori = $this->model_Kategori->get_parent();
		$kategori_result = array();
		foreach ($get_parent_kategori as $row_kategori) {			
			$data_kategori['id_kategori'] = $row_kategori->id_kategori;			
			$data_kategori['nama_kategori'] = $row_kategori->nama_kategori;		
			$data_kategori['icon'] = $row_kategori->icon;			
			$data_kategori['icon_web'] = $row_kategori->icon_web;			
			$data_kategori['background'] = $row_kategori->background;	
			$get_kategori_by_id_parent = $this->model_Kategori->get_by_id_kategori($row_kategori->id_kategori);
			$data_kategori['child'] = $get_kategori_by_id_parent;
			$kategori_result [] = $data_kategori;
		}
		$data_header['list_kursus_saya'] = $kursus_saya_result;
		$data_header['kategori_list'] = $kategori_result;
		$data['header_menu'] = $this->load->view('header_menu',$data_header,true);
		$data['content'] = $this->load->view('page/home','',true);
		$data['footer'] = $this->load->view('footer','',true);
		$this->load->view('base_view', $data);
		// echo json_encode($list_kursus_saya_data);
	}
}

