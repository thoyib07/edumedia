<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Become_mentor extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('model_Kategori');
        $this->load->model('model_mentor');
        $this->load->model('model_kursus');
        $this->load->model('model_kuesioner');
        $this->load->model('model_registration');
        $this->load->model('model_murid');
        // $this->load->model('model_squrity'); 
        // $this->model_squrity->getsqurity(); 
    }

	public function index()
	{
		$list_kursus_saya_data = $this->model_kursus->get_by_id_murid($this->session->userdata('id_murid'));
		$kursus_saya_result = array();
		foreach ($list_kursus_saya_data as $row) {
			$data_kursus_row['id_join_kursus'] = $row->id_join_kursus;
			$data_kursus_row['id_murid'] = $row->id_murid;
			$data_kursus_row['id_mentor'] = $row->id_mentor;
			$data_kursus_row['id_user'] = $row->id_user;
			$data_kursus_row['id_kursus'] = $row->id_kursus;
			$data_kursus_row['nama_user'] = $row->nama_user;
			// $nama_matapelajaran = $row->materi;
			// $matapelajaran = substr($nama_matapelajaran,0,30); // ambil sebanyak 30 karakter
			// $matapelajaran = substr($nama_matapelajaran,0,strrpos($matapelajaran," ")); // potong per spasi kalimat
			// $data_kursus_row['matapelajaran'] = $matapelajaran.'...';
			$data_kursus_row['no_hp'] = $row->no_hp;
			$data_kursus_row['email'] = $row->email;
			$data_kursus_row['alamat'] = $row->alamat;
			$data_kursus_row['tanggal_lahir'] = $row->tanggal_lahir;
			$data_kursus_row['agama'] = $row->agama;
			$data_kursus_row['jenis_kelamin'] = $row->jenis_kelamin;
			$data_kursus_row['path_foto'] = $row->path_foto;
			$data_kursus_row['nama_kursus'] = $row->nama_kursus;
			$data_kursus_row['card_kursus'] = $row->card_kursus;
			$nana_mentor = $this->model_mentor->get_by_id($row->id_mentor);
			$data_kursus_row['nama_mentor'] = $nana_mentor->nama_user;
			$data_kursus_row['nama_kategori'] = $row->nama_kategori;
			$data_kursus_row['background'] = $row->background;
			$kursus_saya_result[] = $data_kursus_row;
		}
		$get_parent_kategori = $this->model_Kategori->get_parent();
		$kategori_result = array();
		foreach ($get_parent_kategori as $row_kategori) {			
			$data_kategori['id_kategori'] = $row_kategori->id_kategori;			
			$data_kategori['nama_kategori'] = $row_kategori->nama_kategori;		
			$data_kategori['icon'] = $row_kategori->icon;			
			$data_kategori['icon_web'] = $row_kategori->icon_web;			
			$data_kategori['background'] = $row_kategori->background;	
			$get_kategori_by_id_parent = $this->model_Kategori->get_by_id_kategori($row_kategori->id_kategori);
			$data_kategori['child'] = $get_kategori_by_id_parent;
			$kategori_result [] = $data_kategori;
		}
		$data_header['list_kursus_saya'] = $kursus_saya_result;
		$data_header['kategori_list'] = $kategori_result;
		$data['header_menu'] = $this->load->view('header_menu',$data_header,true);
		$data['content'] = $this->load->view('page/page_become_mentor','',true);
		$data['footer'] = $this->load->view('footer','',true);
		$this->load->view('base_view', $data);
		// echo json_encode($list_kursus_saya_data);
	}

	public function kuesioner_1()
	{
		$list_kursus_saya_data = $this->model_kursus->get_by_id_murid($this->session->userdata('id_murid'));
		$kursus_saya_result = array();
		foreach ($list_kursus_saya_data as $row) {
			$data_kursus_row['id_join_kursus'] = $row->id_join_kursus;
			$data_kursus_row['id_murid'] = $row->id_murid;
			$data_kursus_row['id_mentor'] = $row->id_mentor;
			$data_kursus_row['id_user'] = $row->id_user;
			$data_kursus_row['id_kursus'] = $row->id_kursus;
			$data_kursus_row['nama_user'] = $row->nama_user;
			// $nama_matapelajaran = $row->materi;
			// $matapelajaran = substr($nama_matapelajaran,0,30); // ambil sebanyak 30 karakter
			// $matapelajaran = substr($nama_matapelajaran,0,strrpos($matapelajaran," ")); // potong per spasi kalimat
			// $data_kursus_row['matapelajaran'] = $matapelajaran.'...';
			$data_kursus_row['no_hp'] = $row->no_hp;
			$data_kursus_row['email'] = $row->email;
			$data_kursus_row['alamat'] = $row->alamat;
			$data_kursus_row['tanggal_lahir'] = $row->tanggal_lahir;
			$data_kursus_row['agama'] = $row->agama;
			$data_kursus_row['jenis_kelamin'] = $row->jenis_kelamin;
			$data_kursus_row['path_foto'] = $row->path_foto;
			$data_kursus_row['nama_kursus'] = $row->nama_kursus;
			$data_kursus_row['card_kursus'] = $row->card_kursus;
			$nana_mentor = $this->model_mentor->get_by_id($row->id_mentor);
			$data_kursus_row['nama_mentor'] = $nana_mentor->nama_user;
			$data_kursus_row['nama_kategori'] = $row->nama_kategori;
			$data_kursus_row['background'] = $row->background;
			$kursus_saya_result[] = $data_kursus_row;
		}
		$get_parent_kategori = $this->model_Kategori->get_parent();
		$kategori_result = array();
		foreach ($get_parent_kategori as $row_kategori) {			
			$data_kategori['id_kategori'] = $row_kategori->id_kategori;			
			$data_kategori['nama_kategori'] = $row_kategori->nama_kategori;		
			$data_kategori['icon'] = $row_kategori->icon;			
			$data_kategori['icon_web'] = $row_kategori->icon_web;			
			$data_kategori['background'] = $row_kategori->background;	
			$get_kategori_by_id_parent = $this->model_Kategori->get_by_id_kategori($row_kategori->id_kategori);
			$data_kategori['child'] = $get_kategori_by_id_parent;
			$kategori_result [] = $data_kategori;
		}
		$data_header['list_kursus_saya'] = $kursus_saya_result;
		$data_header['kategori_list'] = $kategori_result;
		$data['header_menu'] = $this->load->view('header_menu',$data_header,true);
		$data['content'] = $this->load->view('page/page_become_mentor_kuesioner_1','',true);
		$data['footer'] = $this->load->view('footer','',true);
		$this->load->view('base_view', $data);
		// echo json_encode($list_kursus_saya_data);
	}

	public function cek_kuesioner(){
		$id_soal_kuesioner = $this->input->post('id_soal_kuesioner');
		$jawaban = $this->input->post('jawaban');
		$id_mentor = $this->input->post('id_mentor');

		$get_kuesioner_by_id_mentor = $this->model_kuesioner->get_by_id_mentor_id_soal_kuesioner($id_mentor, $id_soal_kuesioner);

		if ($get_kuesioner_by_id_mentor) {

			$data_kuesioner = array(
	            'id_pilihan_jawaban_kuisioner' => $jawaban
	        );
			
			$update_kuesioner = $this->model_registration->update('t_kuisioner', array('id_mentor' => $id_mentor, 'id_soal_kuisioner' => $id_soal_kuesioner), $data_kuesioner);

        	$data['status'] = true;
            $data['pesan'] = 'Sukses tambah materi';

        	echo json_encode($data);
		} else {

			$data_kuesioner = array(
	            'id_mentor' => $id_mentor,
	            'id_soal_kuisioner' => $id_soal_kuesioner,
	            'id_pilihan_jawaban_kuisioner' => $jawaban
	        );

	        $simpan_kuesioner = $this->model_registration->save('t_kuisioner',$data_kuesioner);

	        if ($simpan_kuesioner) {
	        	$data['status'] = true;
	            $data['pesan'] = 'Sukses tambah materi';

	        	echo json_encode($data);
	        }else{
	        	$data['status'] = false;
	            $data['pesan'] = 'Sukses tambah materi';

	        	echo json_encode($data);
	        }

		}

	}

	public function kuesioner_2($id_mentor = null)
	{
		$list_kursus_saya_data = $this->model_kursus->get_by_id_murid($this->session->userdata('id_murid'));
		$kursus_saya_result = array();
		foreach ($list_kursus_saya_data as $row) {
			$data_kursus_row['id_join_kursus'] = $row->id_join_kursus;
			$data_kursus_row['id_murid'] = $row->id_murid;
			$data_kursus_row['id_mentor'] = $row->id_mentor;
			$data_kursus_row['id_user'] = $row->id_user;
			$data_kursus_row['id_kursus'] = $row->id_kursus;
			$data_kursus_row['nama_user'] = $row->nama_user;
			// $nama_matapelajaran = $row->materi;
			// $matapelajaran = substr($nama_matapelajaran,0,30); // ambil sebanyak 30 karakter
			// $matapelajaran = substr($nama_matapelajaran,0,strrpos($matapelajaran," ")); // potong per spasi kalimat
			// $data_kursus_row['matapelajaran'] = $matapelajaran.'...';
			$data_kursus_row['no_hp'] = $row->no_hp;
			$data_kursus_row['email'] = $row->email;
			$data_kursus_row['alamat'] = $row->alamat;
			$data_kursus_row['tanggal_lahir'] = $row->tanggal_lahir;
			$data_kursus_row['agama'] = $row->agama;
			$data_kursus_row['jenis_kelamin'] = $row->jenis_kelamin;
			$data_kursus_row['path_foto'] = $row->path_foto;
			$data_kursus_row['nama_kursus'] = $row->nama_kursus;
			$data_kursus_row['card_kursus'] = $row->card_kursus;
			$nana_mentor = $this->model_mentor->get_by_id($row->id_mentor);
			$data_kursus_row['nama_mentor'] = $nana_mentor->nama_user;
			$data_kursus_row['nama_kategori'] = $row->nama_kategori;
			$data_kursus_row['background'] = $row->background;
			$kursus_saya_result[] = $data_kursus_row;
		}
		$get_parent_kategori = $this->model_Kategori->get_parent();
		$kategori_result = array();
		foreach ($get_parent_kategori as $row_kategori) {			
			$data_kategori['id_kategori'] = $row_kategori->id_kategori;			
			$data_kategori['nama_kategori'] = $row_kategori->nama_kategori;		
			$data_kategori['icon'] = $row_kategori->icon;			
			$data_kategori['icon_web'] = $row_kategori->icon_web;			
			$data_kategori['background'] = $row_kategori->background;	
			$get_kategori_by_id_parent = $this->model_Kategori->get_by_id_kategori($row_kategori->id_kategori);
			$data_kategori['child'] = $get_kategori_by_id_parent;
			$kategori_result [] = $data_kategori;
		}
		$data_header['list_kursus_saya'] = $kursus_saya_result;
		$data_header['kategori_list'] = $kategori_result;
		$data['header_menu'] = $this->load->view('header_menu',$data_header,true);
		$data['content'] = $this->load->view('page/page_become_mentor_kuesioner_2','',true);
		$data['footer'] = $this->load->view('footer','',true);
		$this->load->view('base_view', $data);
		// echo json_encode($list_kursus_saya_data);
	}

	public function cek_kuesioner_2(){
		echo json_encode($_POST);
	}

	public function kuesioner_3($jawaban_1 = null, $jawaban_2 = null)
	{
		$list_kursus_saya_data = $this->model_kursus->get_by_id_murid($this->session->userdata('id_murid'));
		$kursus_saya_result = array();
		foreach ($list_kursus_saya_data as $row) {
			$data_kursus_row['id_join_kursus'] = $row->id_join_kursus;
			$data_kursus_row['id_murid'] = $row->id_murid;
			$data_kursus_row['id_mentor'] = $row->id_mentor;
			$data_kursus_row['id_user'] = $row->id_user;
			$data_kursus_row['id_kursus'] = $row->id_kursus;
			$data_kursus_row['nama_user'] = $row->nama_user;
			// $nama_matapelajaran = $row->materi;
			// $matapelajaran = substr($nama_matapelajaran,0,30); // ambil sebanyak 30 karakter
			// $matapelajaran = substr($nama_matapelajaran,0,strrpos($matapelajaran," ")); // potong per spasi kalimat
			// $data_kursus_row['matapelajaran'] = $matapelajaran.'...';
			$data_kursus_row['no_hp'] = $row->no_hp;
			$data_kursus_row['email'] = $row->email;
			$data_kursus_row['alamat'] = $row->alamat;
			$data_kursus_row['tanggal_lahir'] = $row->tanggal_lahir;
			$data_kursus_row['agama'] = $row->agama;
			$data_kursus_row['jenis_kelamin'] = $row->jenis_kelamin;
			$data_kursus_row['path_foto'] = $row->path_foto;
			$data_kursus_row['nama_kursus'] = $row->nama_kursus;
			$data_kursus_row['card_kursus'] = $row->card_kursus;
			$nana_mentor = $this->model_mentor->get_by_id($row->id_mentor);
			$data_kursus_row['nama_mentor'] = $nana_mentor->nama_user;
			$data_kursus_row['nama_kategori'] = $row->nama_kategori;
			$data_kursus_row['background'] = $row->background;
			$kursus_saya_result[] = $data_kursus_row;
		}
		$get_parent_kategori = $this->model_Kategori->get_parent();
		$kategori_result = array();
		foreach ($get_parent_kategori as $row_kategori) {			
			$data_kategori['id_kategori'] = $row_kategori->id_kategori;			
			$data_kategori['nama_kategori'] = $row_kategori->nama_kategori;		
			$data_kategori['icon'] = $row_kategori->icon;			
			$data_kategori['icon_web'] = $row_kategori->icon_web;			
			$data_kategori['background'] = $row_kategori->background;	
			$get_kategori_by_id_parent = $this->model_Kategori->get_by_id_kategori($row_kategori->id_kategori);
			$data_kategori['child'] = $get_kategori_by_id_parent;
			$kategori_result [] = $data_kategori;
		}
		$data_header['list_kursus_saya'] = $kursus_saya_result;
		$data_header['kategori_list'] = $kategori_result;
		$data['header_menu'] = $this->load->view('header_menu',$data_header,true);
		$data['content'] = $this->load->view('page/page_become_mentor_kuesioner_3','',true);
		$data['footer'] = $this->load->view('footer','',true);
		$this->load->view('base_view', $data);
		// echo json_encode($list_kursus_saya_data);
	}

	public function cek_kuesioner_3(){
		echo json_encode($_POST);
	}

	public function index_save_registration()
	{
        $this->load->view('page/page_registration_become_mentor');
		// echo json_encode($list_kursus_saya_data);
	}

    public function save_registration(){
        $nama_lengkap = $this->input->post('nama_lengkap');
        $email = $this->input->post('email');
        $no_hp = $this->input->post('no_hp');
        $kata_sandi = $this->input->post('kata_sandi');
        $konfirmasi_kata_sandi = $this->input->post('konfirmasi_kata_sandi');

        $data_registration = array(
            'nama_user' => $nama_lengkap,
            'email' => $email,
            'no_hp' => $no_hp,
            'password' => md5($kata_sandi)
        );

        $simpan_registration = $this->model_registration->save('m_user',$data_registration);

        if ($simpan_registration) { 
            $data_mentor = array(
                'id_user' => $simpan_registration
            );

            $simpan_mentor = $this->model_registration->save('m_mentor',$data_mentor);
            if ($simpan_mentor) {

	            $data_murid = array(
	                'id_user' => $simpan_registration
	            );

	            $simpan_murid = $this->model_registration->save('m_murid',$data_murid);

	            if ($simpan_murid) {

	                $data['id_user'] = $simpan_registration;
	                $data['id_murid'] = $simpan_mentor;
	                $data['nama_user'] = $nama_lengkap;
	                $data['no_hp'] = $no_hp;
	                $data['email'] = $email;
	                $this->session->set_userdata($data);
	                redirect('Become_mentor/kuesioner_1/'.$simpan_mentor);  
	            	
	            }else{

                	$this->session->set_flashdata('pesan','Gagal simpan data');
                	
	            }             
            } else {
                $this->session->set_flashdata('pesan','Gagal simpan data');                
            }            
        } else {
            $this->session->set_flashdata('pesan','Gagal simpan data');
        }
        redirect('Become_mentor/index_save_registration');
    }

    function save_registration_murid() {
    	$id_murid = $this->session->userdata('id_murid');

    	$murid_by_id_murid = $this->model_murid->get_by_id($id_murid);

    	if ($murid_by_id_murid) {

            $data_murid = array(
                'id_user' => $murid_by_id_murid->id_user
            );

            $simpan_mentor = $this->model_registration->save('m_mentor',$data_murid);
            if ($simpan_mentor) {
                $data['id_user'] = $murid_by_id_murid->id_user;
                $data['id_murid'] = $simpan_mentor;
                $data['nama_user'] = $murid_by_id_murid->id_user;
                $data['no_hp'] = $murid_by_id_murid->id_user;
                $data['email'] = $murid_by_id_murid->id_user;
                $this->session->set_userdata($data);
                redirect('Become_mentor/kuesioner_1/'.$simpan_mentor);               
            } else {
                $this->session->set_flashdata('pesan','Gagal simpan data');                
            }     
    	}else{
            $this->session->set_flashdata('pesan','Gagal simpan data');     		
    	}
        redirect('Become_mentor/index_save_registration');
    }
}

