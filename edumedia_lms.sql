-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 12, 2021 at 03:25 AM
-- Server version: 10.1.43-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edumedia_lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_admin`
--

CREATE TABLE `m_admin` (
  `id_admin` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_agama`
--

CREATE TABLE `m_agama` (
  `id_agama` int(10) NOT NULL,
  `nama_agama` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_agama`
--

INSERT INTO `m_agama` (`id_agama`, `nama_agama`, `status`) VALUES
(1, 'islam', 1),
(2, 'kristen', 1),
(3, 'hindu', 1),
(4, 'budha', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori`
--

CREATE TABLE `m_kategori` (
  `id_kategori` int(10) NOT NULL,
  `nama_kategori` varchar(200) NOT NULL,
  `icon` text,
  `icon_web` text,
  `icon_app` text,
  `background` text,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kategori`
--

INSERT INTO `m_kategori` (`id_kategori`, `nama_kategori`, `icon`, `icon_web`, `icon_app`, `background`, `status`) VALUES
(1, 'Multimedia', 'assets/images/icon_katagori/Multimedia_20200122174818_icon.png', 'assets/images/icon_katagori/videographer.png', 'assets/images/icon_app_kategori/multimedia_xdpi_1.png', 'assets/images/card_katagori/Multimedia_20200122174818.png', 1),
(2, 'Games', 'assets/images/icon_katagori/Games_20200122183518_icon.png', 'assets/images/icon_katagori/2d game character (2).png', 'assets/images/icon_app_kategori/games_xdpi_1.png', 'assets/images/card_katagori/Games_20200122183518.png', 1),
(3, 'Interior', 'assets/images/icon_katagori/Interior_20200122174958_icon.png', 'assets/images/icon_katagori/3d design (2).png', 'assets/images/icon_app_kategori/interior_xdpi_1.png', 'assets/images/card_katagori/Interior_20200122174958.png', 1),
(4, 'Youtuber', 'assets/images/icon_katagori/Youtuber_20200122184453_icon.png', 'assets/images/icon_katagori/vlog-vlogger-influencer-multimedia-512-01.png', 'assets/images/icon_app_kategori/youtuber_xdpi_1.png', 'assets/images/card_katagori/Youtuber_20200122184453.png', 1),
(5, 'Entrepreneur', 'assets/images/icon_katagori/Entrepreneur_20200122175109_icon.png', 'assets/images/icon_katagori/Business plan.png', 'assets/images/icon_app_kategori/entrepreneur_xdpi_1.png', 'assets/images/card_katagori/Entrepreneur_20200122175109.png', 1),
(6, 'Apps', 'assets/images/icon_katagori/Apps_20200122175154_icon.png', 'assets/images/icon_katagori/Front End.png', 'assets/images/icon_app_kategori/apps_xdpi_1.png', 'assets/images/card_katagori/Apps_20200122175154.png', 1),
(7, 'Internet Of Things', 'assets/images/icon_katagori/Internet Of Things_20200122175311_icon.png', 'assets/images/icon_katagori/smart office-01.png', 'assets/images/icon_app_kategori/iot_xdpi_1.png', 'assets/images/card_katagori/Internet Of Things_20200122175311.png', 1),
(8, 'Digital Marketing', 'assets/images/icon_katagori/Digital Marketing_20200122175348_icon.png', 'assets/images/icon_katagori/SEO Reporting-01.png', 'assets/images/icon_app_kategori/digital marketing_xdpi_1.png', 'assets/images/card_katagori/Digital Marketing_20200122175348.png', 1),
(9, 'Custom Shop', 'assets/images/icon_katagori/Custom Shop_20200122175505_icon.png', 'assets/images/icon_katagori/totem display (2).png', 'assets/images/icon_app_kategori/custom shop_xdpi_1.png', 'assets/images/card_katagori/Custom Shop_20200122175505.png', 1),
(10, 'Office', 'assets/images/icon_katagori/Office_20200122175551_icon.png', 'assets/images/icon_katagori/introduction to google-01-01.png', 'assets/images/icon_app_kategori/office_xdpi_1.png', 'assets/images/card_katagori/Office_20200122175551.png', 1),
(11, 'Edulove', 'assets/images/icon_katagori/Edulove_20200203160745_icon.png', 'assets/images/icon_katagori/FAMILY-01.png', 'assets/images/icon_app_kategori/edulove_xdpi_1.png', 'assets/images/card_katagori/Edulove_20200203160745.png', 1),
(12, 'Computer Network ', 'assets/images/icon_katagori/ Computer Network Engineering_20200222163730_icon.png', 'assets/images/icon_katagori/TKJ_Network.png', 'assets/images/icon_app_kategori/computer network_xdpi_1.png', 'assets/images/card_katagori/ Computer Network Engineering_20200222163730.png', 1),
(13, 'Management', 'assets/images/icon_katagori/Management_20200222161022_icon.png', 'assets/images/icon_katagori/Management.png', 'assets/images/icon_app_kategori/management_xdpi_1.png', 'assets/images/card_katagori/Management_20200222161022.png', 1),
(14, 'Accountant & Financial', 'assets/images/icon_katagori/Accountant & Financial_20200224140118_icon.png', 'assets/images/icon_katagori/Accounting_Finance.png', 'assets/images/icon_app_kategori/Accountant & finance_xdpi_1.png', 'assets/images/card_katagori/Accountant & Financial_20200224140118.png', 1),
(15, 'Human Resource', 'assets/images/icon_katagori/Human Resource_20200222163508_icon.png', 'assets/images/icon_katagori/HR.png', 'assets/images/icon_app_kategori/hr_xdpi_1.png', 'assets/images/card_katagori/Human Resource_20200222163508.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_kecamatan`
--

CREATE TABLE `m_kecamatan` (
  `id_kecamatan` int(10) NOT NULL,
  `id_kota` int(10) NOT NULL,
  `nama_kecamatan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kelurahan`
--

CREATE TABLE `m_kelurahan` (
  `id_kelurahan` int(10) NOT NULL,
  `id_kecamatan` int(10) NOT NULL,
  `nama_kelurahan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kota`
--

CREATE TABLE `m_kota` (
  `id_kota` int(10) NOT NULL,
  `id_provinsi` int(10) NOT NULL,
  `nama_kota` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kursus`
--

CREATE TABLE `m_kursus` (
  `id_kursus` int(10) NOT NULL,
  `nama_kursus` varchar(200) NOT NULL,
  `card_kursus` text,
  `id_mentor` int(10) NOT NULL,
  `id_sub_kategori` int(10) NOT NULL,
  `poin` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kursus`
--

INSERT INTO `m_kursus` (`id_kursus`, `nama_kursus`, `card_kursus`, `id_mentor`, `id_sub_kategori`, `poin`, `status`) VALUES
(1, 'Animasi 2D', 'assets/images/card_kursus/Animasi 2D_20210104174243.png', 6, 1, 5, 0),
(2, 'Animasi 2D', 'assets/images/card_kursus/Animasi 2D_20210104174312.png', 6, 1, 5, 1),
(3, 'Photography', 'assets/images/card_kursus/Photography_20210104192305.png', 7, 1, 5, 0),
(4, 'Photography', 'assets/images/card_kursus/Photography_20210104203006.png', 7, 1, 5, 1),
(5, 'Membuat Tipografi dalam Adobe Illustrator untuk Pemula', 'assets/images/card_kursus/Membuat Tipografi dalam Adobe Illustrator untuk Pemula_20210107164709.png', 1, 1, 5, 1),
(6, 'Multimedia Pemula', 'assets/images/card_kursus/Multimedia Pemula_20210108133458.png', 8, 1, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_materi`
--

CREATE TABLE `m_materi` (
  `id_materi` int(10) NOT NULL,
  `nama_materi` varchar(200) NOT NULL,
  `id_sesi` int(10) NOT NULL,
  `embed_video` varchar(255) NOT NULL,
  `path_pdf` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `status_verified` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_materi`
--

INSERT INTO `m_materi` (`id_materi`, `nama_materi`, `id_sesi`, `embed_video`, `path_pdf`, `status`, `status_verified`, `create_date`) VALUES
(1, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 3, '', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104194344.pdf', 0, 0, '2021-01-04 19:43:44'),
(2, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 3, '', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104194410.pdf', 0, 0, '2021-01-04 19:44:10'),
(3, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 3, '', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104194452.pdf', 0, 0, '2021-01-04 19:44:52'),
(4, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 3, '', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104194647.pdf', 0, 0, '2021-01-04 19:46:47'),
(5, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 3, '', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104194805.pdf', 0, 0, '2021-01-04 19:48:05'),
(6, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 3, 'https://www.youtube.com/embed/vdfQDwwHI4Q', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104201211.pdf', 0, 0, '2021-01-04 20:12:11'),
(7, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 3, 'https://www.youtube.com/embed/vdfQDwwHI4Q', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104202149.pdf', 1, 0, '2021-01-04 20:21:49'),
(8, 'Materi 1 - Tips Belajar Fotografi Untuk Pemula', 4, 'https://www.youtube.com/embed/vdfQDwwHI4Q', 'assets/document/file_materi/Materi 1 - Tips Belajar Fotografi Untuk Pemula_20210104203626.pdf', 1, 0, '2021-01-04 20:36:26'),
(9, 'Materi 2 - Segitiga Exposure dalam Fotografi', 4, 'https://www.youtube.com/embed/UHi9Ry_PLiM', 'assets/document/file_materi/Materi 2 - Segitiga Exposure dalam Fotografi_20210104211107.pdf', 1, 0, '2021-01-04 21:11:07'),
(10, 'Pengenalan Tipografi pada Adobe Illustrator', 5, 'https://www.youtube.com/embed/7oEvgSuRERQ', 'assets/document/file_materi/Pengenalan Tipografi pada Adobe Illustrator_20210107170501.pdf', 1, 0, '2021-01-07 17:05:01'),
(11, 'Multimedia from begining', 6, '', 'assets/document/file_materi/Multimedia from begining_20210108134112.pdf', 0, 0, '2021-01-08 13:41:12'),
(12, 'Multimedia from begining', 6, '', 'assets/document/file_materi/Multimedia from begining_20210108134444.pdf', 0, 0, '2021-01-08 13:44:44'),
(13, 'Multimedia from begining', 6, '', 'assets/document/file_materi/Multimedia from begining_20210108134457.pdf', 0, 0, '2021-01-08 13:44:57'),
(14, 'Multimedia from begining', 6, '', 'assets/document/file_materi/Multimedia from begining_20210108134504.pdf', 0, 0, '2021-01-08 13:45:04'),
(15, 'Multimedia from begining', 6, '', 'assets/document/file_materi/Multimedia from begining_20210108134632.pdf', 0, 0, '2021-01-08 13:46:32'),
(16, 'Multimedia from begining', 6, 'https://www.youtube.com/embed/qVBeK0qVWuU', 'assets/document/file_materi/Multimedia from begining_20210108135145.pdf', 0, 0, '2021-01-08 13:51:45');

-- --------------------------------------------------------

--
-- Table structure for table `m_mentor`
--

CREATE TABLE `m_mentor` (
  `id_mentor` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_mentor`
--

INSERT INTO `m_mentor` (`id_mentor`, `id_user`, `status`) VALUES
(1, 2, 1),
(2, 2, 1),
(3, 2, 1),
(4, 3, 1),
(5, 4, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_murid`
--

CREATE TABLE `m_murid` (
  `id_murid` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_murid`
--

INSERT INTO `m_murid` (`id_murid`, `id_user`, `status`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_pilihan_jawaban`
--

CREATE TABLE `m_pilihan_jawaban` (
  `id_pilihan_jawaban` int(10) NOT NULL,
  `id_soal_kuis` int(10) NOT NULL,
  `pilihan_jawaban` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pilihan_jawaban`
--

INSERT INTO `m_pilihan_jawaban` (`id_pilihan_jawaban`, `id_soal_kuis`, `pilihan_jawaban`, `status`) VALUES
(1, 1, 'Melukis dengan dinding', 1),
(2, 1, 'Melukis dengan kuas', 1),
(3, 1, 'Melukis dengan cahaya', 1),
(4, 1, 'Melukis dengan fokus', 1),
(5, 2, 'Rana, shutter speed, aperture', 1),
(6, 2, 'Rana, shutter speed, iso', 1),
(7, 2, 'Diaghframa, aperture, iso', 1),
(8, 2, 'Aperture, shutter speed, iso', 1),
(9, 3, 'Objek warna putih tampak putih ', 1),
(10, 3, 'Objek warna putih tampak abu-abu', 1),
(11, 3, 'Menghilangkan warna real pada foto', 1),
(12, 3, 'Semua benar', 1),
(13, 4, 'Memperhatikan objek yang diambil', 1),
(14, 4, ' Atur shutter speed cepat jika objek lambat', 1),
(15, 4, 'Atur shutter speed lambat jika objek cepat ', 1),
(16, 4, 'Tidak memperhatikan objek yang diambil', 1),
(17, 5, 'Mereproduksi benda putih dalam warna apapun ', 1),
(18, 5, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(19, 5, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(20, 5, 'Untuk menerangkan foto yang gelap', 1),
(21, 6, 'Melukis dengan dinding', 1),
(22, 6, 'Melukis dengan kuas', 1),
(23, 6, 'Melukis dengan cahaya', 1),
(24, 6, 'Melukis dengan fokus', 1),
(25, 7, 'Rana, shutter speed, aperture', 1),
(26, 7, 'Rana, shutter speed, iso', 1),
(27, 7, 'Diaghframa, aperture, iso', 1),
(28, 7, 'Aperture, shutter speed, iso', 1),
(29, 8, 'Objek warna putih tampak putih ', 1),
(30, 8, 'Objek warna putih tampak abu-abu', 1),
(31, 8, 'Menghilangkan warna real pada foto', 1),
(32, 8, 'Semua benar', 1),
(33, 9, 'Memperhatikan objek yang diambil', 1),
(34, 9, ' Atur shutter speed cepat jika objek lambat', 1),
(35, 9, 'Atur shutter speed lambat jika objek cepat ', 1),
(36, 9, 'Tidak memperhatikan objek yang diambil', 1),
(37, 10, 'Mereproduksi benda putih dalam warna apapun ', 1),
(38, 10, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(39, 10, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(40, 10, 'Untuk menerangkan foto yang gelap', 1),
(41, 11, 'Melukis dengan dinding', 1),
(42, 11, 'Melukis dengan kuas', 1),
(43, 11, 'Melukis dengan cahaya', 1),
(44, 11, 'Melukis dengan fokus', 1),
(45, 12, 'Rana, shutter speed, aperture', 1),
(46, 12, 'Rana, shutter speed, iso', 1),
(47, 12, 'Diaghframa, aperture, iso', 1),
(48, 12, 'Aperture, shutter speed, iso', 1),
(49, 13, 'Objek warna putih tampak putih ', 1),
(50, 13, 'Objek warna putih tampak abu-abu', 1),
(51, 13, 'Menghilangkan warna real pada foto', 1),
(52, 13, 'Semua benar', 1),
(53, 14, 'Memperhatikan objek yang diambil', 1),
(54, 14, ' Atur shutter speed cepat jika objek lambat', 1),
(55, 14, 'Atur shutter speed lambat jika objek cepat ', 1),
(56, 14, 'Tidak memperhatikan objek yang diambil', 1),
(57, 15, 'Mereproduksi benda putih dalam warna apapun ', 1),
(58, 15, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(59, 15, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(60, 15, 'Untuk menerangkan foto yang gelap', 1),
(61, 16, 'Melukis dengan dinding', 1),
(62, 16, 'Melukis dengan kuas', 1),
(63, 16, 'Melukis dengan cahaya', 1),
(64, 16, 'Melukis dengan fokus', 1),
(65, 17, 'Rana, shutter speed, aperture', 1),
(66, 17, 'Rana, shutter speed, iso', 1),
(67, 17, 'Diaghframa, aperture, iso', 1),
(68, 17, 'Aperture, shutter speed, iso', 1),
(69, 18, 'Objek warna putih tampak putih ', 1),
(70, 18, 'Objek warna putih tampak abu-abu', 1),
(71, 18, 'Menghilangkan warna real pada foto', 1),
(72, 18, 'Semua benar', 1),
(73, 19, 'Memperhatikan objek yang diambil', 1),
(74, 19, ' Atur shutter speed cepat jika objek lambat', 1),
(75, 19, 'Atur shutter speed lambat jika objek cepat ', 1),
(76, 19, 'Tidak memperhatikan objek yang diambil', 1),
(77, 20, 'Mereproduksi benda putih dalam warna apapun ', 1),
(78, 20, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(79, 20, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(80, 20, 'Untuk menerangkan foto yang gelap', 1),
(81, 21, 'Melukis dengan dinding', 1),
(82, 21, 'Melukis dengan kuas', 1),
(83, 21, 'Melukis dengan cahaya', 1),
(84, 21, 'Melukis dengan fokus', 1),
(85, 22, 'Rana, shutter speed, aperture', 1),
(86, 22, 'Rana, shutter speed, iso', 1),
(87, 22, 'Diaghframa, aperture, iso', 1),
(88, 22, 'Aperture, shutter speed, iso', 1),
(89, 23, 'Objek warna putih tampak putih ', 1),
(90, 23, 'Objek warna putih tampak abu-abu', 1),
(91, 23, 'Menghilangkan warna real pada foto', 1),
(92, 23, 'Semua benar', 1),
(93, 24, 'Memperhatikan objek yang diambil', 1),
(94, 24, ' Atur shutter speed cepat jika objek lambat', 1),
(95, 24, 'Atur shutter speed lambat jika objek cepat ', 1),
(96, 24, 'Tidak memperhatikan objek yang diambil', 1),
(97, 25, 'Mereproduksi benda putih dalam warna apapun ', 1),
(98, 25, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(99, 25, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(100, 25, 'Untuk menerangkan foto yang gelap', 1),
(101, 26, 'Melukis dengan dinding', 1),
(102, 26, 'Melukis dengan kuas', 1),
(103, 26, 'Melukis dengan cahaya', 1),
(104, 26, 'Melukis dengan fokus', 1),
(105, 27, 'Rana, shutter speed, aperture', 1),
(106, 27, 'Rana, shutter speed, iso', 1),
(107, 27, 'Diaghframa, aperture, iso', 1),
(108, 27, 'Aperture, shutter speed, iso', 1),
(109, 28, 'Objek warna putih tampak putih ', 1),
(110, 28, 'Objek warna putih tampak abu-abu', 1),
(111, 28, 'Menghilangkan warna real pada foto', 1),
(112, 28, 'Semua benar', 1),
(113, 29, 'Memperhatikan objek yang diambil', 1),
(114, 29, ' Atur shutter speed cepat jika objek lambat', 1),
(115, 29, 'Atur shutter speed lambat jika objek cepat ', 1),
(116, 29, 'Tidak memperhatikan objek yang diambil', 1),
(117, 30, 'Mereproduksi benda putih dalam warna apapun ', 1),
(118, 30, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(119, 30, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(120, 30, 'Untuk menerangkan foto yang gelap', 1),
(121, 31, 'Melukis dengan dinding', 1),
(122, 31, 'Melukis dengan kuas', 1),
(123, 31, 'Melukis dengan cahaya', 1),
(124, 31, 'Melukis dengan fokus', 1),
(125, 32, 'Rana, shutter speed, aperture', 1),
(126, 32, 'Rana, shutter speed, iso', 1),
(127, 32, 'Diaghframa, aperture, iso', 1),
(128, 32, 'Aperture, shutter speed, iso', 1),
(129, 33, 'Objek warna putih tampak putih ', 1),
(130, 33, 'Objek warna putih tampak abu-abu', 1),
(131, 33, 'Menghilangkan warna real pada foto', 1),
(132, 33, 'Semua benar', 1),
(133, 34, 'Memperhatikan objek yang diambil', 1),
(134, 34, ' Atur shutter speed cepat jika objek lambat', 1),
(135, 34, 'Atur shutter speed lambat jika objek cepat ', 1),
(136, 34, 'Tidak memperhatikan objek yang diambil', 1),
(137, 35, 'Mereproduksi benda putih dalam warna apapun ', 1),
(138, 35, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(139, 35, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(140, 35, 'Untuk menerangkan foto yang gelap', 1),
(141, 36, 'Melukis dengan dinding', 1),
(142, 36, 'Melukis dengan kuas', 1),
(143, 36, 'Melukis dengan cahaya', 1),
(144, 36, 'Melukis dengan fokus', 1),
(145, 37, 'Rana, shutter speed, aperture', 1),
(146, 37, 'Rana, shutter speed, iso', 1),
(147, 37, 'Diaghframa, aperture, iso', 1),
(148, 37, 'Aperture, shutter speed, iso', 1),
(149, 38, 'Objek warna putih tampak putih ', 1),
(150, 38, 'Objek warna putih tampak abu-abu', 1),
(151, 38, 'Menghilangkan warna real pada foto', 1),
(152, 38, 'Semua benar', 1),
(153, 39, 'Memperhatikan objek yang diambil', 1),
(154, 39, ' Atur shutter speed cepat jika objek lambat', 1),
(155, 39, 'Atur shutter speed lambat jika objek cepat ', 1),
(156, 39, 'Tidak memperhatikan objek yang diambil', 1),
(157, 40, 'Mereproduksi benda putih dalam warna apapun ', 1),
(158, 40, 'Mengoreksi nada warna yang timbul ketika foto diambil dibawah sumber cahaya ', 1),
(159, 40, ' Mengoreksi nada warna yang timbul ketika foto tidak diambil dibawah sumber cahaya', 1),
(160, 40, 'Untuk menerangkan foto yang gelap', 1),
(161, 41, 'Menentukan besar cahaya yang keluar dan dibutuhkan setiap pengambilan gambar', 1),
(162, 41, 'Mulut lensa cahaya yang masuk ketika pengambilan gambar ', 1),
(163, 41, 'Keluarnya cahaya ketika mengambil gambar', 1),
(164, 41, ' Semua benar', 1),
(165, 42, 'Mengontrol beberapa cahaya yang masuk ', 1),
(166, 42, 'Mengontrol cahaya pada objek tertentu', 1),
(167, 42, ' Mengontrol seberapa cahaya yang masuk ke kamera ', 1),
(168, 42, 'Mengatur cahaya pada objek', 1),
(169, 43, 'Membekukan gerakan dan melambatkan gerakan', 1),
(170, 43, 'Menangkap gerakan dan melambat geakan', 1),
(171, 43, 'Membekukan gerakan dan menangkap gerakan', 1),
(172, 43, 'Mempercepat gerakan dan melambatkan gerakan', 1),
(173, 44, '100 - 400', 1),
(174, 44, '100 - 200', 1),
(175, 44, '100', 1),
(176, 44, '50 - 100', 1),
(177, 45, '1/100, 1/500, 1/250', 1),
(178, 45, '1/125, 1/60', 1),
(179, 45, '1/15, 1/30, 1/8', 1),
(180, 45, '1/100, 1/500, 1/250, 1/125, 1/60', 1),
(181, 46, 'Grafis', 1),
(182, 46, 'Visual', 1),
(183, 46, 'Huruf', 1),
(184, 46, 'Foto', 1),
(185, 47, 'Satu susunan huruf yang bermula dari A sehingga Z', 1),
(186, 47, 'Satu seni memilih dan menyusun huruf', 1),
(187, 47, 'Susunan huruf yang berwarna warni', 1),
(188, 47, 'satu rekaan huruf yang pelbagai', 1),
(189, 48, 'Huruf tidak berkait (Sans Serif)', 1),
(190, 48, 'Huruf Monospace', 1),
(191, 48, 'Huruf Dekoratif', 1),
(192, 48, 'Huruf Lengkung', 1),
(193, 49, 'Jenis huruf', 1),
(194, 49, 'Pengaturan', 1),
(195, 49, 'Penggunaan media', 1),
(196, 49, 'Kontras warna', 1),
(197, 50, 'Readability', 1),
(198, 50, 'Legibility', 1),
(199, 50, 'Stem', 1),
(200, 50, 'Clarity', 1),
(201, 51, '5', 1),
(202, 51, '6', 1),
(203, 51, '7', 1),
(204, 51, '8', 1),
(205, 52, '276 juta', 1),
(206, 52, '500 juta', 1),
(207, 52, '250 juta', 1),
(208, 52, '102 juta', 1),
(209, 53, '5', 1),
(210, 53, '6', 1),
(211, 53, '7', 1),
(212, 53, '8', 1),
(213, 54, '276 juta', 1),
(214, 54, '500 juta', 1),
(215, 54, '250 juta', 1),
(216, 54, '102 juta', 1),
(217, 55, 'Megawati', 1),
(218, 55, 'SBY', 1),
(219, 55, 'Jokowi', 1),
(220, 55, 'Habiebie', 1),
(221, 56, 'Fadli zon', 1),
(222, 56, 'Tata Taneta', 1),
(223, 56, 'Prabowo S', 1),
(224, 56, 'Caca Andhika', 1),
(225, 57, 'Ucok', 1),
(226, 57, 'Udin', 1),
(227, 57, 'Ujang', 1),
(228, 57, 'Buyung', 1),
(229, 58, '5', 1),
(230, 58, '6', 1),
(231, 58, '7', 1),
(232, 58, '8', 1),
(233, 59, '276 juta', 1),
(234, 59, '500 juta', 1),
(235, 59, '250 juta', 1),
(236, 59, '102 juta', 1),
(237, 60, 'Megawati', 1),
(238, 60, 'SBY', 1),
(239, 60, 'Jokowi', 1),
(240, 60, 'Habiebie', 1),
(241, 61, 'Fadli zon', 1),
(242, 61, 'Tata Taneta', 1),
(243, 61, 'Prabowo S', 1),
(244, 61, 'Caca Andhika', 1),
(245, 62, 'Ucok', 1),
(246, 62, 'Udin', 1),
(247, 62, 'Ujang', 1),
(248, 62, 'Buyung', 1),
(249, 63, '5', 1),
(250, 63, '6', 1),
(251, 63, '7', 1),
(252, 63, '8', 1),
(253, 64, '276 juta', 1),
(254, 64, '500 juta', 1),
(255, 64, '250 juta', 1),
(256, 64, '102 juta', 1),
(257, 65, 'Megawati', 1),
(258, 65, 'SBY', 1),
(259, 65, 'Jokowi', 1),
(260, 65, 'Habiebie', 1),
(261, 66, 'Fadli zon', 1),
(262, 66, 'Tata Taneta', 1),
(263, 66, 'Prabowo S', 1),
(264, 66, 'Caca Andhika', 1),
(265, 67, 'Ucok', 1),
(266, 67, 'Udin', 1),
(267, 67, 'Ujang', 1),
(268, 67, 'Buyung', 1),
(269, 68, '5', 1),
(270, 68, '6', 1),
(271, 68, '7', 1),
(272, 68, '8', 1),
(273, 69, '276 juta', 1),
(274, 69, '500 juta', 1),
(275, 69, '250 juta', 1),
(276, 69, '102 juta', 1),
(277, 70, 'Megawati', 1),
(278, 70, 'SBY', 1),
(279, 70, 'Jokowi', 1),
(280, 70, 'Habiebie', 1),
(281, 71, 'Fadli zon', 1),
(282, 71, 'Tata Taneta', 1),
(283, 71, 'Prabowo S', 1),
(284, 71, 'Caca Andhika', 1),
(285, 72, 'Ucok', 1),
(286, 72, 'Udin', 1),
(287, 72, 'Ujang', 1),
(288, 72, 'Buyung', 1),
(289, 73, '5', 1),
(290, 73, '6', 1),
(291, 73, '7', 1),
(292, 73, '8', 1),
(293, 74, '276 juta', 1),
(294, 74, '500 juta', 1),
(295, 74, '250 juta', 1),
(296, 74, '102 juta', 1),
(297, 75, 'Megawati', 1),
(298, 75, 'SBY', 1),
(299, 75, 'Jokowi', 1),
(300, 75, 'Habiebie', 1),
(301, 76, 'Fadli zon', 1),
(302, 76, 'Tata Taneta', 1),
(303, 76, 'Prabowo S', 1),
(304, 76, 'Caca Andhika', 1),
(305, 77, 'Ucok', 1),
(306, 77, 'Udin', 1),
(307, 77, 'Ujang', 1),
(308, 77, 'Buyung', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_pilihan_jawaban_kuisioner`
--

CREATE TABLE `m_pilihan_jawaban_kuisioner` (
  `id_pilihan_jawaban_kuisioner` int(10) NOT NULL,
  `pilihan_jawaban` text NOT NULL,
  `id_soal_kuisioner` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pilihan_jawaban_kuisioner`
--

INSERT INTO `m_pilihan_jawaban_kuisioner` (`id_pilihan_jawaban_kuisioner`, `pilihan_jawaban`, `id_soal_kuisioner`, `status`) VALUES
(1, 'Tatap muka, Informal', 1, 1),
(2, 'Tatap muka, Profesional', 1, 1),
(3, 'Online', 1, 1),
(4, 'Lainnya', 1, 1),
(5, 'Saya pemula', 2, 1),
(6, 'Saya memiliki pengetahuan', 2, 1),
(7, 'Saya berpengalaman', 2, 1),
(8, 'Saya punya video yang siap di ungga', 2, 1),
(9, ' Saat ini tidak', 3, 1),
(10, 'Saya memiliki sedikit pengikut', 3, 1),
(11, 'Pengikut saya cuup banyak', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_provinsi`
--

CREATE TABLE `m_provinsi` (
  `id_provinsi` int(10) NOT NULL,
  `nama_provinsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_sesi`
--

CREATE TABLE `m_sesi` (
  `id_sesi` int(10) NOT NULL,
  `nama_sesi` varchar(200) NOT NULL,
  `id_kursus` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_sesi`
--

INSERT INTO `m_sesi` (`id_sesi`, `nama_sesi`, `id_kursus`, `status`) VALUES
(1, 'Kursus 11', 1, 1),
(2, 'kursus 2', 2, 1),
(3, 'Kursus 3', 3, 1),
(4, 'hhh', 4, 1),
(5, 'g', 5, 1),
(6, 'Multimedia Pemula', 1, 1),
(7, 'Animasi 2D', 2, 1),
(8, 'Photography', 3, 1),
(9, 'Photography', 4, 1),
(10, 'Membuat Tipografi dalam Adobe Illustrator untuk Pemula', 5, 1),
(11, 'Multimedia Pemula', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_soal_kuis`
--

CREATE TABLE `m_soal_kuis` (
  `id_soal_kuis` int(10) NOT NULL,
  `soal_kuis` varchar(255) NOT NULL,
  `point` int(100) DEFAULT NULL,
  `id_materi` int(10) NOT NULL,
  `id_pilihan_jawaban` int(10) NOT NULL COMMENT 'jawaban yang benar',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_soal_kuis`
--

INSERT INTO `m_soal_kuis` (`id_soal_kuis`, `soal_kuis`, `point`, `id_materi`, `id_pilihan_jawaban`, `status`) VALUES
(1, '<p>Fotografi adalah ..</p>', 4, 1, 3, 1),
(2, 'Jenis-jenis pencahayaan/exposure', 4, 1, 8, 1),
(3, '<p>Apa\r\nitu white balance?&nbsp;<br></p>', 4, 1, 9, 1),
(4, '<p>Cara\r\nsetting shutter speed pada kamera?&nbsp;&nbsp;<br></p>', 4, 1, 13, 1),
(5, '<p>Fungsi\r\nwhite balance?&nbsp;<br></p>', 4, 1, 18, 1),
(6, '<p>Fotografi adalah ..</p>', 4, 2, 23, 1),
(7, 'Jenis-jenis pencahayaan/exposure', 4, 2, 28, 1),
(8, '<p>Apa\r\nitu white balance?&nbsp;<br></p>', 4, 2, 29, 1),
(9, '<p>Cara\r\nsetting shutter speed pada kamera?&nbsp;&nbsp;<br></p>', 4, 2, 33, 1),
(10, '<p>Fungsi\r\nwhite balance?&nbsp;<br></p>', 4, 2, 38, 1),
(11, '<p>Fotografi adalah ..</p>', 4, 3, 43, 1),
(12, 'Jenis-jenis pencahayaan/exposure', 4, 3, 48, 1),
(13, '<p>Apa\r\nitu white balance?&nbsp;<br></p>', 4, 3, 49, 1),
(14, '<p>Cara\r\nsetting shutter speed pada kamera?&nbsp;&nbsp;<br></p>', 4, 3, 53, 1),
(15, '<p>Fungsi\r\nwhite balance?&nbsp;<br></p>', 4, 3, 58, 1),
(16, '<p>Fotografi adalah ..</p>', 4, 4, 63, 1),
(17, 'Jenis-jenis pencahayaan/exposure', 4, 4, 68, 1),
(18, '<p>Apa\r\nitu white balance?&nbsp;<br></p>', 4, 4, 69, 1),
(19, '<p>Cara\r\nsetting shutter speed pada kamera?&nbsp;&nbsp;<br></p>', 4, 4, 73, 1),
(20, '<p>Fungsi\r\nwhite balance?&nbsp;<br></p>', 4, 4, 78, 1),
(21, '<p>Fotografi adalah ..</p>', 4, 5, 83, 1),
(22, 'Jenis-jenis pencahayaan/exposure', 4, 5, 88, 1),
(23, '<p>Apa\r\nitu white balance?&nbsp;<br></p>', 4, 5, 89, 1),
(24, '<p>Cara\r\nsetting shutter speed pada kamera?&nbsp;&nbsp;<br></p>', 4, 5, 93, 1),
(25, '<p>Fungsi\r\nwhite balance?&nbsp;<br></p>', 4, 5, 98, 1),
(26, '<p>Fotografi adalah ..</p>', 4, 6, 103, 1),
(27, '<p>Jenis-jenis exposure/pencahayaan</p>', 4, 6, 108, 1),
(28, '<p>Apa\r\nitu white balance?&nbsp;<br></p>', 4, 6, 109, 1),
(29, '<p>Bagaimana cara setting shutter speed pada kamera?</p>', 4, 6, 113, 1),
(30, '<p>Apa fungsi white balance</p>', 4, 6, 118, 1),
(31, '<p>Apa yang dimaksdu dengan fotografi?</p>', 4, 7, 123, 1),
(32, 'Sebutkan jenis-jenis pencahayaan!', 4, 7, 128, 1),
(33, '<p>Apa itu white balance?</p>', 4, 7, 129, 1),
(34, '<p>Bagaimana cara seting shutter speed dikamera?</p>', 4, 7, 133, 1),
(35, '<p>Apa fungsi dari white balance?</p>', 4, 7, 138, 1),
(36, '<p>Apa yang dimaksud dengan fotografi?</p>', 4, 8, 143, 1),
(37, '<p>Sebutkan jenis-jenis pencahayaan!</p>', 4, 8, 148, 1),
(38, '<p>Apa yang dimaksud dengan white balance?</p>', 4, 8, 149, 1),
(39, '<p>Bagaimana cara seting shutter speed dikamera?</p>', 4, 8, 153, 1),
(40, '<p>Apa fungsi dari white balance?</p>', 4, 8, 158, 1),
(41, '<p>Apa\r\nitu aperture?<br></p>', 4, 9, 161, 1),
(42, '<p>Apa\r\nFungsi aperture?&nbsp;<br></p>', 4, 9, 165, 1),
(43, '<p>Apa fungsi dari shutter speed?</p>', 4, 9, 171, 1),
(44, '<p>Dalam keadaan cerah pada luar ruangan lebih baik menggunakan ISO?</p>', 4, 9, 174, 1),
(45, '<p>Pada shutter speed jika ingin menggunakan freeze action harus menggunakan speed?</p>', 4, 9, 177, 1),
(46, '<span style=\"color: rgb(57, 58, 104); font-family: &quot;Open Sans&quot;, sans-serif, Helvetica, Arial; font-weight: 600;\">Tipografi merupakan ilmu yang mempelajari tentang seni dan reka bentuk....</span><br>', 4, 10, 181, 1),
(47, '<p><span style=\"color: rgb(57, 58, 104); font-family: &quot;Open Sans&quot;, sans-serif, Helvetica, Arial; font-weight: 600;\">Apakah maksud tipografi</span><br></p>', 4, 10, 186, 1),
(48, '<p><span style=\"color: rgb(57, 58, 104); font-family: &quot;Open Sans&quot;, sans-serif, Helvetica, Arial; font-weight: 600;\">berikut ini yang&nbsp;</span><strong style=\"margin: 0px; padding: 0px; color: rgb(57, 58, 104); font-family: &quot;Open Sans&quot', 4, 10, 192, 1),
(49, '<p><span style=\"color: rgb(57, 58, 104); font-family: &quot;Open Sans&quot;, sans-serif, Helvetica, Arial; font-weight: 600;\">Readability dalam tipografi dipengaruhi oleh faktor berikut ini, kecuali..</span><br></p>', 4, 10, 195, 1),
(50, '<p><span style=\"color: rgb(57, 58, 104); font-family: &quot;Open Sans&quot;, sans-serif, Helvetica, Arial; font-weight: 600;\">Kemudahan mengenali setiap huruf dan kata disebut...</span><br></p>', 4, 10, 198, 1),
(51, 'Brapa jumlah item pancasila', 4, 11, 201, 1),
(52, '<p>Berapa jumlah penduduk indonesia&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 11, 205, 1),
(53, 'Brapa jumlah item pancasila', 4, 12, 209, 1),
(54, '<p>Berapa jumlah penduduk indonesia&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 12, 213, 1),
(55, '<p>Siapakah Presiden Indonesia tahun 2013&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 12, 218, 1),
(56, '<p>Siapakah Mentri pertahanan tahun 2020&nbsp; &nbsp;</p>', 4, 12, 223, 1),
(57, '<p>Apa nama panggilan laki laki di sumatera barat dipanggil?</p>', 4, 12, 228, 1),
(58, 'Brapa jumlah item pancasila', 4, 13, 229, 1),
(59, '<p>Berapa jumlah penduduk indonesia&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 13, 233, 1),
(60, '<p>Siapakah Presiden Indonesia tahun 2013&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 13, 238, 1),
(61, '<p>Siapakah Mentri pertahanan tahun 2020&nbsp; &nbsp;</p>', 4, 13, 243, 1),
(62, '<p>Apa nama panggilan laki laki di sumatera barat dipanggil?</p>', 4, 13, 248, 1),
(63, 'Brapa jumlah item pancasila', 4, 14, 249, 1),
(64, '<p>Berapa jumlah penduduk indonesia&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 14, 253, 1),
(65, '<p>Siapakah Presiden Indonesia tahun 2013&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 14, 258, 1),
(66, '<p>Siapakah Mentri pertahanan tahun 2020&nbsp; &nbsp;</p>', 4, 14, 263, 1),
(67, '<p>Apa nama panggilan laki laki di sumatera barat dipanggil?</p>', 4, 14, 268, 1),
(68, 'Brapa jumlah item pancasila', 4, 15, 269, 1),
(69, '<p>Berapa jumlah penduduk indonesia&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 15, 273, 1),
(70, '<p>Siapakah Presiden Indonesia tahun 2013&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 15, 278, 1),
(71, '<p>Siapakah Mentri pertahanan tahun 2020&nbsp; &nbsp;</p>', 4, 15, 283, 1),
(72, '<p>Apa nama panggilan laki laki di sumatera barat dipanggil?</p>', 4, 15, 288, 1),
(73, 'Brapa jumlah item pancasila', 4, 16, 289, 1),
(74, '<p>Berapa jumlah penduduk indonesia&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 16, 293, 1),
(75, '<p>Siapakah Presiden Indonesia tahun 2013&nbsp;&nbsp;&nbsp;&nbsp;</p>', 4, 16, 298, 1),
(76, '<p>Siapakah Mentri pertahanan tahun 2020&nbsp; &nbsp;</p>', 4, 16, 303, 1),
(77, '<p>Apa nama panggilan laki laki di sumatera barat dipanggil?</p>', 4, 16, 308, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_soal_kuisioner`
--

CREATE TABLE `m_soal_kuisioner` (
  `id_soal_kuisioner` int(10) NOT NULL,
  `soal_kuisioner` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_soal_kuisioner`
--

INSERT INTO `m_soal_kuisioner` (`id_soal_kuisioner`, `soal_kuisioner`, `status`) VALUES
(1, 'Pengajaran seperti apa yang sudah pernah anda lakukan?', 1),
(2, 'Seberapa \"Pro\" anda dalam hal video?', 1),
(3, 'Apa ada audients yang dapat anda ajak berbagi kursus anda?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_sub_kategori`
--

CREATE TABLE `m_sub_kategori` (
  `id_sub_kategori` int(10) NOT NULL,
  `nama_sub_kategori` varchar(200) NOT NULL,
  `icon` text,
  `icon_web` text,
  `icon_app` text,
  `background` text,
  `id_kategori` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_sub_kategori`
--

INSERT INTO `m_sub_kategori` (`id_sub_kategori`, `nama_sub_kategori`, `icon`, `icon_web`, `icon_app`, `background`, `id_kategori`, `status`) VALUES
(1, 'Multimedia', 'assets/images/icon_katagori/Multimedia_20200122174818_icon.png', 'assets/images/icon_katagori/videographer.png', 'assets/images/icon_app_kategori/multimedia_xdpi_1.png', 'assets/images/card_katagori/Multimedia_20200122174818.png', 1, 1),
(2, 'Games', 'assets/images/icon_katagori/Games_20200122183518_icon.png', 'assets/images/icon_katagori/2d game character (2).png', 'assets/images/icon_app_kategori/games_xdpi_1.png', 'assets/images/card_katagori/Games_20200122183518.png', 2, 1),
(3, 'Interior', 'assets/images/icon_katagori/Interior_20200122174958_icon.png', 'assets/images/icon_katagori/3d design (2).png', 'assets/images/icon_app_kategori/interior_xdpi_1.png', 'assets/images/card_katagori/Interior_20200122174958.png', 3, 1),
(4, 'Youtuber', 'assets/images/icon_katagori/Youtuber_20200122184453_icon.png', 'assets/images/icon_katagori/vlog-vlogger-influencer-multimedia-512-01.png', 'assets/images/icon_app_kategori/youtuber_xdpi_1.png', 'assets/images/card_katagori/Youtuber_20200122184453.png', 4, 1),
(5, 'Entrepreneur', 'assets/images/icon_katagori/Entrepreneur_20200122175109_icon.png', 'assets/images/icon_katagori/Business plan.png', 'assets/images/icon_app_kategori/entrepreneur_xdpi_1.png', 'assets/images/card_katagori/Entrepreneur_20200122175109.png', 5, 1),
(6, 'Apps', 'assets/images/icon_katagori/Apps_20200122175154_icon.png', 'assets/images/icon_katagori/Front End.png', 'assets/images/icon_app_kategori/apps_xdpi_1.png', 'assets/images/card_katagori/Apps_20200122175154.png', 6, 1),
(7, 'Internet Of Things', 'assets/images/icon_katagori/Internet Of Things_20200122175311_icon.png', 'assets/images/icon_katagori/smart office-01.png', 'assets/images/icon_app_kategori/iot_xdpi_1.png', 'assets/images/card_katagori/Internet Of Things_20200122175311.png', 7, 1),
(8, 'Digital Marketing', 'assets/images/icon_katagori/Digital Marketing_20200122175348_icon.png', 'assets/images/icon_katagori/SEO Reporting-01.png', 'assets/images/icon_app_kategori/digital marketing_xdpi_1.png', 'assets/images/card_katagori/Digital Marketing_20200122175348.png', 8, 1),
(9, 'Custom Shop', 'assets/images/icon_katagori/Custom Shop_20200122175505_icon.png', 'assets/images/icon_katagori/totem display (2).png', 'assets/images/icon_app_kategori/custom shop_xdpi_1.png', 'assets/images/card_katagori/Custom Shop_20200122175505.png', 9, 1),
(10, 'Office', 'assets/images/icon_katagori/Office_20200122175551_icon.png', 'assets/images/icon_katagori/introduction to google-01-01.png', 'assets/images/icon_app_kategori/office_xdpi_1.png', 'assets/images/card_katagori/Office_20200122175551.png', 10, 1),
(11, 'Edulove', 'assets/images/icon_katagori/Edulove_20200203160745_icon.png', 'assets/images/icon_katagori/FAMILY-01.png', 'assets/images/icon_app_kategori/edulove_xdpi_1.png', 'assets/images/card_katagori/Edulove_20200203160745.png', 11, 1),
(12, 'Computer Network ', 'assets/images/icon_katagori/ Computer Network Engineering_20200222163730_icon.png', 'assets/images/icon_katagori/TKJ_Network.png', 'assets/images/icon_app_kategori/computer network_xdpi_1.png', 'assets/images/card_katagori/ Computer Network Engineering_20200222163730.png', 12, 1),
(13, 'Management', 'assets/images/icon_katagori/Management_20200222161022_icon.png', 'assets/images/icon_katagori/Management.png', 'assets/images/icon_app_kategori/management_xdpi_1.png', 'assets/images/card_katagori/Management_20200222161022.png', 13, 1),
(14, 'Accountant & Financial', 'assets/images/icon_katagori/Accountant & Financial_20200224140118_icon.png', 'assets/images/icon_katagori/Accounting_Finance.png', 'assets/images/icon_app_kategori/Accountant & finance_xdpi_1.png', 'assets/images/card_katagori/Accountant & Financial_20200224140118.png', 14, 1),
(15, 'Human Resource', 'assets/images/icon_katagori/Human Resource_20200222163508_icon.png', 'assets/images/icon_katagori/HR.png', 'assets/images/icon_app_kategori/hr_xdpi_1.png', 'assets/images/card_katagori/Human Resource_20200222163508.png', 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `id_user` int(10) NOT NULL,
  `nama_user` varchar(200) NOT NULL,
  `no_hp` tinyint(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `id_kelurahan` int(10) NOT NULL,
  `jenis_kelamin` tinyint(2) NOT NULL COMMENT '1=pria,2=wanita',
  `path_foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama_user`, `no_hp`, `email`, `username`, `password`, `alamat`, `tanggal_lahir`, `agama`, `status`, `id_kelurahan`, `jenis_kelamin`, `path_foto`) VALUES
(1, 'Oktha Dwi Purnomo', 127, 'okthadp771@gmail.com', '', 'b3ddeb1fb3ceec4b2c1c5b222f2ec575', '', '0000-00-00', '', 1, 0, 0, ''),
(2, 'Rizqy Pratama', 127, 'pratamarizqy29@gmail.com', '', '87c3557742522c54d6363657b1d512d9', '', '0000-00-00', '', 1, 0, 0, ''),
(3, 'Fadil Renze', 127, 'fadildafirenze2@gmail.com', '', '27699904a2197130c50597f34dab77a2', '', '0000-00-00', '', 1, 0, 0, ''),
(4, 'Fadil Renze', 127, 'fadildafirenze2@gmail.com', '', '27699904a2197130c50597f34dab77a2', '', '0000-00-00', '', 1, 0, 0, ''),
(5, 'password', 0, 'babel3126367@gmail.com', '', '5f4dcc3b5aa765d61d8327deb882cf99', '', '0000-00-00', '', 1, 0, 0, ''),
(6, 'Ravi Ham Utama Sonindra', 127, 'raviutama123@gmail.com', '', '2672539caaadf337147e72ba5ff163b2', '', '0000-00-00', '', 1, 0, 0, ''),
(7, 'Andini Gigih Rahmadani', 127, 'Andinirahmadani70@gmail.com', '', 'f93c746fd7f02fa3535f3461ff8fecdb', '', '0000-00-00', '', 1, 0, 0, ''),
(8, 'chandra kirana', 127, 'ck@edumedia.id', '', '058e095496c22fddc54e282e7205ff3f', '', '0000-00-00', '', 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_belajar`
--

CREATE TABLE `t_belajar` (
  `id_belajar` int(10) NOT NULL,
  `id_join_kursus` int(10) NOT NULL,
  `id_materi` int(10) NOT NULL,
  `nilai_kuis` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_belajar`
--

INSERT INTO `t_belajar` (`id_belajar`, `id_join_kursus`, `id_materi`, `nilai_kuis`, `status`, `create_date`) VALUES
(1, 1, 6, 20, 1, '2021-01-04 20:17:44'),
(2, 1, 7, 20, 1, '2021-01-04 20:40:14');

-- --------------------------------------------------------

--
-- Table structure for table `t_jawaban_kuis`
--

CREATE TABLE `t_jawaban_kuis` (
  `id_jawaban_kuis` int(10) NOT NULL,
  `id_soal_kuis` int(10) NOT NULL,
  `id_pilihan_jawaban` int(10) NOT NULL,
  `id_murid` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jawaban_kuis`
--

INSERT INTO `t_jawaban_kuis` (`id_jawaban_kuis`, `id_soal_kuis`, `id_pilihan_jawaban`, `id_murid`, `status`) VALUES
(1, 26, 103, 7, 1),
(2, 27, 108, 7, 1),
(3, 28, 109, 7, 1),
(4, 29, 113, 7, 1),
(5, 30, 118, 7, 1),
(6, 31, 123, 7, 1),
(7, 32, 128, 7, 1),
(8, 33, 129, 7, 1),
(9, 34, 133, 7, 1),
(10, 31, 123, 7, 1),
(11, 32, 128, 7, 1),
(12, 33, 129, 7, 1),
(13, 34, 133, 7, 1),
(14, 31, 123, 7, 1),
(15, 32, 128, 7, 1),
(16, 33, 129, 7, 1),
(17, 34, 133, 7, 1),
(18, 35, 138, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_join_kursus`
--

CREATE TABLE `t_join_kursus` (
  `id_join_kursus` int(10) NOT NULL,
  `id_murid` int(10) NOT NULL,
  `id_kursus` int(10) NOT NULL,
  `poin` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_join_kursus`
--

INSERT INTO `t_join_kursus` (`id_join_kursus`, `id_murid`, `id_kursus`, `poin`, `status`) VALUES
(1, 7, 3, 5, 1),
(2, 7, 4, 5, 1),
(3, 2, 1, 5, 1),
(4, 2, 2, 5, 1),
(5, 2, 3, 5, 1),
(6, 2, 4, 5, 1),
(7, 2, 5, 5, 1),
(8, 8, 6, 5, 1),
(9, 8, 1, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kuisioner`
--

CREATE TABLE `t_kuisioner` (
  `id_kuisioner` int(10) NOT NULL,
  `id_mentor` int(10) NOT NULL,
  `id_soal_kuisioner` int(10) NOT NULL,
  `id_pilihan_jawaban_kuisioner` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kuisioner`
--

INSERT INTO `t_kuisioner` (`id_kuisioner`, `id_mentor`, `id_soal_kuisioner`, `id_pilihan_jawaban_kuisioner`, `status`) VALUES
(1, 2, 1, 0, 1),
(2, 2, 2, 5, 1),
(3, 2, 3, 9, 1),
(4, 0, 1, 1, 1),
(5, 0, 2, 5, 1),
(6, 0, 3, 9, 1),
(7, 6, 1, 1, 1),
(8, 6, 2, 5, 1),
(9, 6, 3, 9, 1),
(10, 7, 1, 4, 1),
(11, 7, 2, 5, 1),
(12, 7, 3, 9, 1),
(13, 8, 1, 2, 1),
(14, 8, 2, 7, 1),
(15, 8, 3, 9, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_admin`
--
ALTER TABLE `m_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `m_agama`
--
ALTER TABLE `m_agama`
  ADD PRIMARY KEY (`id_agama`);

--
-- Indexes for table `m_kategori`
--
ALTER TABLE `m_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `m_kelurahan`
--
ALTER TABLE `m_kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indexes for table `m_kota`
--
ALTER TABLE `m_kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `m_kursus`
--
ALTER TABLE `m_kursus`
  ADD PRIMARY KEY (`id_kursus`);

--
-- Indexes for table `m_materi`
--
ALTER TABLE `m_materi`
  ADD PRIMARY KEY (`id_materi`);

--
-- Indexes for table `m_mentor`
--
ALTER TABLE `m_mentor`
  ADD PRIMARY KEY (`id_mentor`);

--
-- Indexes for table `m_murid`
--
ALTER TABLE `m_murid`
  ADD PRIMARY KEY (`id_murid`);

--
-- Indexes for table `m_pilihan_jawaban`
--
ALTER TABLE `m_pilihan_jawaban`
  ADD PRIMARY KEY (`id_pilihan_jawaban`);

--
-- Indexes for table `m_pilihan_jawaban_kuisioner`
--
ALTER TABLE `m_pilihan_jawaban_kuisioner`
  ADD PRIMARY KEY (`id_pilihan_jawaban_kuisioner`);

--
-- Indexes for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `m_sesi`
--
ALTER TABLE `m_sesi`
  ADD PRIMARY KEY (`id_sesi`);

--
-- Indexes for table `m_soal_kuis`
--
ALTER TABLE `m_soal_kuis`
  ADD PRIMARY KEY (`id_soal_kuis`);

--
-- Indexes for table `m_soal_kuisioner`
--
ALTER TABLE `m_soal_kuisioner`
  ADD PRIMARY KEY (`id_soal_kuisioner`);

--
-- Indexes for table `m_sub_kategori`
--
ALTER TABLE `m_sub_kategori`
  ADD PRIMARY KEY (`id_sub_kategori`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `t_belajar`
--
ALTER TABLE `t_belajar`
  ADD PRIMARY KEY (`id_belajar`);

--
-- Indexes for table `t_jawaban_kuis`
--
ALTER TABLE `t_jawaban_kuis`
  ADD PRIMARY KEY (`id_jawaban_kuis`);

--
-- Indexes for table `t_join_kursus`
--
ALTER TABLE `t_join_kursus`
  ADD PRIMARY KEY (`id_join_kursus`);

--
-- Indexes for table `t_kuisioner`
--
ALTER TABLE `t_kuisioner`
  ADD PRIMARY KEY (`id_kuisioner`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_admin`
--
ALTER TABLE `m_admin`
  MODIFY `id_admin` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_agama`
--
ALTER TABLE `m_agama`
  MODIFY `id_agama` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_kategori`
--
ALTER TABLE `m_kategori`
  MODIFY `id_kategori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  MODIFY `id_kecamatan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_kelurahan`
--
ALTER TABLE `m_kelurahan`
  MODIFY `id_kelurahan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_kota`
--
ALTER TABLE `m_kota`
  MODIFY `id_kota` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_kursus`
--
ALTER TABLE `m_kursus`
  MODIFY `id_kursus` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_materi`
--
ALTER TABLE `m_materi`
  MODIFY `id_materi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `m_mentor`
--
ALTER TABLE `m_mentor`
  MODIFY `id_mentor` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_murid`
--
ALTER TABLE `m_murid`
  MODIFY `id_murid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_pilihan_jawaban`
--
ALTER TABLE `m_pilihan_jawaban`
  MODIFY `id_pilihan_jawaban` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=309;

--
-- AUTO_INCREMENT for table `m_pilihan_jawaban_kuisioner`
--
ALTER TABLE `m_pilihan_jawaban_kuisioner`
  MODIFY `id_pilihan_jawaban_kuisioner` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
  MODIFY `id_provinsi` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_sesi`
--
ALTER TABLE `m_sesi`
  MODIFY `id_sesi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `m_soal_kuis`
--
ALTER TABLE `m_soal_kuis`
  MODIFY `id_soal_kuis` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `m_soal_kuisioner`
--
ALTER TABLE `m_soal_kuisioner`
  MODIFY `id_soal_kuisioner` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_sub_kategori`
--
ALTER TABLE `m_sub_kategori`
  MODIFY `id_sub_kategori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_belajar`
--
ALTER TABLE `t_belajar`
  MODIFY `id_belajar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_jawaban_kuis`
--
ALTER TABLE `t_jawaban_kuis`
  MODIFY `id_jawaban_kuis` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `t_join_kursus`
--
ALTER TABLE `t_join_kursus`
  MODIFY `id_join_kursus` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_kuisioner`
--
ALTER TABLE `t_kuisioner`
  MODIFY `id_kuisioner` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
